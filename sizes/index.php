<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Таблица размеров");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container page-sizes">
	<ul class="nav nav-tabs" role="tablist">
	    <li class="active"><a href="#women-dress" role="tab" data-toggle="tab">Женская одежда</a></li>
        <li><a href="#men-dress" role="tab" data-toggle="tab">Мужская одежда</a></li>
	    <li><a href="#shoes" role="tab" data-toggle="tab">Обувь</a></li>
	</ul>
	<div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="women-dress">
            <table class="sizes-table">
                <thead>
                    <tr>
                        <th>Обхват груди (см)</th>
                        <th>Обхват талии (см)</th>
                        <th>Обхват бедер (см)</th>
                        <th>Международный размер</th>
                        <th>Российский размер</th>
                        <th>Европейский размер</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>80</td>
                        <td>62</td>
                        <td>86</td>
                        <td>XS</td>
                        <td>40</td>
                        <td>34</td>
                    </tr>
                    <tr>
                        <td>84</td>
                        <td>66</td>
                        <td>92</td>
                        <td>S</td>
                        <td>42</td>
                        <td>36</td>
                    </tr>
                    <tr>
                        <td>88</td>
                        <td>70</td>
                        <td>96</td>
                        <td>M</td>
                        <td>44</td>
                        <td>38</td>
                    </tr>
                    <tr>
                        <td>92</td>
                        <td>74</td>
                        <td>100</td>
                        <td>M</td>
                        <td>46</td>
                        <td>40</td>
                    </tr>
                    <tr>
                        <td>96</td>
                        <td>78</td>
                        <td>104</td>
                        <td>L</td>
                        <td>48</td>
                        <td>42</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="men-dress">
            <table class="sizes-table">
                <thead>
                    <tr>
                        <th>Обхват груди (см)</th>
                        <th>Обхват талии (см)</th>
                        <th>Обхват бедер (см)</th>
                        <th>Международный размер</th>
                        <th>Российский размер</th>
                        <th>Европейский размер</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>88</td>
                        <td>70</td>
                        <td>92</td>
                        <td>XXS</td>
                        <td>44</td>
                        <td>38</td>
                    </tr>
                    <tr>
                        <td>92</td>
                        <td>76</td>
                        <td>96</td>
                        <td>XS</td>
                        <td>46</td>
                        <td>40</td>
                    </tr>
                    <tr>
                        <td>96</td>
                        <td>82</td>
                        <td>100</td>
                        <td>S</td>
                        <td>48</td>
                        <td>42</td>
                    </tr>
                    <tr>
                        <td>100</td>
                        <td>88</td>
                        <td>104</td>
                        <td>M</td>
                        <td>50</td>
                        <td>44</td>
                    </tr>
                    <tr>
                        <td>104</td>
                        <td>94</td>
                        <td>108</td>
                        <td>L</td>
                        <td>52</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td>108</td>
                        <td>100</td>
                        <td>112</td>
                        <td>XL</td>
                        <td>54</td>
                        <td>48</td>
                    </tr>
                    <tr>
                        <td>112</td>
                        <td>106</td>
                        <td>116</td>
                        <td>XXL</td>
                        <td>56</td>
                        <td>50</td>
                    </tr>
                    <tr>
                        <td>116</td>
                        <td>112</td>
                        <td>120</td>
                        <td>XXXL</td>
                        <td>58</td>
                        <td>52</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="shoes">
            <table class="sizes-table table-shoes">
                <thead>
                    <tr>
                        <th>Длинна стопы (см)</th>
                        <th>Российский размер</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>22.8</td>
                        <td>35</td>
                    </tr>
                    <tr>
                        <td>23.1</td>
                        <td>35.5</td>
                    </tr>
                    <tr>
                        <td>23.5</td>
                        <td>35-36</td>
                    </tr>
                    <tr>
                        <td>23.8</td>
                        <td>36</td>
                    </tr>
                    <tr>
                        <td>24.1</td>
                        <td>36.5</td>
                    </tr>
                    <tr>
                        <td>24.5</td>
                        <td>37</td>
                    </tr>
                    <tr>
                        <td>24.8</td>
                        <td>37.5</td>
                    </tr>
                    <tr>
                        <td>25.1</td>
                        <td>38</td>
                    </tr>
                    <tr>
                        <td>25.4</td>
                        <td>39</td>
                    </tr>
                    <tr>
                        <td>25.7</td>
                        <td>39.5</td>
                    </tr>
                    <tr>
                        <td>26</td>
                        <td>40</td>
                    </tr>
                    <tr>
                        <td>26.7</td>
                        <td>41</td>
                    </tr>
                    <tr>
                        <td>27.6</td>
                        <td>41.5</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
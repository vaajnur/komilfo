<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Корзина");
$APPLICATION->SetTitle("Корзина");
if(isset($_REQUEST['ORDER_ID'])){
    LocalRedirect("/cart/order_finished.php?ORDER_ID=".$_REQUEST['ORDER_ID']);
}
?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
  )
);?>
<div class="container">
    <p class="h3">В Вашей корзине</p> 
    <?$APPLICATION->IncludeComponent(
        "komilfo:sale.basket.basket",
        "",
        Array(
            "ACTION_VARIABLE" => "action",
            "AUTO_CALCULATION" => "Y",
            "TEMPLATE_THEME" => "blue",
            "COLUMNS_LIST" => array("NAME","DISCOUNT","WEIGHT","DELETE","DELAY","TYPE","PRICE","QUANTITY"),
            "COMPONENT_TEMPLATE" => ".default",
            "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
            "GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
            "GIFTS_CONVERT_CURRENCY" => "Y",
            "GIFTS_HIDE_BLOCK_TITLE" => "N",
            "GIFTS_HIDE_NOT_AVAILABLE" => "N",
            "GIFTS_MESS_BTN_BUY" => "Выбрать",
            "GIFTS_MESS_BTN_DETAIL" => "Подробнее",
            "GIFTS_PAGE_ELEMENT_COUNT" => "4",
            "GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
            "GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
            "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
            "GIFTS_SHOW_IMAGE" => "Y",
            "GIFTS_SHOW_NAME" => "Y",
            "GIFTS_SHOW_OLD_PRICE" => "Y",
            "GIFTS_TEXT_LABEL_GIFT" => "Подарок",
            "GIFTS_PLACE" => "BOTTOM",
            "HIDE_COUPON" => "N",
            "OFFERS_PROPS" => array("SIZES_SHOES","SIZES_CLOTHES"),
            "PATH_TO_ORDER" => "/personal/order.php",
            "PRICE_VAT_SHOW_VALUE" => "N",
            "QUANTITY_FLOAT" => "N",
            "SET_TITLE" => "Y",
            "TEMPLATE_THEME" => "blue",
            "USE_GIFTS" => "Y",
            "USE_PREPAYMENT" => "N"
        )
    );?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
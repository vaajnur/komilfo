<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Оплата");
$APPLICATION->SetTitle("Оплата");?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
  )
);?>
<?
$order_id=$_REQUEST['ORDER_ID'];
$arOrder = CSaleOrder::GetByID($order_id);
$dbPaySysAction = CSalePaySystemAction::GetList(
	array(),
	array(
			"ID" => $arOrder['PAY_SYSTEM_ID']
		),
	false,
	false,
	array("NAME", "ACTION_FILE", "NEW_WINDOW", "PARAMS", "ENCODING", "LOGOTIP")
);
$arPaySysAction = $dbPaySysAction->Fetch();
if($arOrder['PAYED']=="Y")
	header("Location: /personal/orders/");
if($arOrder["USER_ID"]!=$USER->GetID()){
	//CHTTP::SetStatus("404 Not Found");
	//@define("ERROR_404","Y");
}
CSalePaySystemAction::InitParamArrays($arOrder, $order_id, $arPaySysAction["PARAMS"]);
$pathToAction = $_SERVER["DOCUMENT_ROOT"].$arPaySysAction["ACTION_FILE"];
$pathToAction = str_replace("//", "/", $pathToAction);
while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
	$pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);
if (file_exists($pathToAction))
{
	if (is_dir($pathToAction) && file_exists($pathToAction."/payment.php"))
		$pathToAction .= "/payment.php";

	try
	{
		include $pathToAction;
	}
	catch(\Bitrix\Main\SystemException $e)
	{
		if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
			$arResult["ERROR"][] = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
		else
			$arResult["ERROR"][] = $e->getMessage();
        //print_r($arResult["ERROR"]);
	}
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
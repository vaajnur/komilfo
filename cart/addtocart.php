<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");
if(isset($_REQUEST["cmd"])){
    switch($_REQUEST["cmd"])
    {
        case "addP2B":
            //количество товаров в корзине (без цикла) 
            $cntBasketItems = CSaleBasket::GetList(
               array(),
               array( 
                  "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                  "LID" => SITE_ID,
                  "ORDER_ID" => "NULL",
                  "PRODUCT_ID"=> $_REQUEST['product_id']
               ), 
               array()
            );
            $product=CCatalogProduct::GetByID(
             $_REQUEST['product_id']
            );
            if($cntBasketItems>=$product['QUANTITY']){
                echo "disabled";
            }
            else{
               $resItem=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST,"ID"=>intval($_REQUEST["product_id"])),false,false,array("ID","IBLOCK_ID","NAME","IBLOCK_SECTION_ID","PROPERTY_CML2_MANUFACTURER"));
                $arItem=$resItem->GetNext(); 
                //цена на товар
                $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $_REQUEST["product_id"], 'CATALOG_GROUP_ID' => 2)); 
                if ($arPrice = $rsPrices->Fetch()) $price = floor($arPrice["PRICE"]);
                //путь к товару
                $category="";
                $url_product_cat="";
                $nav = CIBlockSection::GetNavChain($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
                while ($arNav=$nav->GetNext()):
                    $category.=$arNav["NAME"].'/';
                endwhile;	
                $designer = $arItem['PROPERTY_CML2_MANUFACTURER_VALUE'];
                $arReturn['name']=explode(" ",$arItem['NAME'])[0]." ".$designer;
                $arReturn['id']=$_REQUEST["product_id"];
                $arReturn['price']=$price;
                $arReturn['brand']=$designer; 
                $arReturn['category']=$category;
                echo json_encode($arReturn);
                 Add2BasketByProductID(
                     $_REQUEST['product_id'],
                     1,
                     array(
                             array("NAME" => "Цвет", "CODE" => "CLR", "VALUE" => $_REQUEST['color']),
                             array("NAME" => "Размер", "CODE" => "SIZE", "VALUE" => $_REQUEST['size']),
                             array("NAME" => "Дата", "CODE" => "DATE", "VALUE" => strtotime("now")),
                         )
                 );
            }
        break;
        case "getHeadBasket":
            $APPLICATION->IncludeComponent("komilfo:sale.basket.basket.line", "top_cart_wc", Array(
                "PATH_TO_BASKET" => "/personal/basket.php",	
                "PATH_TO_PERSONAL" => "/personal/",	
                "SHOW_PERSONAL_LINK" => "Y",
                ),
                false 
            );
        break;
        case "getHeadBasket2":
            $APPLICATION->IncludeComponent("komilfo:sale.basket.basket.line", "float_basket", Array(
                "PATH_TO_BASKET" => "/personal/basket.php",	
                "PATH_TO_PERSONAL" => "/personal/",	
                "SHOW_PERSONAL_LINK" => "Y",
                ),
                false 
            );
        break;
    }
}
?>
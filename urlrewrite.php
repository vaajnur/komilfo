<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/([a-z0-9\\_,-]+)/([a-z0-9\\_,-]+)/([a-z0-9\\_,-]+)/page([0-9]+)/#",
		"RULE" => "SECTION_CODE=\$3&PAGEN_1=\$4",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/([a-z0-9\\_,-]+)/([a-z0-9\\_,-]+)/page([0-9]+)/#",
		"RULE" => "SECTION_CODE=\$2&PAGEN_1=\$3",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/brands/([a-z0-9\\_,-]+)/page([0-9]+)/#",
		"RULE" => "BRAND=\$1&PAGEN_1=\$2",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#",
		"RULE" => "alias=\$1",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/([a-z0-9\\_,-]+)/page([0-9]+)/#",
		"RULE" => "SECTION_CODE=\$1&PAGEN_1=\$2",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/personal/sales/page([0-9]+)/#",
		"RULE" => "PAGEN_1=\$1",
		"ID" => "",
		"PATH" => "/personal/sales/index.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/brands/([a-z0-9\\_,-]+)/#",
		"RULE" => "BRAND=\$1&PAGEN_1=\$2",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/novinki/page([0-9]+)/#",
		"RULE" => "NOVINKI=Y&PAGEN_1=\$1",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/search/page([0-9]+)/#",
		"RULE" => "SEARCH=Y&PAGEN_1=\$1",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/blog/page([0-9]+)/#",
		"RULE" => "PAGEN_1=\$1",
		"ID" => "",
		"PATH" => "/blog/index.php",
	),
	array(
		"CONDITION" => "#^/online/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/personal/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.section",
		"PATH" => "/personal/index.php",
	),
	array(
		"CONDITION" => "#^/novinki/#",
		"RULE" => "NOVINKI=Y",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/search/#",
		"RULE" => "SEARCH=Y",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/store/index.php",
	),
	array(
		"CONDITION" => "#^/blog/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/blog/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
	array(
		"CONDITION" => "#^/#",
		"RULE" => "",
		"ID" => "komilfo:catalog",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^#",
		"RULE" => "",
		"ID" => "komilfo:catalog.section",
		"PATH" => "/bitrix/components/komilfo/news/templates/.default/bitrix/news.detail/.default/template.php",
	),
);

?>
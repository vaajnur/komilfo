<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Политика конфиденциальности");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container static-page">
	<div class="row">
		<div class="col-xs-12">
             <p>Магазин "Komilfo-butik" заботится о конфиденциальности информации и персональных данных, которые предоставляют пользователи сервиса, и обязуется прилагать все возможные усилия по их неразглашению, защите и безопасному хранению.</p>
             <p>Персональные данные, полученные магазином "Komilfo-butik", используются для предоставления пользователям услуг по оформлению заказов, доставке и информированию о деталях заказов.</p>
             <p>Магазин "Komilfo-butik" придерживается принципов обеспечения конфиденциальности персональных данных в соответствии с требованиями законодательства РФ и общепринятыми этическими нормами.</p>
             <h2>Сбор и использование информации</h2>
             <p>Магазин "Komilfo-butik" собирает информацию, передаваемую пользователями с целью использования сервиса интернет-магазина: имя и фамилию, электронный адрес, номер телефона - только в случаях, предусмотренных законодательством.</p>
             <p>Магазин "Komilfo-butik" гарантирует, что конфиденциальная информация не разглашается и не передаётся третьим лицам (ОПЦИОНАЛЬНО кроме доверенных лиц, исключительно с целью исполнения обязательств перед пользователями интернет-магазина). Магазин "Komilfo-butik" принимает все необходимые меры, в том числе криптографические средства (шифрование), для защиты личных данных пользователей от случайного или неправомерного доступа к ним третьих лиц.</p>
             <p>Магазин "Komilfo-butik" оставляет за собой право на агрегацию, группировку и систематизацию исходных статистических данных для разработки инструментов и оказания услуг пользователям. Агрегированные данные не позволяют получить какую-либо информацию о конкретных пользователях без их на то согласия. Собранная информация может храниться администрацией магазина неограниченное количество времени.</p>
             <h2>Передача персональной информации третьей стороне</h2>
             <p>Магазин "Komilfo-butik" не продаёт и не передаёт персональную информацию о пользователях сервиса.</p>
             <p>Магазин "Komilfo-butik" вправе предоставить доступ к персональным данным пользователей исключительно в следующих случаях:</p>
             <p>1. Пользователь магазина дал на то личное согласие.</p>
             <p>2. Это требуется для оформления заказа в магазине "Komilfo-butik", информирования пользователей о статусе заказа и уточнения деталей, доставки.</p>
             <p>3. Этого требует действующее российское законодательство или органы государственной власти в соответствии с предусмотренными законом процедурами.</p>
             <h2>Электронные рассылки</h2>
             <p>Магазин "Komilfo-butik" с согласия пользователей вправе осуществлять электронные рассылки новостей, информации о специальных предложениях и акциях, а также отправлять индивидуальные ответы на обращения своих клиентов.</p>
             <p>В любой момент пользователи могут отказаться от рассылок, перейдя по соответствующей ссылке внизу любого из полученных писем. Также пользователь может отписаться от рассылки, отправив письмо с данным требованием по электронному адресу: <a href="mailto:info@komilfo-butik.com">info@komilfo-butik.com</a></p>
             <h2>Изменения в политике конфиденциальности</h2>
             <p>Магазин "Komilfo-butik" вправе вносить изменения и дополнения в настоящую политику конфиденциальности.</p>
             <p>Актуальный вариант Политики конфиденциальности магазина "Komilfo-butik" опубликован по адресу: <a href="http://komilfo-butik.com/privacy">http://komilfo-butik.com/privacy</a></p>
             <h2>Вопросы по политике конфиденциальности</h2>
             <p>Если у Вас возникли вопросы, предложения или комментарии относительно Политики конфиденциальности магазина "Komilfo-butik", Вы можете обратиться по электронному адресу: <a href="mailto:info@komilfo-butik.com">info@komilfo-butik.com</a></p>
		</div>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
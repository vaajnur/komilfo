<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Магазины");
?><? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
); ?>
	<div class="container shop-info">
		<div class="row">
			<div class="col-md-6">
				<ul id="autoWidth" class="cs-hidden">
					<li><img src="/images/komilfo/shoes-img-1.jpg" alt=""></li>
					<li><img src="/images/komilfo/shoes-img-2.jpg" alt=""></li>
					<li><img src="/images/komilfo/shoes-img-3.jpg" alt=""></li>
				</ul>
			</div>
			<div class="col-md-6">
				<div class="shop-info-container">
					<p>Комильфо-бутик на <b>Садовой, 28.</b></p>
					<p><b>График работы бутика:</b><br>с 11:00 до 21:00 каждый день.</p>
					<p><b>График приёма вещей:</b><br>c понедельника по пятницу с 14:00 до 19:00.</p>
					<p><b>Контактный телефон:</b><br><a class="inverted-link comagic_phone" href="tel:+78003336794">8 (800) 333-67-94</a></p>
					<p><b>Добавочные коды для телефона:</b><br>1 - Администратор - можно узнать по наличию и записаться на выплату.<br>2 - Оценщик - можно уточнить, примут ли Вашу вещь в магазин.<br>3 - Для рекламы и сотрудничества.<br>4 - Отдел контроля качества.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="yandex-map">
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa24701967aecb748f0527994efbe4e0a8a7bf454ce46af9f554cd742607179a6&amp;width=100%&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<div class="container shop-info">
		<div class="row">
			<div class="col-md-6">
				<div class="shop-info-container">
					<p>Комильфо-бутик на <b>Каменноостровском проспекте, 9/2.</b></p>
					<p><b>График работы бутика:</b><br>с 11:00 до 21:00 каждый день.</p>
					<p><b>График приёма вещей:</b><br>c понедельника по пятницу с 14:00 до 19:00.</p>
					<p><b>Контактный телефон:</b><br><a class="inverted-link comagic_phone" href="tel:+78003336794">8 (800) 333-67-94</a></p>
					<p><b>Добавочные коды для телефона:</b><br>1 - Администратор - можно узнать по наличию и записаться на выплату.<br>2 - Оценщик - можно уточнить, примут ли Вашу вещь в магазин.<br>3 - Для рекламы и сотрудничества.<br>4 - Отдел контроля качества.</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="yandex-map">
					<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A172fd8a20544ec84db4dbf51e08d7d400e5042255b691416a755803269bfd86f&amp;width=100%25&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
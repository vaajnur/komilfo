<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отписаться от рассылки");
?>

<main class="sidebar-version">
	<?$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"WITHOUT_FOOTER"=>"Y",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);?>
	<section class="section-policy">
		<div class="container">
			<div class="col-xs-12">
				<?
					if(isset($_REQUEST['email'])&&strlen($_REQUEST['email'])){
						CModule::IncludeModule("subscribe");
						$subscription = CSubscription::GetByEmail($_REQUEST['email']); 
						$subscr=new CSubscription;
						if($arSub = $subscription->Fetch()){
							$aSubscrRub = CSubscription::GetRubricArray($arSub['ID']);
							$subscr->Delete($arSub['ID']);

							$arEventFields = array( 
								"NAME" => $arSub['NAME'],
								"EMAIL" => $arSub['EMAIL']
							); 
							if (CModule::IncludeModule("main")): 
							  if (CEvent::Send("EMAIL_UNSUB_ALL", "s1", $arEventFields,"Y",78)): 
								  echo "ok"; 
							   endif;
							endif;
						}
					}
					if(isset($_REQUEST['rubrics'])){
						$arFields = Array(
							"USER_ID" => ($USER->IsAuthorized()? $USER->GetID():false),
							"FORMAT" => ($FORMAT <> "html"? "text":"html"),
							"EMAIL" => $_REQUEST['email'],
							"CONFIRMED" => "Y",
							"SEND_CONFIRM" => "Y",
							"ACTIVE" => "Y",
							"RUB_ID" => json_decode($_REQUEST['rubrics'])
						);
						$subscr = new CSubscription;
						$ID = $subscr->Add($arFields);
					}
				?>
				<? if(isset($_REQUEST['rubrics'])){ ?> 
				<p>Вы подписались на рассылку обратно.</p>
				<?} else if(isset($_REQUEST['email'])&&strlen($_REQUEST['email'])){?>
				<p>Вы отписались от рассылки</p>
				<a href="<? echo "/unsubscribe/?email=".$_REQUEST['email']."&rubrics=".json_encode($aSubscrRub);?>">Подписаться обратно</a>
				<?}
				else{?>
				<form>
				<input type="text" name="email" placeholder="Ваш E-mail"/>
				<button>Отписаться от рассылки</button>
				</form>
				<?}?>
			</div>
		</div>
	</section>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
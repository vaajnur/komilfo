 <?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Карта сайта");
$APPLICATION->SetTitle("Карта сайта");
?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
  )
);?>
<div class="container">
    <?$APPLICATION->IncludeComponent("komilfo:main.map","",Array(
            "LEVEL" => "3", 
            "COL_NUM" => "1", 
            "SHOW_DESCRIPTION" => "Y", 
            "SET_TITLE" => "Y", 
            "CACHE_TYPE" => "A", 
            "CACHE_TIME" => "3600" 
        )
    );?>   
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
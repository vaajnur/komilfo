<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сменить пароль");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container static-page">
	<div class="row">
		<div class="col-xs-12">
            <?$APPLICATION->IncludeComponent( "komilfo:system.auth.changepasswd", 
                "flat", 
                Array() 
            );
            $APPLICATION->IncludeComponent(
              "komilfo:system.auth.form",
              "errors",
              Array(
              "REGISTER_URL" => "/auth/user/",
              "FORGOT_PASSWORD_URL" => "/auth/",
              "PROFILE_URL" => "/personal/profile/",
              "SHOW_ERRORS" => "Y"
              )
           );
            ?>
		</div>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

<div class="multi-city-popup close">
    <div class="multi-city-popup__container">
        <div class="multi-city-popup__close">
            <svg xmlns="http://www.w3.org/2000/svg" width="9.318" height="9.318" viewBox="0 0 9.318 9.318">
              <g id="Cros" transform="translate(-1078.341 -348.341)">
                <line id="Line_2" data-name="Line 2" x2="7.903" y2="7.903" transform="translate(1079.048 349.048)" fill="none" stroke="#d6d6d6" stroke-linecap="round" stroke-width="1"/>
                <line id="Line_3" data-name="Line 3" x1="7.903" y2="7.903" transform="translate(1079.048 349.048)" fill="none" stroke="#d6d6d6" stroke-linecap="round" stroke-width="1"/>
              </g>
            </svg>
        </div>
        <div class="multi-city-popup__decore"></div>
        <div class="h3">Ваш город</div>
        <ul class="multi-city-popup__list">
        	<? 
        	// $citiesAll = getStoreCities();
        	include __DIR__.'/cities_list.php';
        	$citiesAll = $cities_list1;
        	if(!empty($citiesAll)){
        		foreach ($citiesAll as $key => $city) {?>
			          <li><a class="multi-city-popup__link" href="#"><?=$city;?></a></li>
        		<?}
        	}?>
        </ul>
    </div>

    
</div>

<div class="popup addtocart">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p>Теперь в корзине!</p>
                <p class="h2"></p>
                <p class="popup-text"></p>
            </div>
        </div>
    </div>
</div>
<div class="popup addtowishlist">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p>Отложено!</p>
                <p class="h2"></p>
                <p class="popup-text"></p>
            </div>
        </div>
    </div>
</div>
<div class="popup callbackheader">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                
                <p class="h2">Заказать звонок стилиста</p>

                <p class="popup-text">Заполните ваши контакты, и стилист-консультант свяжется с Вами ближайшее рабочее время.</p>
                <?$APPLICATION->IncludeComponent(
	"slam:easyform", 
	"template_main", 
	array(
		"CATEGORY_ACCEPT_IBLOCK_FIELD" => "FORM_ACCEPT",
		"CATEGORY_ACCEPT_TITLE" => "Согласие на обработку данных",
		"CATEGORY_ACCEPT_TYPE" => "accept",
		"CATEGORY_ACCEPT_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_ACCEPT_VALUE" => "Согласен на обработку <a href=\"#\" target=\"_blank\">персональныx данных</a>",
		"CATEGORY_EMAIL_PLACEHOLDER" => "",
		"CATEGORY_EMAIL_TITLE" => "Ваш E-mail",
		"CATEGORY_EMAIL_TYPE" => "email",
		"CATEGORY_EMAIL_VALIDATION_ADDITIONALLY_MESSAGE" => "data-bv-emailaddress-message=\"E-mail введен некорректно\"",
		"CATEGORY_EMAIL_VALIDATION_MESSAGE" => "Обязательное поле",
		"CATEGORY_EMAIL_VALUE" => "",
		"CATEGORY_MESSAGE_PLACEHOLDER" => "",
		"CATEGORY_MESSAGE_TITLE" => "Сообщение",
		"CATEGORY_MESSAGE_TYPE" => "textarea",
		"CATEGORY_MESSAGE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_MESSAGE_VALUE" => "",
		"CATEGORY_PHONE_IBLOCK_FIELD" => "FORM_PHONE",
		"CATEGORY_PHONE_INPUTMASK" => "N",
		"CATEGORY_PHONE_INPUTMASK_TEMP" => "+7 (999) 999-9999",
		"CATEGORY_PHONE_PLACEHOLDER" => "Ваш телефон",
		"CATEGORY_PHONE_TITLE" => "Ваш телефон",
		"CATEGORY_PHONE_TYPE" => "tel",
		"CATEGORY_PHONE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_PHONE_VALUE" => "",
		"CATEGORY_TITLE_IBLOCK_FIELD" => "NAME",
		"CATEGORY_TITLE_PLACEHOLDER" => "Ваше имя",
		"CATEGORY_TITLE_TITLE" => "Ваше имя",
		"CATEGORY_TITLE_TYPE" => "text",
		"CATEGORY_TITLE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_TITLE_VALUE" => "",
		"CREATE_IBLOCK" => "",
		"CREATE_SEND_MAIL" => "",
		"DISPLAY_FIELDS" => array(
			0 => "TITLE",
			1 => "PHONE",
			2 => "ACCEPT",
			3 => "",
		),
		"EMAIL_BCC" => "",
		"EMAIL_TO" => "",
		"ENABLE_SEND_MAIL" => "Y",
		"ERROR_TEXT" => "Произошла ошибка. Сообщение не отправлено.",
		"EVENT_MESSAGE_ID" => array(
			0 => "101",
		),
		"FIELDS_ORDER" => "TITLE,PHONE,ACCEPT",
		"FORM_AUTOCOMPLETE" => "N",
		"FORM_ID" => "FORM_01",
		"FORM_NAME" => "Заказать звонок",
		"FORM_SUBMIT_VALUE" => "Заказать звонок",
		"FORM_SUBMIT_VARNING" => "Нажимая на кнопку \"#BUTTON#\", вы даете согласие на обработку <a target=\"_blank\" href=\"#\">персональных данных</a>",
		"HIDE_ASTERISK" => "N",
		"HIDE_FIELD_NAME" => "N",
		"HIDE_FORMVALIDATION_TEXT" => "N",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_BOOTSRAP_JS" => "N",
		"MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы Заказать звонок",
		"OK_TEXT" => "Ваше сообщение отправлено.",
		"REPLACE_FIELD_FROM" => "N",
		"REQUIRED_FIELDS" => array(
			0 => "TITLE",
			1 => "PHONE",
			2 => "ACCEPT",
		),
		"SEND_AJAX" => "Y",
		"SHOW_MODAL" => "N",
		"TITLE_SHOW_MODAL" => "Спасибо!",
		"USE_BOOTSRAP_CSS" => "N",
		"USE_BOOTSRAP_JS" => "N",
		"USE_CAPTCHA" => "N",
		"USE_FORMVALIDATION_JS" => "N",
		"USE_IBLOCK_WRITE" => "N",
		"USE_JQUERY" => "Y",
		"USE_MODULE_VARNING" => "Y",
		"WIDTH_FORM" => "500px",
		"WRITE_MESS_FILDES_TABLE" => "N",
		"_CALLBACKS" => "succes_form",
		"COMPONENT_TEMPLATE" => "template_main"
	),
	false
);?>
            </div>
        </div>
    </div>
</div>


<div class="popup buyoneclick">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                
                <p class="h2">Быстрый заказ</p>

                <p class="popup-text">Отправьте заявку, и наш менеджер свяжется с Вами и ответ на все Ваши вопросы в течение ближайших 15 минут рабочего времени.</p>
                <?$APPLICATION->IncludeComponent(
	"slam:easyform", 
	"callback", 
	array(
		"CATEGORY_ACCEPT_IBLOCK_FIELD" => "FORM_ACCEPT",
		"CATEGORY_ACCEPT_TITLE" => "Согласие на обработку данных",
		"CATEGORY_ACCEPT_TYPE" => "accept",
		"CATEGORY_ACCEPT_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_ACCEPT_VALUE" => "Согласен на обработку <a href=\"#\" target=\"_blank\">персональныx данных</a>",
		"CATEGORY_EMAIL_PLACEHOLDER" => "",
		"CATEGORY_EMAIL_TITLE" => "Ваш E-mail",
		"CATEGORY_EMAIL_TYPE" => "email",
		"CATEGORY_EMAIL_VALIDATION_ADDITIONALLY_MESSAGE" => "data-bv-emailaddress-message=\"E-mail введен некорректно\"",
		"CATEGORY_EMAIL_VALIDATION_MESSAGE" => "Обязательное поле",
		"CATEGORY_EMAIL_VALUE" => "",
		"CATEGORY_MESSAGE_PLACEHOLDER" => "",
		"CATEGORY_MESSAGE_TITLE" => "Сообщение",
		"CATEGORY_MESSAGE_TYPE" => "textarea",
		"CATEGORY_MESSAGE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_MESSAGE_VALUE" => "",
		"CATEGORY_PHONE_IBLOCK_FIELD" => "FORM_PHONE",
		"CATEGORY_PHONE_INPUTMASK" => "N",
		"CATEGORY_PHONE_INPUTMASK_TEMP" => "+7 (999) 999-9999",
		"CATEGORY_PHONE_PLACEHOLDER" => "Ваш телефон",
		"CATEGORY_PHONE_TITLE" => "Ваш телефон",
		"CATEGORY_PHONE_TYPE" => "tel",
		"CATEGORY_PHONE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_PHONE_VALUE" => "",
		"CATEGORY_PRODUCTS_PLACEHOLDER" => "",
		"CATEGORY_PRODUCTS_TITLE" => "Товары в корзине",
		"CATEGORY_PRODUCTS_TYPE" => "hidden",
		"CATEGORY_PRODUCTS_VALUE" => "",
		"CATEGORY_TITLE_IBLOCK_FIELD" => "NAME",
		"CATEGORY_TITLE_PLACEHOLDER" => "Ваше имя",
		"CATEGORY_TITLE_TITLE" => "Ваше имя",
		"CATEGORY_TITLE_TYPE" => "text",
		"CATEGORY_TITLE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_TITLE_VALUE" => "",
		"CREATE_IBLOCK" => "",
		"CREATE_SEND_MAIL" => "",
		"DISPLAY_FIELDS" => array(
			0 => "TITLE",
			1 => "PHONE",
			2 => "ACCEPT",
			3 => "PRODUCTS",
			4 => "",
		),
		"EMAIL_BCC" => "",
		"EMAIL_TO" => "",
		"ENABLE_SEND_MAIL" => "Y",
		"ERROR_TEXT" => "Произошла ошибка. Сообщение не отправлено.",
		"EVENT_MESSAGE_ID" => array(
			0 => "101",
		),
		"FIELDS_ORDER" => "TITLE,PHONE,ACCEPT,PRODUCTS",
		"FORM_AUTOCOMPLETE" => "N",
		"FORM_ID" => "FORM_02",
		"FORM_NAME" => "Быстрый заказ",
		"FORM_SUBMIT_VALUE" => "Отправить заказ",
		"FORM_SUBMIT_VARNING" => "Нажимая на кнопку \"#BUTTON#\", вы даете согласие на обработку <a target=\"_blank\" href=\"#\">персональных данных</a>",
		"HIDE_ASTERISK" => "N",
		"HIDE_FIELD_NAME" => "N",
		"HIDE_FORMVALIDATION_TEXT" => "N",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_BOOTSRAP_JS" => "N",
		"MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы купить в 1 клик",
		"OK_TEXT" => "Ваше сообщение отправлено.",
		"REPLACE_FIELD_FROM" => "N",
		"REQUIRED_FIELDS" => array(
			0 => "TITLE",
			1 => "PHONE",
			2 => "ACCEPT",
		),
		"SEND_AJAX" => "Y",
		"SHOW_MODAL" => "N",
		"TITLE_SHOW_MODAL" => "Спасибо!",
		"USE_BOOTSRAP_CSS" => "N",
		"USE_BOOTSRAP_JS" => "N",
		"USE_CAPTCHA" => "N",
		"USE_FORMVALIDATION_JS" => "N",
		"USE_IBLOCK_WRITE" => "N",
		"USE_JQUERY" => "Y",
		"USE_MODULE_VARNING" => "Y",
		"WIDTH_FORM" => "500px",
		"WRITE_MESS_FILDES_TABLE" => "N",
		"_CALLBACKS" => "succes_form_oneclick",
		"COMPONENT_TEMPLATE" => "callback"
	),
	false
);?>
            </div>
        </div>
    </div>
</div>


<div class="popup top-brands">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h2">Популярные бренды</p>
        <ul class="list-unstyled">
          <li><a href="/brands/gucci/">Gucci</a></li>
          <li><a href="/brands/balenciaga/">Balenciaga</a></li>
          <li><a href="/brands/dolce-gabbana/">Dolce & Gabbana</a></li>
          <li><a href="/brands/saint-laurent/">Saint Laurent</a></li>
          <li><a href="/brands/off-white/">Off White</a></li>
          <li><a href="/brands/dior/">Dior</a></li>
          <li><a href="/brands/prada/">Prada</a></li>
          <li><a href="/brands/chanel/">Chanel</a></li>
          <li><a href="/brands/fendi/">Fendi</a></li>
          <li><a href="/brands/louis-vuitton/">Louis Vuitton</a></li>
          <li><a href="/brands/burberry/">Burberry</a></li>
          <li><a href="/brands/versace/">Versace</a></li>
          <li><a href="/brands/brunello-cucinelli/">Brunello Cucinelli</a></li>
          <li><a href="/brands/alexander-mcqueen/">Alexander McQueen</a></li>
          <li><a href="/brands/celine/">Celine</a></li>
          <li><a href="/brands/chloe/">Chloe</a></li>
          <li><a href="/brands/givenchy/">Givenchy</a></li>
          <li><a href="/brands/escada/">Escada</a></li>
          <li><a href="/brands/valentino/">Valentino</a></li>
          <li><a href="/brands/michael-kors/">Michael Kors</a></li>
          <li><a href="/brands/" class="btn btn-fullorange btn-square btn-fullwidth">Все бренды</a></li>
        </ul>
            </div>
        </div>
    </div>
</div>
<div class="popup hand-items">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h2">Оставьте свои данные и мы с вами свяжемся</p>
                <form action="" onsubmit="yaCounter20825989.reachGoal('request_submit'); return true;" >
                    <div class="inputs-container">
                        <div class="input-group input__full-width">
                            <label for="">Ваше имя</label>
                            <input class="your_name" type="text" name="USER_NAME" <? if (strlen($_COOKIE['BITRIX_SM_USER_NAME'])>0) echo "value='".htmlspecialcharsbx($_COOKIE['BITRIX_SM_USER_NAME'])."'";?> placeholder="Имя">
                        </div>
                        <div class="input-group">
                            <label for="">Ваш E-mail</label>
                            <input class="your_email" type="text" name="USER_EMAIL" <? if (strlen($_COOKIE['BITRIX_SM_USER_EMAIL'])>0) echo "value='".htmlspecialcharsbx($_COOKIE['BITRIX_SM_USER_EMAIL'])."'";?> placeholder="E-mail">
                        </div>
                        <div class="input-group">
                            <label for="">Введите Ваш телефон</label>
                            <input class="your_phone" type="text" name="USER_PHONE" class="phone_masked_input" placeholder="Телефон">
                        </div>
                    </div>
                    <button class="btn btn-fullorange go-hand-items btn-square btn-fullwidth">Отправить заявку</button>
                    <div id="message_hand_items" class="information_message"></div>
                </form>
                <br>
                <p class="for_answer reg-info"></p>
                <p class="reg-info">Ваши контакты не попадут в руки 3-х лиц: мы бережно их храним в соответсвии с 152-ФЗ «О защите персональных данных»</p>
            </div>
        </div>
    </div>
</div>
<div class="popup ordered-fitting">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h4">Заказать примерку</p>
                <p class="h2">БАЛЕРО CHANEL</p>
                 <span>Вы можете записаться на бесплатную примерку понравивщейся вещи в бутике, либо померить ее у себя дома.</span>
                <span>Вещи на примерку доставляются по Санкт-Петербургу.</span>
                <span>Оплачивается только доставка - 400 руб.</span>
                <span>Если Вы не в Санкт-Петербурге - высылаем на почту фотографии и дополнительные замеры.</span>
                <span>Оставьте свои данные и мы свяжемся с Вами в течение часа.</span>
                <div class="checkbox">
                    <i></i>
                    <span>Получить дополнительные фото и размеры</span>
                </div>
                <input type="hidden" class="product_id"/>
                <div class="inputs-container">
                    <div class="input-group">
                        <label for="">Ваше имя</label>
                        <input type="text" class="your_name" placeholder="Ваше имя">
                    </div>
                    <div class="input-group">
                        <label for="">Ваш телефон</label>
                        <input type="text" class="your_phone" placeholder="Ваш телефон">
                    </div>
                     <div class="input-group radio_merka">
                        <div class="flex-radio">
                            <input type="radio" name="place_merit" class="radio_primerka" value="home" id="home_primerka"> <label class="label_radio_primerka" for="home_primerka">Буду мерить дома</label>
                        </div>
                         <div class="flex-radio">
                            <input type="radio" name="place_merit" class="radio_primerka" value="butik" id="butik_primerka"> <label class="label_radio_primerka" for="butik_primerka">Буду мерить в бутике</label>
                        </div>
                    </div>
                    <div class="input-group input-group-email disabled" style="padding-right: 1rem;">
                        <label for="">Ваш E-mail</label>
                        <input type="text" class="your_email" placeholder="Ваш E-mail">
                    </div>
                    <div class="input-group adr_primerka">
                        <label for="">Адрес доставки</label>
                        <input type="text" class="your_address" placeholder="Ваш адрес доставки">
                    </div>
                </div>
                <button class="btn btn-fullorange btn-square go-order-fit">Заказать примерку</button>
                <div id="message_order_fit" class="information_message"></div>
            </div>
        </div>
    </div>
</div>
<div class="popup oneclick">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h4">Заказать в 1 клик</p>
                <p class="h2">БАЛЕРО CHANEL</p>
                <br>
                <input type="hidden" class="product_id"/>
                <div class="inputs-container">
                    <div class="input-group">
                        <label for="">Ваше имя</label>
                        <input type="text" class="your_name" placeholder="Ваше имя">
                    </div>
                    <div class="input-group">
                        <label for="">Ваш телефон</label>
                        <input type="text" class="your_phone" placeholder="Ваш телефон">
                    </div>
                </div>
                <button class="btn btn-fullorange btn-square btn-fullwidth" >Оформить заказ</button>
                <div id="message_oneclick" class="information_message"></div>
            </div>
        </div>
    </div>
</div>
<!-- РЕГИСТРАЦИЯ -->
<?
    if(!$USER->IsAuthorized() && CModule::IncludeModule("socialservices"))
    {
        $oAuthManager = new CSocServAuthManager();
        $arServices = $oAuthManager->GetActiveAuthServices($arResult);

        if(!empty($arServices))
        {
            $arResult["AUTH_SERVICES"] = $arServices;
            if(isset($_REQUEST["auth_service_id"]) && $_REQUEST["auth_service_id"] <> '' && isset($arResult["AUTH_SERVICES"][$_REQUEST["auth_service_id"]]))
            {
                $arResult["CURRENT_SERVICE"] = $_REQUEST["auth_service_id"];
                if(isset($_REQUEST["auth_service_error"]) && $_REQUEST["auth_service_error"] <> '')
                {
                    $arResult['ERROR_MESSAGE'] = $oAuthManager->GetError($arResult["CURRENT_SERVICE"], $_REQUEST["auth_service_error"]);
                }
                elseif(!$oAuthManager->Authorize($_REQUEST["auth_service_id"]))
                {
                    $ex = $APPLICATION->GetException();
                    if ($ex)
                        $arResult['ERROR_MESSAGE'] = $ex->GetString();
                }
            }
        }
    }
    
?>
<? if(!$USER->IsAuthorized()){?>
<div class="popup reg">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h2">Регистрация на сайте</p>
                <div class="fast-reg">
                    <p>Быстрая регистрация через соцсети:</p>
                    <div>
                        <?
                            $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat",
                                array(
                                    "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                                    "SUFFIX"=>"form",
                                ),
                                $component,
                                array("HIDE_ICONS"=>"Y")
                            );
                        ?>
                    </div>
                </div>
                <form action="" onsubmit="console.log('form_send');ga('send', 'event', 'forms', 'registration_submit');ym(20825989,'reachGoal','registration_submit');">
                    <div class="inputs-container">
                        <div class="input-group">
                            <label for="">Ваше имя</label>
                            <input type="text" name="USER_NAME" <? if (strlen($_COOKIE['BITRIX_SM_USER_NAME'])>0) echo "value='".htmlspecialcharsbx($_COOKIE['BITRIX_SM_USER_NAME'])."'";?> placeholder="Имя">
                        </div>
                        <div class="input-group">
                            <label for="">Ваш E-mail</label>
                            <input type="text" name="USER_EMAIL" <? if (strlen($_COOKIE['BITRIX_SM_USER_EMAIL'])>0) echo "value='".htmlspecialcharsbx($_COOKIE['BITRIX_SM_USER_EMAIL'])."'";?> placeholder="E-mail">
                        </div>
                        <!--<div class="input-group">
                            <label for="">Введите Ваш телефон</label>
                            <input type="text" name="USER_PHONE" class="phone_masked_input" placeholder="Телефон">
                        </div>-->
                        <!--<div class="input-group">
                            <label for="">Введите Вашу дату рождения</label>
                            <input type="text" name="USER_BIRTHDAY" <? if (strlen($_COOKIE['BITRIX_SM_USER_BIRTHDAY'])>0) echo "value='".htmlspecialcharsbx($_COOKIE['BITRIX_SM_USER_BIRTHDAY'])."'";?> placeholder="День рождения" class="masked-birthday" data-position="bottom center">
                        </div>-->
                    </div>
                    <button class="btn btn-fullorange btn-square btn-fullwidth">Зарегистрироваться</button>
                </form>
                <br>
                <div class="open-enter-popup text-center">
                    <a href="">Уже зарегистрированы? Войти</a>
                </div>
                <p class="for_answer reg-info"></p>
                <p class="reg-info">Ваши контакты не попадут в руки 3-х лиц: мы бережно их храним в соответсвии с 152-ФЗ «О защите персональных данных»</p>
            </div>
        </div>
    </div>
</div>
<!-- ВХОД В ЛК -->
<div class="popup enter">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h2">Вход в личный кабинет</p>
                <br>
                <form action="">
                    <div class="inputs-container">
                        <div class="input-group">
                            <label for="">E-mail</label>
                            <input type="text" name="login" placeholder="E-mail">
                        </div>
                        <div class="input-group">
                            <label for="">Пароль</label>
                            <input type="password" name="password" placeholder="Пароль">
                        </div>
                    </div>
                    <button class="btn btn-fullorange btn-square btn-fullwidth">Войти</button>
                </form>
                <br>
                <p class="password_forgot text-center"><a href="javascript:void(0)" class="forgot_password">Забыли пароль?</a></p>
<!--                <span>Авторизация через соцсети:</span>-->
                <div class="enter-socials">
                    <?
                        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat",
                            array(
                                "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                                "SUFFIX"=>"form",
                            ),
                            $component,
                            array("HIDE_ICONS"=>"Y")
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ЗАБЫЛ ПАРОЛЬ -->
<div class="popup forgot">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h2">Забыли пароль?</p>
                <br>
                <form action="">
                <div class="inputs-container">
                    <div class="input-group">
                        <label for="">Ваш e-mail</label>
                        <input type="text" name="login" class="forgot_email" placeholder="Ваш e-mail">
                    </div>
                </div>
                <button class="btn btn-fullorange btn-square btn-fullwidth">Восстановить</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?} else{?>
<!-- ВЫХОД ИЗ ЛК -->
<div class="popup exit">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h4">Подтвердите выход из личного кабинета</p>
                <p class="h2">Выйти?</p>
                <div class="agreement">
                    <button class="btn btn-orange nowhite btn-square go_exit">Да</button>
                    <button class="btn btn-fullorange btn-square go_exit_cancel">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?}?>
<!--удаление адреса лк -->
<div class="popup deleteadr">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content exit-site">
                <p class="h2">Вы уверены, что хотите удалить адрес?</p>
                <div class="agreement">
                    <button class="btn btn-orange nowhite btn-square  yes">Да</button>
                    <button class="btn btn-fullorange btn-square  no">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--удалить сданную вещь-->
<div class="popup deletehandthing">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <p class="h4">Подтвердите удаление товара из продаж.</p>
                <p class="h2">Вы уверены?</p>
                <div class="agreement">
                    <button class="btn btn-orange nowhite btn-square  yes">Да</button>
                    <button class="btn btn-fullorange btn-square  no">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--попап для изображения-->
<div class="popup imgpopup">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <img src=""/>
            </div>
        </div>
    </div>
</div>

<!-- попап быстрого просмотра -->




<div class="popup quickview">
    <div class="popup-container">
        <div class="popup-loading"></div>
        <div class="popup-close-area"></div>
        <div class="popup-body">
            <div class="popup-close-button"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-close.svg' ?></div>
            <div class="popup-content">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="quickview-slider-wrapper">
                            <div class="quickview-slider-templ">
                                <div class="item">
                                    <img src="#" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="quickview-slider">
                                <div class="item">
                                    <img src="#" alt="" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="product-title">Парка Philipp Plein</div>
                        <div class="description product-desc">
                            Производитель: <a href="/brands/philipp-plein/" class="inverted-link"><color>Philipp Plein</color></a><br>
                            Состав: полиэстер<br>
                            Состояние: Хорошее<br>
                            Наличие: Есть <br>Бутик: Садовая, 28<br><br>
                            Размер: <a href="/sizes/">М</a>
                        </div>
                        <div class="product-inner-price">
                            <div class="prices-detail">
                                <p class="old-price">
                                    <span class="old-price-val">240000</span>
                                    <sup>
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="510.127px" height="510.127px" viewBox="0 0 510.127 510.127" style="enable-background:new 0 0 510.127 510.127;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M34.786,428.963h81.158v69.572c0,3.385,1.083,6.156,3.262,8.322c2.173,2.18,4.951,3.27,8.335,3.27h60.502
                                                        c3.14,0,5.857-1.09,8.152-3.27c2.295-2.166,3.439-4.938,3.439-8.322v-69.572h182.964c3.377,0,6.156-1.076,8.334-3.256
                                                        c2.18-2.178,3.262-4.951,3.262-8.336v-46.377c0-3.365-1.082-6.156-3.262-8.322c-2.172-2.18-4.957-3.27-8.334-3.27H199.628v-42.754
                                                        h123.184c48.305,0,87.73-14.719,118.293-44.199c30.551-29.449,45.834-67.49,45.834-114.125c0-46.604-15.283-84.646-45.834-114.125
                                                        C410.548,14.749,371.116,0,322.812,0H127.535c-3.385,0-6.157,1.089-8.335,3.256c-2.173,2.179-3.262,4.969-3.262,8.335v227.896
                                                        H34.786c-3.384,0-6.157,1.145-8.335,3.439c-2.172,2.295-3.262,5.012-3.262,8.151v53.978c0,3.385,1.083,6.158,3.262,8.336
                                                        c2.179,2.18,4.945,3.256,8.335,3.256h81.158v42.754H34.786c-3.384,0-6.157,1.09-8.335,3.27c-2.172,2.166-3.262,4.951-3.262,8.322
                                                        v46.377c0,3.385,1.083,6.158,3.262,8.336C28.629,427.887,31.401,428.963,34.786,428.963z M199.628,77.179h115.938
                                                        c25.6,0,46.248,7.485,61.953,22.46c15.697,14.976,23.549,34.547,23.549,58.691c0,24.156-7.852,43.733-23.549,58.691
                                                        c-15.705,14.988-36.354,22.473-61.953,22.473H199.628V77.179z"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </sup>
                                </p>
                                <p>
                                    <span class="price-val">59 000</span>
                                    <sup>
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="510.127px" height="510.127px" viewBox="0 0 510.127 510.127" style="enable-background:new 0 0 510.127 510.127;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M34.786,428.963h81.158v69.572c0,3.385,1.083,6.156,3.262,8.322c2.173,2.18,4.951,3.27,8.335,3.27h60.502
                                                        c3.14,0,5.857-1.09,8.152-3.27c2.295-2.166,3.439-4.938,3.439-8.322v-69.572h182.964c3.377,0,6.156-1.076,8.334-3.256
                                                        c2.18-2.178,3.262-4.951,3.262-8.336v-46.377c0-3.365-1.082-6.156-3.262-8.322c-2.172-2.18-4.957-3.27-8.334-3.27H199.628v-42.754
                                                        h123.184c48.305,0,87.73-14.719,118.293-44.199c30.551-29.449,45.834-67.49,45.834-114.125c0-46.604-15.283-84.646-45.834-114.125
                                                        C410.548,14.749,371.116,0,322.812,0H127.535c-3.385,0-6.157,1.089-8.335,3.256c-2.173,2.179-3.262,4.969-3.262,8.335v227.896
                                                        H34.786c-3.384,0-6.157,1.145-8.335,3.439c-2.172,2.295-3.262,5.012-3.262,8.151v53.978c0,3.385,1.083,6.158,3.262,8.336
                                                        c2.179,2.18,4.945,3.256,8.335,3.256h81.158v42.754H34.786c-3.384,0-6.157,1.09-8.335,3.27c-2.172,2.166-3.262,4.951-3.262,8.322
                                                        v46.377c0,3.385,1.083,6.158,3.262,8.336C28.629,427.887,31.401,428.963,34.786,428.963z M199.628,77.179h115.938
                                                        c25.6,0,46.248,7.485,61.953,22.46c15.697,14.976,23.549,34.547,23.549,58.691c0,24.156-7.852,43.733-23.549,58.691
                                                        c-15.705,14.988-36.354,22.473-61.953,22.473H199.628V77.179z"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </sup>
                                </p>
                            </div>
                            <div class="buttons-detail">
                                <button type="button" product_id="256948" product_name="Парка Philipp Plein" name="button" class="btn  btn-orange nowhite btn-oneclick btn-square  btn-block">Купить в 1 клик</button>
                                <br>
                                <!-- button id="addtocart256948" type="button" name="button" onclick="addtocart('256948','М','черный','Парка Philipp Plein'); return false;" class="btn  btn-fullorange btn-square btn-block addtocartid256948">В корзину</button -->
                                <button id="addtocart256948" type="button" name="button" class="btn  btn-fullorange btn-square btn-block addtocartid">В корзину</button>
                            </div>                            
                            <div class="links-detail">
                                <a class="order-fit" product_id="256948" product_name="Парка Philipp Plein" href="">Заказать примерку</a>
                            </div>
                            <div class="decription">
                                Доставим завтра курьером по СПБ - 400 ₽ Аутентичность гарантирована
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-gray btn-block btn-more">Больше информации о товаре</a>
        </div>
    </div>
</div>


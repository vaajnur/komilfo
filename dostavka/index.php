<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container static-page">
	<div class="row">
		<div class="col-sm-6">
 <img src="/images/dostavka_kartinka.png" class="img-responsive" style="margin-top: 0;" alt="">
		</div>
		<div class="col-sm-6">
			<p>
				 Komilfo осуществляет доставку почтой по предоплате по всему СНГ.
			</p>
			<p>
				 Для Санкт-Петербурга доступна доставка курьером, а также услуга заказа примерки товара на дом. Если товар не подойдет - его можно будет сразу вернуть курьеру, оплатив 400 руб. за доставку.
			</p>
			<table id="delivery-table">
			<thead>
			<tr>
				<td>
					 Способ доставки
				</td>
				<td>
					 Срок, количество дней
				</td>
				<td>
					 Стоимость в рублях
				</td>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					 Курьерская доставка СДЭК<br>
				</td>
				<td>
					 1-3
				</td>
				<td>
					 от 300 до 700
				</td>
			</tr>
			<tr>
				<td>
					 Почта России 1-го класса, ускоренная
				</td>
				<td>
					 Подробнее на сайте: <a href="http://russianpost.ru" target="_blank" class="inverted-link">russianpost.ru</a>
				</td>
				<td>
					 Подробнее на сайте: <a href="http://russianpost.ru" target="_blank" class="inverted-link">russianpost.ru</a>
				</td>
			</tr>
			<tr>
				<td>
					 Экспресс-доставка EMS
				</td>
				<td>
					 Подробнее на сайте: <a href="http://emspost.ru" target="_blank" class="inverted-link">emspost.ru</a>
				</td>
				<td>
					 Подробнее на сайте: <a href="http://emspost.ru" target="_blank" class="inverted-link">emspost.ru</a>
				</td>
			</tr>
			<tr>
				<td>
					 Курьером по Санкт-Петербургу
				</td>
				<td>
					 1-2
				</td>
				<td>
					 400
				</td>
			</tr>
			<tr>
				<td>
					 Почта России
				</td>
				<td>
					 3-8
				</td>
				<td>
					 500
				</td>
			</tr>
			</tbody>
			</table>
			<p>
				 Все заказы можно отследить по трек-номеру.
			</p>
			<p>
 <i>Внимание! Ответственность за сроки поставки несет почта. При нарушении сроков доставки, обращайтесь в отдел претензий почты.</i>
			</p>
			<p>
 <b>Способы оплаты:</b>
			</p>
			<ul>
				<li><a href="/kontakty/" target="_blank" class="inverted-link">Наличными в магазине в Санкт-Петербурге</a>;</li>
				<li>Переводом на карту Сбербанка или Альфа-банка;</li>
				<li>Переводом на счет Сбербанка или Альфа-банка;</li>
				<li>Переводом на Яндекс.Деньги, WebMoney, PayPal.</li>
			</ul>
			<p>
				 Заказ можно оформить на сайте или по тел.: <a class="comagic_phone3" href="tel:+78003336794">8 (800) 333-67-94</a>.
			</p>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
$APPLICATION->SetPageProperty("description", "Комиссионный магазин одежды и аксессуаров в Санкт-Петербурге. Комиссионный магазин элитной одежды в СПб. Комиссионный бутик бренды.");
$APPLICATION->SetPageProperty("keywords", "комиссионный магазин, одежда, бутик, брендовая, элитная, бу, санкт петербург, спб, доставка, цена, стоимость, недорого, дешево");
$APPLICATION->SetPageProperty("title", "Комиссионный магазин брендовой одежды в Санкт-Петербурге купить недорого бу");

?><? 
$url=$_SERVER['REQUEST_URI'];
$url=explode("?",$url);
$url=$url[0];
$url=preg_replace('/page+[0-9]{4}/i', '', $url, -1, $count);
$url=preg_replace('/page+[0-9]{3}/i', '', $url, -1, $count);
$url=preg_replace('/page+[0-9]{2}/i', '', $url, -1, $count);
$url=preg_replace('/page+[0-9]{1}/i', '', $url, -1, $count);
$url=str_replace("//","/",$url);
if(!isset($_REQUEST['NOVINKI'])&&!isset($_SESSION["SORTFIELD_1".$url])){
   /* $_SESSION["SORTFIELD_1".$url]="CATALOG_PRICE_2"; //catalog_PRICE_1 было по обычной цене
    $_SESSION["SORTFIELD_1_ASC".$url]="asc";*/
}
if(isset($_GET['sort'])){
    $APPLICATION->RestartBuffer();
    
    //echo $url;
   if($_GET["sort"]=="PRICE") { ?>
        <?
        $_SESSION["SORTFIELD_1".$url]="CATALOG_PRICE_2"; //catalog_PRICE_1 было по обычной цене
        $_SESSION["SORTFIELD_1_ASC".$url]=$_GET['order'];
        ?>
    <?}
    if($_GET["sort"]=="NAME") { ?>
            <?
            $_SESSION["SORTFIELD_1".$url]="NAME"; //catalog_PRICE_1 было по обычной цене
            $_SESSION["SORTFIELD_1_ASC".$url]=$_GET['order'];
            ?>
    <?}
    if($_GET['sort']=="NONE"){
        unset($_SESSION["SORTFIELD_1".$url]);
        unset($_SESSION["SORTFIELD_1_ASC".$url]);
        if(!isset($_REQUEST['NOVINKI'])){
          $_SESSION["SORTFIELD_1".$url]="id"; //catalog_PRICE_1 было по обычной цене
          $_SESSION["SORTFIELD_1_ASC".$url]="asc";
        }
    }
}
if(isset($_SESSION["SORTFIELD_1".$url]))
    $sort=$_SESSION["SORTFIELD_1".$url];
if(isset($_SESSION["SORTFIELD_1_ASC".$url]))
    $order=$_SESSION["SORTFIELD_1_ASC".$url];
?>
<div class="top-indent"></div>
    <?
    //фильтр по цене
    $GLOBALS['CATALOG_FILTER']=array(">CATALOG_PRICE_2"=>0,"CATALOG_AVAILABLE"=>"Y");
    if(isset($_GET['filter'])){
        $APPLICATION->RestartBuffer();
        //echo $url;
       if($_GET["filter"]=="PRICE") { ?>
                <?
                $_SESSION["FILTERPRICEMIN".$url]=$_GET['MIN_PRICE'];
                $_SESSION["FILTERPRICEMAX".$url]=$_GET['MAX_PRICE'];
                ?>
        <?}
         else if($_GET['filter']=="NONE"){
            unset($_SESSION["FILTERPRICEMIN".$url]);
            unset($_SESSION["FILTERPRICEMAX".$url]);
            unset($_SESSION["FILTERCML2_MANUFACTURER".$url]);
            unset($_SESSION["FILTERVALUESCML2_MANUFACTURER".$url]);
            unset($_SESSION["FILTERIZNOS".$url]);
            unset($_SESSION["FILTERVALUESIZNOS".$url]);
            unset($_SESSION["FILTERSOSTAV".$url]);
            unset($_SESSION["FILTERVALUESSOSTAV".$url]);
            unset($_SESSION["FILTERTSVET".$url]);
            unset($_SESSION["FILTERVALUESTSVET".$url]);
			unset($_SESSION["FILTERRAZMER_TOVARA".$url]);
            unset($_SESSION["FILTERVALUESRAZMER_TOVARA".$url]);
			unset($_SESSION["FILTERAKTSIYA".$url]);
			unset($_SESSION["FILTERVALUESAKTSIYA".$url]);
        } 
        else{
            $arfil=explode(";",$_GET['VALUES']);
            unset($arfil[count($arfil)-1]);
            $_SESSION["FILTERVALUES".$_GET['filter'].$url]=$arfil;
            if(count($arfil)==0)
                unset($_SESSION["FILTERVALUES".$_GET['filter'].$url]);
        }
    }
    if(isset($_SESSION["FILTERVALUESCML2_MANUFACTURER".$url]))
        $GLOBALS['CATALOG_FILTER']["PROPERTY_CML2_MANUFACTURER_VALUE"]= $_SESSION["FILTERVALUESCML2_MANUFACTURER".$url];
	if(isset($_SESSION["FILTERVALUESAKTSIYA".$url])){
        $GLOBALS['CATALOG_FILTER']["PROPERTY_AKTSIYA"]= $_SESSION["FILTERVALUESAKTSIYA".$url];
	}
        
   /* if(isset($_SESSION["FILTERVALUESIZNOS".$url]))
        $GLOBALS['CATALOG_FILTER']["PROPERTY_IZNOS"]= $_SESSION["FILTERVALUESIZNOS".$url];*/
    if(isset($_SESSION["FILTERVALUESSOSTAV".$url]))
        $GLOBALS['CATALOG_FILTER']["PROPERTY_SOSTAV"]= $_SESSION["FILTERVALUESSOSTAV".$url];
    if(isset($_SESSION["FILTERVALUESTSVET".$url]))
        $GLOBALS['CATALOG_FILTER']["PROPERTY_TSVET"]= $_SESSION["FILTERVALUESTSVET".$url];
 if(isset($_SESSION["FILTERVALUESRAZMER_TOVARA".$url]))
        $GLOBALS['CATALOG_FILTER']["PROPERTY_RAZMER_TOVARA"]= $_SESSION["FILTERVALUESRAZMER_TOVARA".$url];
    if(isset($_SESSION["FILTERPRICEMIN".$url]))
        $GLOBALS['CATALOG_FILTER'][">=CATALOG_PRICE_2"]=$_SESSION["FILTERPRICEMIN".$url];
    if(isset($_SESSION["FILTERPRICEMAX".$url]))
        $GLOBALS['CATALOG_FILTER']["<=CATALOG_PRICE_2"]=$_SESSION["FILTERPRICEMAX".$url];
    //echo htmlspecialcharsbx($_SESSION["FILTERCML2_MANUFACTURER".$url]);
    $SET404="Y";
    //если страница Каталог
    if($_SERVER['REQUEST_URI']=="/catalog/"){
         $SET404="N";
        $APPLICATION->AddChainItem("Каталог", "");
        $APPLICATION->SetTitle("Женская одежда и обувь");
    }
    //если страница Новинки
    if(isset($_REQUEST['NOVINKI'])){
         $SET404="N";
        //TODO поменять дату на date("d.m.Y H:i:s",strtotime("-7 days"));
        $GLOBALS['CATALOG_FILTER'][">DATE_CREATE"]= date("d.m.Y H:i:s",strtotime("-30 days"));
        //$APPLICATION->AddChainItem("Новинки", "");
        $APPLICATION->SetTitle("Новинки");
    }
    //если страница Поиск
    if(isset($_REQUEST['SEARCH'])){
        $SET404="N";
        $APPLICATION->AddChainItem("Поиск", "");
        $APPLICATION->SetTitle("Поиск");
        if(isset($_REQUEST['q'])){
            $_SESSION['SEARCH'.$url]=$_REQUEST['q'];
        }
        if(isset($_SESSION['SEARCH'.$url])){
            $_REQUEST['q']=$_SESSION['SEARCH'.$url];
        }
        
        
    }
    //если страница бренда
    if(isset($_REQUEST['BRAND'])){
      $SET404="N";
      $arSelect = Array("ID", "NAME","DETAIL_TEXT");
      $arFilter = Array("IBLOCK_ID"=>13,"CODE"=>$_REQUEST['BRAND']);
      $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
      $found=false;
      while($ob = $res->GetNextElement())
      {
          
          $arFi=$ob->GetFields();
		  $GLOBALS['SEO-TEXT']=$arFi['DETAIL_TEXT'];
          if(strlen($arFi['NAME'])>0)
              $found=true;
          $GLOBALS['CATALOG_FILTER']["PROPERTY_CML2_MANUFACTURER_VALUE"]= $arFi['NAME'];
          $APPLICATION->AddChainItem($arFi['NAME'], "");
          $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(
               13, // ID инфоблока
               $arFi["ID"] // ID элемента
           );
            $arElMetaProp = $ipropValues->getValues();
          if(strlen($arElMetaProp['ELEMENT_META_TITLE'])>0){
             $APPLICATION->SetTitle($arElMetaProp['ELEMENT_META_TITLE']); 
          }
          else{
              $APPLICATION->SetTitle($arFi['NAME']);
          }
          if(strlen($arElMetaProp['ELEMENT_META_DESCRIPTION'])>0){
             $APPLICATION->SetPageProperty("description",$arElMetaProp['ELEMENT_META_DESCRIPTION']); 
          }
          if(strlen($arElMetaProp['ELEMENT_PAGE_TITLE'])>0){
             $APPLICATION->SetPageProperty("title",$arElMetaProp['ELEMENT_PAGE_TITLE']); 
          }
      }
        if($found==false){
            $SET404="Y";
        }
    }

// Остатки по складам
if($city != ''){
	$store_am = getStoreAmountAll();
	$city = strtolower($city);
	if(isset($store_am[$city]))
		$GLOBALS['CATALOG_FILTER']['=ID'] = $store_am[$city];
	else
		$GLOBALS['CATALOG_FILTER']['=ID'] = $store_am['санкт-петербург'];

}    
    
    $APPLICATION->IncludeComponent(
	"komilfo:catalog", 
	".default", 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => CATALOG_IBLOCK_ID_CONST,
		"TEMPLATE_THEME" => "site",
		"HIDE_NOT_AVAILABLE" => "Y",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "86400", // 24 часа 
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "Y",
		"ADD_SECTION_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"SET_STATUS_404" => $SET404,
		"DETAIL_DISPLAY_NAME" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "CATALOG_FILTER",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "CML2_MANUFACTURER",
			1 => "IZNOS",
			2 => "SOSTAV",
			3 => "TSVET",
			4 => "RAZMER",
			5 => "AKTSIYA",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "BASE",
		),
		"FILTER_OFFERS_FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"FILTER_OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"USE_REVIEW" => "Y",
		"MESSAGES_PER_PAGE" => "10",
		"USE_CAPTCHA" => "Y",
		"REVIEW_AJAX_POST" => "Y",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"FORUM_ID" => "1",
		"URL_TEMPLATES_READ" => "",
		"SHOW_LINK_TO_FORUM" => "Y",
		"USE_COMPARE" => "N",
		"PRICE_CODE" => array(
			0 => "Интернет - магазин",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "Y",
		"CONVERT_CURRENCY" => "N",
		"QUANTITY_FLOAT" => "N",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "SIZES_SHOES",
			1 => "SIZES_CLOTHES",
			2 => "COLOR_REF",
		),
		"SHOW_TOP_ELEMENTS" => "N",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_TOP_DEPTH" => "1",
		"SECTIONS_VIEW_MODE" => "TILE",
		"SECTIONS_SHOW_PARENT_NAME" => "N",
		"PAGE_ELEMENT_COUNT" => "20",
		"LINE_ELEMENT_COUNT" => "1",
		"ELEMENT_SORT_FIELD" => strlen($sort)>0?$sort:"HAS_DETAIL_PICTURE",
		"ELEMENT_SORT_ORDER" => strlen($order)>0?$order:"desc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "NEWPRODUCT",
			2 => "SALELEADER",
			3 => "SPECIALOFFER",
			4 => "AKTSIYA",
		),
		"INCLUDE_SUBSECTIONS" => "Y",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"LIST_OFFERS_PROPERTY_CODE" => array(
			0 => "SIZES_SHOES",
			1 => "SIZES_CLOTHES",
			2 => "COLOR_REF",
			3 => "MORE_PHOTO",
			4 => "ARTNUMBER",
			5 => "",
		),
		"LIST_OFFERS_LIMIT" => "0",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "TSENA_V_OFITS_BUTIKE",
			1 => "VIDEO",
			2 => "CML2_MANUFACTURER",
			3 => "RAZMER_TOVARA",
			4 => "TSVET",
			5 => "GOODS",
		),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"DETAIL_OFFERS_PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
			3 => "COLOR_REF",
			4 => "MORE_PHOTO",
			5 => "",
		),
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"USE_ALSO_BUY" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "4",
		"ALSO_BUY_MIN_BUYES" => "1",
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "desc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_TEMPLATE" => "round",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "Y",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"LABEL_PROP" => "-",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "SIZES_SHOES",
			1 => "SIZES_CLOTHES",
			2 => "COLOR_REF",
			3 => "",
		),
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_USE_VOTE_RATING" => "Y",
		"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
		"DETAIL_USE_COMMENTS" => "Y",
		"DETAIL_BLOG_USE" => "Y",
		"DETAIL_VK_USE" => "N",
		"DETAIL_FB_USE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"USE_STORE" => "Y",
		"FIELDS" => array(
			0 => "SCHEDULE",
			1 => "STORE",
			2 => "",
		),
		"USE_MIN_AMOUNT" => "N",
		"STORE_PATH" => "/store/#store_id#",
		"MAIN_TITLE" => "Наличие на складах",
		"MIN_AMOUNT" => "10",
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_BRAND_PROP_CODE" => array(
			0 => "",
			1 => "BRAND_REF",
			2 => "",
		),
		"SIDEBAR_SECTION_SHOW" => "Y",
		"SIDEBAR_DETAIL_SHOW" => "Y",
		"SIDEBAR_PATH" => "/catalog/sidebar.php",
		"COMPONENT_TEMPLATE" => ".default",
		"COMMON_SHOW_CLOSE_POPUP" => "N",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"DETAIL_BLOG_URL" => "catalog_comments",
		"DETAIL_BLOG_EMAIL_NOTIFY" => "N",
		"DETAIL_FB_APP_ID" => "",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"USE_SALE_BESTSELLERS" => "Y",
		"INSTANT_RELOAD" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
		"COMMON_ADD_TO_BASKET_ACTION" => "undefined",
		"TOP_ADD_TO_BASKET_ACTION" => "ADD",
		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
		"DETAIL_ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
		),
		"DETAIL_SHOW_BASIS_PRICE" => "Y",
		"SECTIONS_HIDE_SECTION_NAME" => "N",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"SHOW_DEACTIVATED" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"USE_GIFTS_DETAIL" => "Y",
		"USE_GIFTS_SECTION" => "Y",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
		"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"STORES" => array(
		),
		"USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_EMPTY_STORE" => "Y",
		"SHOW_GENERAL_STORE_INFORMATION" => "N",
		"USE_BIG_DATA" => "Y",
		"BIG_DATA_RCM_TYPE" => "bestsell",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "catalog/",
			"section" => "#SECTION_CODE_PATH#/",
			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
			"compare" => "compare/",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		)
	),
	false
);?>
<?$APPLICATION->AddChainItem($GLOBALS['CHAINELEMENT'], "");?>   
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
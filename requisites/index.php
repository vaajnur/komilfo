<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Реквизиты");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container static-page">
	<div class="row">
		<div class="col-sm-12">
			<table id="requisites">
				<tbody>
					<tr>
						<td>Название компании</td>
						<td>ИП Федотовский А.В</td>
					</tr>
					<tr>
						<td>ИНН/КПП</td>
						<td>471506571856</td>
					</tr>
					<tr>
						<td>ОГРН</td>
						<td>308471521000046</td>
					</tr>
					<tr>
						<td>Юридический адрес</td>
						<td>Ленинградская область, Тихвинский р-н, д. Черноваткино, 19</td>
					</tr>
					<tr>
						<td>Фактический адрес</td>
						<td>Ленинградская область, Тихвинский р-н, д. Черноваткино,19</td>
					</tr>
					<tr>
						<td>р/с</td>
						<td>40802810032030000000</td>
					</tr>
					<tr>
						<td>к/с</td>
						<td>30101810600000000786</td>
					</tr>
					<tr>
						<td>Банк</td>
						<td>ОАО "Альфа-банк"</td>
					</tr>
					<tr>
						<td>БИК</td>
						<td>044030786</td>
					</tr>
					<tr>
						<td>Электронная почта</td>
						<td>info@komilfo-butik.com</td>
					</tr>
					<tr>
						<td>Телефон</td>
						<td>8 (921) 961-30-40</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сдать вещи");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container static-page">
	 <!-- <p class="information-text">
		Уважаемые клиенты Комильфо!
		В связи с введением самоизоляции по карантину и ЧС,
		которое повлекло за собой ограничение торговой деятельности компании Комильфо,
		с 28 го марта компания уходит на карантин и всю возможную деятельность переводит в онлайн.
		Приносим извинения за предоставленные неудобства.
		Берегите себя и своих близких!
	</p> -->
	<h2 style="text-align: center;">Избавь свой гардероб от ненужных <br>
	 вещей и заработай на них.</h2>
	<p style="text-align: center;">
	</p>
	<p style="font-size: 16px;text-align: center;">
		 Продадим Ваши вещи в нашем люксовом комиссионном бутике.
	</p>
	<form class="handthings-form">
		<div class="container handthings-container">
			<div class="our-stats">
				<p>
					 Приняли на комиссию:<b>40 537</b>вещей
				</p>
				<p>
					 Продали:<b>22 852</b>вещи
				</p>
				<p>
					 Выплатили:<b>95 391 895</b>рублей
				</p>
				<p>
					 У нас продается<b>каждая вторая</b>вещь
				</p>
			</div>
			<div class="row text-content">
				<div class="col-md-6 col-sm-6 col-xs-12 hadnling-text">
					<h4>Какие вещи нам подходят?</h4>
					<p>
						 Мы очень трепетно относимся к нашему ассортименту, стремясь максимально радовать наших клиентов. На комиссию принимаем только оригинальные вещи известных дизайнеров класса luxe. В идеальном или очень хорошем состоянии - без дефектов и повреждений.
					</p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 hadnling-text">
					<h4>Когда и где можно сдать вещи?</h4>
					<p>
						 Предварительную оценку вещей можно получить заполнив форму приема вещей на сайте. Пункт приема вещей находится в Санкт-Петербурге на Садовой, 28. 4 этаж, офис 18 и на Малой Посадской, 2. График работы: с понедельника по пятницу с 14:00 до 19:00. Контактный телефон: <a href="tel:+79819851641">8 (981) 985-16-41 </a>
					</p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 hadnling-text">
					<h4>От чего зависит цена?</h4>
					<p>
						 Каждая вещь оценивается индивидуально – исходя из ее качества, актуальности, состояния, а также Ваших пожеланий. Наша комиссия составляет от 30%.
					</p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 hadnling-text">
					<h4>Как мы продаем вещи?</h4>
					<p>
						 Для каждой вещи мы делаем профессиональные фотографии, приводим в порядок при необходимости, отправляя в ателье, размещаем у нас на сайте, группе ВКонтакте, Инстаграме, и на Авито, делая все, чтобы Ваша вещь продалась.
					</p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 hadnling-text">
					<h4>Можно ли забрать вещи обратно?</h4>
					<p>
						 Вы можете в любой момент забрать вещи, оплатив стоимость наших расходов в 300 руб за вещь, после 3-х месяцев вещи возвращаются бесплатно.
					</p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 hadnling-text">
					<h4>А какие бренды принимаете?</h4>
					<p>
						 Мы принимаем вещи от большинства известных брендов, вы всегда можете <a href="/brands/" class="inverted-link"> посмотреть список брендов</a>.
					</p>
				</div>
			</div>

			<h2 style="text-align: center;">Таблица ценообразования</h2>

			<div class="row">
				<div class="col">
					<table class="table pricing-table">
						<tbody>
						<tr>
							<th scope="row">Срок договора</th>
							<td>180 дней</td>
						</tr>
						<tr>
							<th scope="row">Процент комиссии</th>
							<td class="pricing-table__wrap">
						<table class="table table-bordered pricing-table__inner">
							<tbody>
							<tr>
								<td>До 5000 руб</td>
								<td class="font-weight-bold"><span>70%</span></td>
							</tr>
							<tr>
								<td>5-20 тыс руб</td>
								<td class="font-weight-bold"><span>60%</span></td>
							</tr>
							<tr>
								<td>20-50 тыс руб</td>
								<td class="font-weight-bold"><span>50%</span></td>
							</tr>
							<tr>
								<td>50-100 тыс руб</td>
								<td class="font-weight-bold"><span>40%</span></td>
							</tr>
							<tr>
								<td>От 100 тыс руб</td>
								<td class="font-weight-bold"><span>30%</span></td>
							</tr>
						</tbody>
					</table>
				</td>
				</tr>
				<tr>
				<th scope="row">Уценка</th>
				<td>20% после 60 дней без уведомления</td>
				</tr>
				<tr>
				<th scope="row">Хранение (возврат)</th>
				<td>После уведомления: 14 дней бесплатно, после — 10 рублей в день</td>
				</tr>
				<tr>
				<th scope="row">По всем вопросам пишите</th>
				<td>+ 7 (921) 912-49-99 (WhatsApp)</td>
				</tr>
				</tbody>
			</table>
			<div class="download-table">
				<a href="/upload/pricing.docx" class="pricing-table-download" download>Скачать таблицу ценообразования
					<img src="/images/komilfo/download.svg" alt="">
				</a>	
			</div>
			</div>
		</div>


			<p class="h2 text-center">
				 Заполните заявку на прием вещей,<br>
				 и получите оценку стоимости на почту:
			</p>
			 <!-- <div class="hs-group top-hs-group">
				  Егор<span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP</span></span>" placeholder="Ваше имя"> 89681921759<span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP</span></span>" placeholder="Ваш телефон"> komilfo.alex@gmail.com<span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP</span></span>" placeholder="Ваш email">
			</div> -->
			<div class="hs-group top-hs-group">
 <input value="" name="name" placeholder="Ваше имя"> <input value="" name="phone" placeholder="Ваш телефон"> <input value="" name="email" placeholder="Ваш email">
			</div>
			<div class="handthings">
				<div class="handthings-inputs">
 <button onclick="deletehandthings(this); return false;" class="region-delete handthings-delete" style="display: none;"></button>
					<div class="hs-group">
						<div class="hs-img hs-img-1">
							<div class="hs-add-button">
								<div class="hs-add-icon-c">
									<div class="hs-add-icon">
									</div>
								</div>
								 Фото 1
							</div>
 <input type="file" name="img1[]" style="display:none">
						</div>
						<div class="hs-img hs-img-2">
							<div class="hs-add-button">
								<div class="hs-add-icon-c">
									<div class="hs-add-icon">
									</div>
								</div>
								 Фото 2
							</div>
 <input type="file" name="img2[]" style="display:none">
						</div>
					</div>
					<div class="hs-group">
 <input class="txt-size" type="text" name="size[]" placeholder="Укажите размер"> <input class="txt-price" type="text" name="price[]" placeholder="Цена продажи">
					</div>
 <textarea class="txt-description" name="description[]" id="" cols="30" rows="10" placeholder="Опишите вещь, которую вы хотите сдать"></textarea>
				</div>
				<div class="handthings-add">
					<div class="hs-add-button">
						<div class="hs-add-icon-c">
							<div class="hs-add-icon">
							</div>
						</div>
 <span>Добавить<br>
						 еще вещь</span>
					</div>
				</div>
			</div>
			<div class="handthings-errors-text">
			</div>
 <button class="btn btn-fullorange btn-fullwidth go-handthings">Отправить</button>
		</div>
		 <!--<<div class="no-time">
		   <div class="text-content-garderob">
			   <h2>Если у вас большой гардероб
				   <br>
				   и нет времени приехать к нам
			   </h2>
			   <p>Вы можете оставить заявку на вызов оценщика, и он приедет и примет вещи на комиссию у Вас дома</p>
			   <button class="btn btn-fullorange">Оставить заявку</button>
		   </div>
		   <img src="/images/komilfo/gard.jpg" alt="">
		   </div>
	   </div> -->
		<div class="sign-up no-timer">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<div class="bg-container">
							<h2>
							Если у вас большой гардероб <br>
							 и нет времени приехать к нам </h2>
							<p>
								 Вы можете оставить заявку на вызов оценщика,<br>
								 и он приедет и примет вещи на комиссию у Вас дома.
							</p>
							<div class="btn btn-fullorange hdn-item">
								 Оставить заявку
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container about-butik">
			<div class="row">
				<div class="col-md-8">
					<h2>О бутике</h2>
					<p>
						 Комильфо - сеть комиссионных бутиков. Мы работаем уже 7 лет, занимаясь исключительно брендами класса люкс. Принимаем вещи от тех, кому они уже не нужны: не подходят по размеру или по стилю, которые перестали нравиться или которые уже больше не хочется надевать, но они продолжают занимать много места в гардеробе. И находим им новых хозяев.
					</p>
					<p>
						 Выставляем вещи в наших шоу-румах на 200 кв. м, с посещаемостью более 5000 человек каждый месяц, на нашем сайте, с посещаемостью более 10 000 человек, в <a href="#">Instagram</a> и <a href="#">ВКонтакте</a>
					</p>
				</div>
			</div>
			<div class="row gallery-butik">
				<div class="col-md-3 col-sm-3 col-xs-6">
 <img src="/images/komilfo/img1.png" alt="">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
 <img src="/images/komilfo/img2.png" alt="">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
 <img src="/images/komilfo/img3.png" alt="">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
 <img src="/images/komilfo/img4.png" alt="">
				</div>
			</div>
		</div>
		<div class="container static-page">
			<div class="row">
				<div class="col-xs-12">
					<table cellpadding="0" cellspacing="0" style="color:#2b587a;">
					<tbody>
					<tr>
						<td>
 <b>Бутик на Садовой:</b> г. Санкт-Петербург, м. Гостиный двор, ул. Садовая д. 28, Угол Садовой улицы и улицы Ломоносова.
						</td>
					</tr>
					<tr>
						<td>
 <b>Бутик на Малой Посадской:</b> г. Санкт-Петербург, м. Горьковское, ул. Малая Посадская д.2.
						</td>
					</tr>
					<tr>
						<td>
							 &nbsp;
						</td>
					</tr>
					</tbody>
					</table>
					<div>
						 &nbsp;
					</div>
					<div>
						<table class="contacts-table" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td valign="top">
 <b>Позвоните нам:</b>
							</td>
							<td valign="top">
 <b>Напишите нам:</b>
							</td>
						</tr>
						<tr>
							<td valign="top">
 <span class="autogrow-textarea comagic_phone3">8 (800) 333-67-94</span>
							</td>
							<td valign="top">
 <a href="mailto:info@komilfo-butik.com">info@komilfo-butik.com</a>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<p>
									 График работы бутика:
								</p>
							</td>
							<td valign="top">
								<p>
									 Ежедневно, с 11:00 до 21:00;
								</p>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<p>
									 График приема вещей:
								</p>
							</td>
							<td valign="top">
								<p>
									 С понедельника по пятницу, с 14:00 до 19:00;
								</p>
							</td>
						</tr>
						</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="yandex-map">
								 <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa24701967aecb748f0527994efbe4e0a8a7bf454ce46af9f554cd742607179a6&amp;width=100%&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="yandex-map">
								 <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7dfa7e2ffa83577f556ac5464a6ccdf457b71b09382506fc36cd46d091886ba3&amp;width=100%25&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- parts/footer.html -->
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
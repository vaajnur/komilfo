<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои продажи");?><?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
  )
);?>
<div class="lk-indent"></div>
<div class="container">
	<?
	if((isset($_REQUEST['PHONE'])&&isset($_REQUEST['CODE'])&&$_REQUEST['PHONE']==$_SESSION['PHONE']&&$_REQUEST["CODE"]==$_SESSION['CODE'])||(isset($_REQUEST['PHONE']))){
		$phone8="238388398948932";
		if(strpos($_REQUEST['PHONE'],"+7")!==false)
			$phone8=preg_replace("/[^0-9]/", '', str_replace("+7","8",$_REQUEST['PHONE']));
        ?>
        <div class="row">
            <div class="col-md-12"> 
				<button class="enroll_paymenthand">Записаться на выплату</button>
				<button class="enroll_backhand">Записаться на возврат</button>
                <?php
                $filter = Array("PERSONAL_PHONE" => array($_REQUEST['PHONE'],preg_replace("/[^0-9]/", '', $_REQUEST['PHONE']),$phone8));
                $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
                while ($arUser = $rsUsers->Fetch()) {
                  $arSpecUser = $arUser;
                }
                ?>
                <input type="hidden" class="user_id" value="<?=$_REQUEST['PHONE'];?>"/>
            <?
				/* ЗДЕСЬ ПРОДАЖИ */
				$arFilter = array('IBLOCK_ID' => 22,"UF_PHONE_CLIENT"=>array($_REQUEST['PHONE'],preg_replace("/[^0-9]/", '', $_REQUEST['PHONE']),$phone8));
				$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);?>
				<table id="lk-order_table">
					<thead>
						<tr>
							<th>Номер документа</th>
							<th>Товар</th>
							<th>Кол-во</th>
							<!--<th>Цена</th>
							<th>Сумма</th>-->
							<th>Статус</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tbody>
				<?
				$goods=array();
				$docs=false;
				while ($arSection = $rsSections->Fetch())
				{
					$docs=true;
					$arSelect = Array();
					$arFilter = Array("IBLOCK_ID"=>22, "SECTION_ID"=>$arSection['ID']);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
					$goods=array();
					$index=0;
					while($ob = $res->GetNextElement())
					{
						$arFields = $ob->GetFields();
						$arProps = $ob->GetProperties();
						$goods[$index]['name']=$arFields['NAME'];
						$goods[$index]['amount']=$arProps['amount']['VALUE'];
						$goods[$index]['amount_back']=$arProps['amount_back']['VALUE'];
						$goods[$index]['good']=$arProps['good']['VALUE'];
						$goods[$index]['price']=$arProps['price']['VALUE'];
						$goods[$index]['cost']=$arProps['cost']['VALUE'];
						$goods[$index]['measurement']=$arProps['measurement']['VALUE'];
						$index++;
					}
					?><? foreach($goods as $key=>$good){?>
							<tr id="bx_2592568946_177494">
								<? if ($key==0){?>
								<td rowspan="<?=count($goods);?>"><?=$arSection['NAME'];?></td>
								<?}?>
								<td>
								<?
								
								$arSelect = Array("ID","NAME","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER");
								$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST, "ID"=>$good['good'],'ACTIVE'=>"Y","CATALOG_AVAILABLE"=>"Y");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
								if(strlen($good['good'])>0&&$ob = $res->GetNextElement())
								{
								$arFields=$ob->GetFields();
									?>
								<a href="<?=$arFields['DETAIL_PAGE_URL'];?>"><?=explode(" ",$arFields['NAME'])[0]." ".$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];?></a>
								<?} else{?>
								<?=$good['name'];?>
								<?}?>
								
								</td>
								<td><?=$good['amount']." ".$good['measurement'];?>.</td>
								<!--<td><?=$good['price'];?></td>
								<td><?=$good['cost'];?></td>-->
								<?
								$arSelect = Array("ID","NAME","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER");
								$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST, "ID"=>$good['good'],'ACTIVE'=>"Y","CATALOG_AVAILABLE"=>"Y");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
								if(strlen($good['good'])>0&&$ob = $res->GetNextElement())
								{?>
									<? if(intval($good['amount'])-intval($good['amount_back'])<=0){
											?><td>Возврат</td><?
										}
										else if(intval($good['amount'])>intval($good['amount_back'])&&intval($good['amount_back'])>0){
											?><td>Частично продан</td><?
										}
										else{?>
											<td>В наличии</td>
										<?}?>
								<?} else{
									if(strlen($good['good'])==0){?>
									<td>Нет в продаже</td>
									<?} else{?>
										<? if(intval($good['amount'])-intval($good['amount_back'])<=0){
											?><td>Есть возврат</td><?
										}
										else if(intval($good['amount'])>intval($good['amount_back'])&&intval($good['amount_back'])>0){
											?><td>Частично продан</td><?
										}
										else{?>
											<td>Продан</td>
										<?}?>
									<?}?>
								<?}?>
							</tr>
							<?}?>

					<?}?>
					<? if($docs===false){?>
						<tr><td colspan="6">У вас еще нет продаж</td></tr>
					<?}?>
				</tbody>
			</table>
			<? ?>
            </div> 
        </div>
    <?} else{?>
        <div class="row handthings-check"> 
            <div class="col-md-9">
				<p class="fieald_title">Номер телефона:</p>
                <input type="text" class="gray_field user_phone phone_masked_input" value="" tabindex="4">
                <div class="approve-sms-block">
                    <button class="sms-approve-button approve-phone">Подтвердить телефон</button>
                    <div class="code-sms" style="display:none;">
						<p class="fieald_title">Код:</p>
                        <input type="text" class="gray_field code-sms-val"/>
                        <button class="sms-approve-button-handthings enter-code-handthings">Ввести код</button>
                    </div>
                </div>
            </div>
        </div>
			<div class="row progressrow">
				<div class="col-md-3">
					<p>
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M18.48 22.926l-1.193.658c-6.979 3.621-19.082-17.494-12.279-21.484l1.145-.637 3.714 6.467-1.139.632c-2.067 1.245 2.76 9.707 4.879 8.545l1.162-.642 3.711 6.461zm-9.808-22.926l-1.68.975 3.714 6.466 1.681-.975-3.715-6.466zm8.613 14.997l-1.68.975 3.714 6.467 1.681-.975-3.715-6.467z"/></svg>
						Вводите и подтверждаете телефон.</p>
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M8.122 24l-4.122-4 8-8-8-8 4.122-4 11.878 12z"/></svg>
				</div>
				<div class="col-md-3">
					<p><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M11 6v8h7v-2h-5v-6h-2zm10.854 7.683l1.998.159c-.132.854-.351 1.676-.652 2.46l-1.8-.905c.2-.551.353-1.123.454-1.714zm-2.548 7.826l-1.413-1.443c-.486.356-1.006.668-1.555.933l.669 1.899c.821-.377 1.591-.844 2.299-1.389zm1.226-4.309c-.335.546-.719 1.057-1.149 1.528l1.404 1.433c.583-.627 1.099-1.316 1.539-2.058l-1.794-.903zm-20.532-5.2c0 6.627 5.375 12 12.004 12 1.081 0 2.124-.156 3.12-.424l-.665-1.894c-.787.2-1.607.318-2.455.318-5.516 0-10.003-4.486-10.003-10s4.487-10 10.003-10c2.235 0 4.293.744 5.959 1.989l-2.05 2.049 7.015 1.354-1.355-7.013-2.184 2.183c-2.036-1.598-4.595-2.562-7.385-2.562-6.629 0-12.004 5.373-12.004 12zm23.773-2.359h-2.076c.163.661.261 1.344.288 2.047l2.015.161c-.01-.755-.085-1.494-.227-2.208z"/></svg>
						Получаете статусы ваших вещей.</p>
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M8.122 24l-4.122-4 8-8-8-8 4.122-4 11.878 12z"/></svg>
				</div>
				<div class="col-md-3">
					<p>
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M4.062 16.626c-.165-.5.038-.921.656-1.145l3.249-1.155-.134 1.028c-.077.589-.103 1.107-.089 1.573-.25.078-1.095.339-1.945.552-.871.218-1.538-.25-1.737-.853zm1.745 1.916c-.461.167-.612.48-.489.854.147.449.645.798 1.294.635.606-.151 1.408-.396 1.701-.487-.203-.505-.378-1.035-.479-1.659l-2.027.657zm.193-10.542h2.643l.128-1h-2.771v1zm16-6v9c0 1.104-.896 2-2 2h-1.989l.015.02c-.031.43-.105.906-.222 1.457-.451 2.144-1.637 5.122-.83 8.418-2.312.356-3.651.523-5.992 1.105-.273-4.062-2.266-4.943-1.804-8.47.542-4.137.844-6.461 1.196-9.255.11-.879.731-1.307 1.337-1.307.631 0 1.246.464 1.252 1.366.021 3.303.108 6.593.324 7.393.146.54 1.087.638 1.087-.512l-.002-2.216h5.128c.276 0 .5-.224.5-.5v-4.499h-5.568c-.157-1.461-1.27-2.531-2.721-2.531h-.001c-1.44 0-2.578 1.038-2.806 2.531h-4.904v4.5c0 .276.224.5.5.5h3.757l-.261 2h-3.996c-1.104 0-2-.896-2-2v-9c0-1.104.896-2 2-2h16c1.104 0 2 .896 2 2zm-2 .5c0-.276-.224-.5-.5-.5h-15c-.276 0-.5.224-.5.5v.5h16v-.5zm-14 7.5h2.386l.129-1h-2.515v1zm12-3h-3v1h3v-1z"/></svg>
						Записываетесь на выплату или возврат.</p>
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M8.122 24l-4.122-4 8-8-8-8 4.122-4 11.878 12z"/></svg>
				</div>
				<div class="col-md-3">
					<p>
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M20.822 18.096c-3.439-.794-6.641-1.49-5.09-4.418.4-.756.74-1.481 1.027-2.178-.866.496-1.833.88-2.359 1.011l-.224-1.066c1.148-.328 2.388-.939 3.252-1.838 1.836-6.283-1.267-9.607-5.428-9.607-5.082 0-8.465 4.949-3.732 13.678 1.598 2.945-1.725 3.641-5.09 4.418-2.979.688-3.178 2.143-3.178 4.663l.005 1.241h23.99l.005-1.241c0-2.52-.199-3.975-3.178-4.663zm-8.814-4.776c-.441.091-.873-.194-.963-.636-.092-.442.193-.874.634-.965l1.753-.389.329 1.6-1.753.39z"/></svg>
С Вами связываются администраторы и уточняют дату и детали.</p>	
				</div>
			</div>
    <?}?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
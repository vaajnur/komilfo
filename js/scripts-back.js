$(document).ready(function () {
    //смена валют
    $( document ).on( "change", ".choose-currency", function() {
      var d = new Date();
      d.setDate(d.getDate() + 7);
      setCookie("valute",$(".choose-currency option:selected").val(),{expires: d,path: "/"});
      location.reload();
    });
	$( document ).on( "click", 'input[name="DELIVERY_ID"]', function() {
		setCookie("CHECKED_DELIVERY",$(this).val(),{path: "/"});
	});
	$( document ).on( "click", 'input[name="PAY_SYSTEM_ID"]', function() {
		setCookie("CHECKED_PAY",$(this).val(),{path: "/"});
	});
	$( document ).on( "click", ".level1", function() {
		$(".level1").removeClass("tabactive");
		$(this).addClass("tabactive");
	});
	$( document ).on( "click", ".tabactive.level1", function() {
       	location.replace($(this).attr("reallink"));
	});
   $( document ).on( "click", ".swap-products a", function() {
       // $(".products-container").addClass("loading");
       $(".swap-products a").removeClass("active");
       $(this).addClass("active");
       var id=$(this).attr("id");
		  xhr = $.ajax({
            url: '/ajax.content/products.main-page.php',
            data: {
               id:id
            },
            success: function(ans){
                $(".products-container .products").html(ans);
                $(".products-container").removeClass("loading");
            }
        });
	});
    //изменение адреса в корзине
    $( document ).on( "click", ".location_select .dropdown-menu li", function() {
      var index=$(this).index();
      var element = $(".choose_adres option:eq("+index+")");
		$(".choose_adres .dropdown-toggle .filter-option").text($(".choose_adres option:eq("+index+")").text());
      if(element.attr("value")=="new"){
			setCookie("default_address","new",{path: "/"});
			$("#ORDER_PROP_8").show();
			$("#ORDER_PROP_9").show();
            $("#ORDER_PROP_10").show();
            $("#ORDER_PROP_6_val").show();
			$(".bx-ui-sls-fake").val("");
			$(".dropdown-field.town").val("");
			$("#ORDER_PROP_8").val("");
			$("#ORDER_PROP_9").val("");
			$("#ORDER_PROP_10").val("");
		}
		else{
			setCookie("default_address",element.attr("value"),{path: "/"});
			$(".location-block-wrapper").hide();
			$("#ORDER_PROP_8").hide();
			$("#ORDER_PROP_9").hide();
			$("#ORDER_PROP_10").hide();
            $("#ORDER_PROP_6_val").hide();
            $("#ORDER_PROP_5").val(element.attr("cityname"));
			$("#ORDER_PROP_6").val(element.attr("city"));
			$("#ORDER_PROP_8").val(element.attr("street"));
			$("#ORDER_PROP_9").val(element.attr("house"));
			$("#ORDER_PROP_10").val(element.attr("flat"));
		}
    });
    //бренды попап
    /*
    $( document ).on( "click", ".top-brands-link", function() {
        $(".popup.top-brands").fadeIn();
        $('.popup.top-brands .popup-body').animateCss('fadeInUp');
        return false;
    });
    */
    //заказать примерку попап
    $( document ).on( "click", ".order-fit", function() {
		    setTimeout(function(){
          try {
            // statements
            yaCounter20825989.reachGoal('button_click_fit')
          } catch(e) {
            // statements
            console.log(e);
          }
      }, 1000);
        $(".popup.ordered-fitting .product_id").val($(this).attr("product_id"));
        $(".popup.ordered-fitting .h2").text($(this).attr("product_name"));
        $(".popup.ordered-fitting").fadeIn();
        $('.popup.ordered-fitting .popup-body').animateCss('fadeInUp');

        return false;
	});
    //чекбокс в заказе примерки
    $( document ).on( "click", ".popup.ordered-fitting .checkbox", function() {
        if($(this).hasClass("active")){
            $(".input-group-email").removeClass("disabled");
        }
        else{
            $(".input-group-email").addClass("disabled");
        }
    });

    $( document ).on( "click", ".popup.ordered-fitting .radio_primerka", function() {
        if($(this).val()=='home'){
          $('.adr_primerka').show();
        }else{
          $('.adr_primerka').hide();
        }
    });
    //заказать примерку кнопка
    $( document ).on( "click", ".go-order-fit", function() {
       var oldtxt=$(this).text();
        var check=0;
        if($(".popup.ordered-fitting .checkbox").hasClass("active")){
            check=1;
        }
       $(".popup.ordered-fitting input").removeClass("error");
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax.content/form.primerka.php',
           data:{
                check:check,
                name:$(".popup.ordered-fitting .your_name").val(),
                email:$(".popup.ordered-fitting .your_email").val(),
                phone:$(".popup.ordered-fitting .your_phone").val(),
                adres:$(".popup.ordered-fitting .your_address").val(),
               product:$(".popup.ordered-fitting .product_id").val(),
               place: $(".popup.ordered-fitting .radio_primerka:checked").val(),
           },
           success: function(ans){
              console.log(ans);
               if(ans.indexOf("name-error")!=-1){
                   $(".popup.ordered-fitting .your_name").addClass("error");
               }
               if(ans.indexOf("phone-error")!=-1){
                   $(".popup.ordered-fitting .your_phone").addClass("error");
               }
               if(ans.indexOf("email-error")!=-1){
                   $(".popup.ordered-fitting .your_email").addClass("error");
               }
               if(ans.indexOf("adres-error")!=-1){
                   $(".popup.ordered-fitting .your_address").addClass("error");
               }
               if(ans.indexOf("okidoki")!=-1){
                   $(".popup.ordered-fitting .popup-content").addClass("success");
                   $(".go-order-fit").text("Отправлено");
				   $(".go-order-fit").attr("disabled","disabled");
				   $("#message_order_fit").html("<br>Менеджер свяжется с Вами в течение часа");
				   ingEvents.listTrackers(); ingEvents.GAEvent({category:'forms', action:'submit', label:'fitting_na'});
				   try {
             // statements
             yaCounter20825989.reachGoal('success_order_fit');
           } catch(e) {
             // statements
             console.log(e);
           }
                   setTimeout(function() {
                            $(".popup.ordered-fitting").fadeOut();
                            $('.popup.ordered-fitting .popup-body').animateCss('fadeOutDown');
                            $(".popup.ordered-fitting .popup-content").removeClass("success");
                            $(".popup.ordered-fitting input").val("");
                            $(".go-order-fit").text(oldtxt);
					   		$(".go-order-fit").removeAttr("disabled","disabled");
					   		$("#message_order_fit").text("");
                    }, 10000);
               }
               else{
                    $(".go-order-fit").text(oldtxt);
               }
           }
       });
    });
     //сдать вещи форма обратной связи
     $( document ).on( "click", ".go-hand-items", function() {
        var oldtxt=$(this).text();
        $(".popup.hand-items input").removeClass("error");
        $(this).text("Загрузка...");
        $.ajax({
            url: '/ajax.content/form.hand-items.php',
            data:{
                 name:$(".popup.hand-items .your_name").val(),
                 email:$(".popup.hand-items .your_email").val(),
                 phone:$(".popup.hand-items .your_phone").val(),
            },
            success: function(ans){
               console.log(ans);
                if(ans.indexOf("name-error")!=-1){
                    $(".popup.hand-items .your_name").addClass("error");
                }
                if(ans.indexOf("phone-error")!=-1){
                    $(".popup.hand-items .your_phone").addClass("error");
                }
                if(ans.indexOf("email-error")!=-1){
                    $(".popup.hand-items .your_email").addClass("error");
                }
                if(ans.indexOf("adres-error")!=-1){
                    $(".popup.hand-items .your_address").addClass("error");
                }
                if(ans.indexOf("okidoki")!=-1){

                  ingEvents.listTrackers(); ingEvents.GAEvent({category:'forms', action:'submit', label:'request_submit'});  
                 // ga('send', 'event', 'forms', 'request_submit');
                  //ym(20825989,'reachGoal','request_submit');
                   try {
                     // statements
                     yaCounter20825989.reachGoal('request_submit');
                   } catch(e) {
                     // statements
                     console.log(e);
                   }
                   console.log('СОБЫТИЕ YM ОТПРАВИЛИ');
                    $(".popup.hand-items .popup-content").addClass("success");
                    $(".go-hand-items").text("Отправлено");
                    $(".go-hand-items").attr("disabled","disabled");
                    $("#message_hand_items").html("<br>Менеджер свяжется с Вами в течение часа");
                    setTimeout(function() {
                             $(".popup.hand-items").fadeOut();
                             $('.popup.hand-items .popup-body').animateCss('fadeOutDown');
                             $(".popup.hand-items .popup-content").removeClass("success");
                             $(".popup.hand-items input").val("");
                             $(".go-hand-items").text(oldtxt);
                                $(".go-hand-items").removeAttr("disabled","disabled");
                                $("#message_hand_items").text("");
                     }, 10000);
                }
                else{
                     $(".go-hand-items").text(oldtxt);
                }
            }
        });
        return false;
     });
     //проверка оригинальности форма обратной связи
     $( document ).on( "click", ".btn-fake", function() {
        var oldtxt=$(this).text();
        $(".how-to-spot input").removeClass("error");
        $(this).text("Загрузка...");
        $.ajax({
            url: '/ajax.content/form.check-items.php',
            data:{
                 name:$(".how-to-spot .your_name").val(),
                 email:$(".how-to-spot .your_email").val(),
                 phone:$(".how-to-spot .your_phone").val(),
            },
            success: function(ans){
               console.log(ans);
                if(ans.indexOf("name-error")!=-1){
                    $(".how-to-spot .your_name").addClass("error");
                }
                if(ans.indexOf("phone-error")!=-1){
                    $(".how-to-spot .your_phone").addClass("error");
                }
                if(ans.indexOf("email-error")!=-1){
                    $(".how-to-spot .your_email").addClass("error");
                }
                if(ans.indexOf("adres-error")!=-1){
                    $(".how-to-spot .your_address").addClass("error");
                }
                if(ans.indexOf("okidoki")!=-1){
                    $(".how-to-spot input").addClass("success");
                    $(".btn-fake").text("Заявка отправлена");
                    $(".btn-fake").attr("disabled","disabled");
                    setTimeout(function() {
                        $(".how-to-spot input").removeClass("success");
                             $(".how-to-spot input").val("");
                             $(".btn-fake").text(oldtxt);
                                $(".btn-fake").removeAttr("disabled","disabled");
                     }, 10000);
                }
                else{
                     $(".btn-fake").text(oldtxt);
                }
            }
        });
        return false;
     });
    //купить в 1 клик попап
    $( document ).on( "click", ".btn-oneclick", function() {
		try {
      // statements
      yaCounter20825989.reachGoal('zakazv1');
    } catch(e) {
      // statements
      console.log(e);
    }
        $(".popup.oneclick .product_id").val($(this).attr("product_id"));
        $(".popup.oneclick .h2").text($(this).attr("product_name"));
        $(".popup.oneclick").fadeIn();
        $('.popup.oneclick .popup-body').animateCss('fadeInUp');

        return false;
    });
     //записаться на выплату
     $( document ).on( "click", ".enroll_payment", function() {
        $(this).text("Загрузка...");
        $.ajax({
            url: '/ajax.content/enroll_payment.php',
            data:{
            },
            success: function(ans){
                $(".enroll_payment").text("Заявка оставлена");
                $( "<p style='font-size: 1.6rem;'>Мы свяжемся с Вами и назначим дату выплаты</p>" ).insertAfter( ".enroll_payment" );
                $(".enroll_payment").attr("disabled","disabled");
            }
        });
     });
     //записаться на выплату неавт
     $( document ).on( "click", ".enroll_paymenthand", function() {
        $(this).text("Загрузка...");
        $.ajax({
            url: '/ajax.content/enroll_payment.php',
            data:{
                USER_ID:$(".user_id").val()
            },
            success: function(ans){
                $(".enroll_paymenthand").text("Заявка оставлена");
                $( "<p style='font-size: 1.6rem;'>Мы свяжемся с Вами и назначим дату выплаты</p>" ).insertAfter( ".enroll_paymenthand" );
                $(".enroll_paymenthand").attr("disabled","disabled");
            }
        });
     });
      //записаться на возврат неавт
      $( document ).on( "click", ".enroll_backhand", function() {
        $(this).text("Загрузка...");
        $.ajax({
            url: '/ajax.content/enroll_back.php',
            data:{
                USER_ID:$(".user_id").val()
            },
            success: function(ans){
                $(".enroll_backhand").text("Заявка оставлена");
                $( "<p style='font-size: 1.6rem;'>Мы свяжемся с Вами и назначим дату возврата</p>" ).insertAfter( ".enroll_backhand" );
                $(".enroll_backhand").attr("disabled","disabled");
            }
        });
     });
    //купить в 1 клик
    $( document ).on( "click", ".popup.oneclick button", function() {
       var oldtxt=$(this).text();
       $(".popup.oneclick input").removeClass("error");
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax.content/form.oneclick.php',
           data:{
                product:$(".popup.oneclick .product_id").val(),
                name:$(".popup.oneclick .your_name").val(),
                phone:$(".popup.oneclick .your_phone").val(),
           },
           success: function(ans){
              console.log(ans);
               if(ans.indexOf("name-error")!=-1){
                   $(".popup.oneclick .your_name").addClass("error");
               }
               if(ans.indexOf("phone-error")!=-1){
                   $(".popup.oneclick .your_phone").addClass("error");
               }
               if(ans.indexOf("okidoki")!=-1){
                   $(".popup.oneclick .popup-content").addClass("success");
                   $(".popup.oneclick button").text("Заказ оформлен");
				   $(".popup.oneclick button").attr("disabled","disabled");
				   $("#message_oneclick").html("<br>Менеджер свяжется с Вами в течение часа");
		

ingEvents.listTrackers(); ingEvents.GAEvent({category:'forms', action:'submit', label:'otprav_klik'});  
try {
  yaCounter20825989.reachGoal('otprav_klik');
  // statements
} catch(e) {
  // statements
  console.log(e);
}

           console.log('ОТПРАВИЛИ Я МЕТРИКУ and GA otprav_klik');
                   setTimeout(function() {
                            $(".popup.oneclick").fadeOut();
                            $('.popup.oneclick .popup-body').animateCss('fadeOutDown');
                            $(".popup.oneclick input").val("");
                            $(".popup.oneclick .popup-content").removeClass("success");
                            $(".popup.oneclick button").text(oldtxt);
					   		$(".popup.oneclick button").removeAttr("disabled","disabled");
					   		$("#message_oneclick").text("");
                    }, 10000);
               }
               else{
                   $(".popup.oneclick button").text(oldtxt);
               }
           }
       });
    });
    //вход
    $( document ).on( "click", ".open-enter-popup", function() {
        $(".popup.enter").fadeIn();
        $('.popup.enter .popup-body').animateCss('fadeInUp');
        return false;
	});

    $( document ).on( "click", ".open-callbackheader-popup", function() {
        $(".popup.callbackheader").fadeIn();
        $('.popup.callbackheader .popup-body').animateCss('fadeInUp');
        return false;
  });

      $( document ).on( "click", ".open-buyoneclick-popup", function() {
        $(".popup.buyoneclick").fadeIn();
        $('.popup.buyoneclick .popup-body').animateCss('fadeInUp');
        return false;
  });

    
     $( document ).on( "click", ".popup.enter button", function() {
       var oldtxt=$(this).text();
       $(".popup.enter input").removeClass("error");
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax.content/form.auth.php',
           data:{
                login:$(".popup.enter input[name=login]").val(),
                password:$(".popup.enter  input[name=password]").val()
           },
           success: function(ans){
              console.log(ans);
               if(ans.indexOf("error")!=-1){
                   $(".popup.enter input").addClass("error");
                   $(".popup.enter button").text(oldtxt);
               }
               if(ans.indexOf("okidoki")!=-1){
                  location.reload();
               }
           }
       });
         return false;
    });
    //регистрация
    $( document ).on( "click", ".open-reg-popup", function() {
        $(".popup.reg").fadeIn();
        $('.popup.reg .popup-body').animateCss('fadeInUp');
        return false;
	});
    $( document ).on( "click", ".popup.reg button", function() {
       var oldtxt=$(this).text();
       $(".popup.reg input").removeClass("error");
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax.content/form.reg.php',
           data:{
                USER_NAME:$(".popup.reg input[name=USER_NAME]").val(),
                USER_EMAIL:$(".popup.reg input[name=USER_EMAIL]").val(),
                USER_PHONE:$(".popup.reg input[name=USER_PHONE]").val(),
                USER_BIRTHDAY:$(".popup.reg input[name=USER_BIRTHDAY]").val(),
           },
           success: function(ans){
              console.log(ans);


              
               if(ans.indexOf("name-error")!=-1){
                   $(".popup.reg input[name='USER_NAME']").addClass("error");
               }
               if(ans.indexOf("email-error")!=-1){
                   $(".popup.reg input[name='USER_EMAIL']").addClass("error");
               }
               if(ans.indexOf("date-error")!=-1){
                   $(".popup.reg input[name='USER_BIRTHDAY']").addClass("error");
               }
               if(ans.indexOf("okidoki")!=-1){
                   location.reload();
                     console.log('reg GA and YM');

              ingEvents.listTrackers(); ingEvents.GAEvent({category:'forms', action:'submit', label:'registration_submit'});
                   try {
                     // statements
                     yaCounter20825989.reachGoal('registration_submit');
                   } catch(e) {
                     // statements
                     console.log(e);
                   }
               }
               else{
                    $(".popup.reg button").text(oldtxt);
                }
           }

       });
       return false;
    });
    //диалоговое окно на выход
	$( document ).on( "click", ".open-exit-popup", function() {
		$(".popup.exit").fadeIn();
        $('.popup.exit .popup-body').animateCss('fadeInUp');
        return false;
	});
    $( document ).on( "click", ".go_exit_cancel", function() {
		$(".popup.exit").fadeOut();
        $('.popup.exit .popup-body').animateCss('fadeOutDown');
	});
	$( document ).on( "click", ".go_exit", function() {
		$( ".go_exit_cancel" ).remove();
		$(".go_exit").text("Загрузка...");
        $(".go_exit").css("width","100%");
		$(".go_exit").attr("disabled","disabled");
		$.ajax({
			type:"POST",
			url:'/ajax.content/form.exit.php',
			data:{
				EXIT: 'Y',
            },
			success: function(response){
				location.replace("/");
			}
		});
	});
    //забыли пароль
    $( document ).on( "click", ".enter .forgot_password", function() {
        $(".popup.enter").fadeOut();
        $('.popup.enter .popup-body').animateCss('fadeOutDown');
        $(".popup.forgot").fadeIn();
        $('.popup.forgot .popup-body').animateCss('fadeInUp');
		return false;
	});
     $( document ).on( "click", ".popup.forgot button", function() {
         $(".popup.forgot input").removeClass("error");
         var oldtxt=$(".popup.forgot button").text();
         $(".popup.forgot button").text("Загрузка...");
        var form=$(".popup.forgot form");
		$.ajax({
			type:"POST",
			url:"/ajax.content/form.forgot.php",
			data:form.serialize(),
			success: function(response){
				// console.log(response);
                if(response.indexOf("login-error")!=-1){
                    $(".popup.forgot input").addClass("error");
                }
                if(response.indexOf("success")!=-1){
                    $(".popup.forgot input").addClass("success");
                }
                $(".popup.forgot button").text(oldtxt);
			}
		});
		return false;
    });
    //попап для изображений
    $( document ).on( "click", ".img-popup", function() {
        $(".popup.imgpopup img").attr("src",$(this).attr("href"));
        $(".popup.imgpopup").fadeIn();
        $('.popup.imgpopup .popup-body').animateCss('fadeInUp');
        return false;
	});
    //редактировать в личном кабинете
	$( document ).on( "click", ".basicdata .lk-edit", function() {
		$(".basicdata  .lk-edit").hide();
		$(".basicdata  .lk-close-edit").show();
		$(".basicdata input").removeAttr("disabled");
		$(".sms_box").removeClass("disabled");
		$(".basicdata button.sms-approve-button").removeAttr("disabled");
		$('.gray_field.user_name').trigger( "focus" );
	});
	$( document ).on( "click", ".basicdata .lk-close-edit", function() {
		$(".basicdata  .lk-edit").show();
		$(".basicdata .lk-close-edit").hide();
		$(".basicdata input").attr("disabled","disabled");
		$(".basicdata button.sms-approve-button").attr("disabled","disabled");
		$(".sms_box").addClass("disabled");
	});
	$( document ).on( "click", ".addresses  .lk-edit", function() {
		$(".addresses  .lk-edit").hide();
		$(".addresses  .lk-close-edit").show();
		$(".addresses input").removeAttr("disabled");
		$(".addresses .country").attr("disabled","disabled");
		$(".lk-regions").removeClass("disabled");
		$('.gray_field.city').trigger( "focus" );
	});
	$( document ).on( "click", ".addresses  .lk-close-edit", function() {
		$(".addresses  .lk-edit").show();
		$(".addresses  .lk-close-edit").hide();
		$(".addresses input").attr("disabled","disabled");
		$(".lk-regions").addClass("disabled");
	});
	$( document ).on( "click", ".sizesdata .lk-edit", function() {
		$(".sizesdata  .lk-edit").hide();
		$(".sizesdata  .lk-close-edit").show();
		$(".sizesdata input").removeAttr("disabled");
		$('.gray_field.user_growth').trigger( "focus" );
	});
	$( document ).on( "click", ".sizesdata .lk-close-edit", function() {
		$(".sizesdata  .lk-edit").show();
		$(".sizesdata  .lk-close-edit").hide();
		$(".sizesdata input").attr("disabled","disabled");
	});
	$( document ).on( "click", ".passwords .lk-edit", function() {
		$(".passwords  .lk-edit").hide();
		$(".passwords  .lk-close-edit").show();
		$(".passwords .active_password").removeAttr("disabled");
		$('.gray_field.active_password').trigger( "focus" );
	});
	$( document ).on( "click", ".passwords .lk-close-edit", function() {
		$(".passwords  .lk-edit").show();
		$(".passwords  .lk-close-edit").hide();
		$(".passwords .active_password").attr("disabled","disabled");
	});
    //чекбокс в личном кабинете
	$( document ).on( "click", ".lk-body .sms_box", function() {
		if($(this).hasClass("checked")){
			$(this).removeClass("checked");
			$.ajax({
				url: '/ajax.content/form.profile.php',
				data: {
					NAME:"UF_SMS",
					VALUE: "",
				},
				success: function(ans){
					console.log(ans);
				}
			});
		}
		else{
			$(this).addClass("checked");
			$.ajax({
				url: '/ajax.content/form.profile.php',
				data: {
					NAME:"UF_SMS",
					VALUE: "Y",
				},
				success: function(ans){
					console.log(ans);
				}
			});
		}
	});
	//подтвердить телефон
	$( document ).on( "click", ".approve-phone", function() {
		$(".user_phone").removeClass("error");
		$.ajax({
			url: '/ajax.content/form.approvecode.php',
			data: {
				PHONE:$(".user_phone").val(),
			},
			success: function(ans){
				console.log(ans);
				if(ans.indexOf('success')==-1){
					$(".user_phone").addClass("error");
				}
				else{
					$(".code-sms").show();
					$(".sms-approve-button").hide();
					$(".user_phone").attr("disabled","disabled");
				}
			}
		});
	});
	//подтвердить код из смс
	$( document ).on( "click", ".enter-code", function() {
		$(".code-sms-val").removeClass("error");
		$.ajax({
			url: '/ajax.content/form.approvecode.php',
			data: {
				CODE:$(".code-sms-val").val(),
				VALUE:1
			},
			success: function(ans){
				console.log(ans);
				if(ans.indexOf('error')==-1){
					$(".approved-phone").show();
					$(".approve-sms-block").hide();
				}
				else{
					$(".code-sms-val").addClass("error");
				}
			}
		});
    });
    //подтвердить код из смс сдать вещи
	$( document ).on( "click", ".enter-code-handthings", function() {
		$(".code-sms-val").removeClass("error");
		$.ajax({
			url: '/ajax.content/form.approvecodehand.php',
			data: {
                CODE:$(".code-sms-val").val(),
                PHONE:$(".user_phone").val()
			},
			success: function(ans){
				if(ans.indexOf('error')==-1){
					location.replace(ans);
				}
				else{
					$(".code-sms-val").addClass("error");
				}
			}
		});
	});
    //фото пользователя
    $( document ).on( "click", ".lk-img_edit", function() {
		$( "#avatar" ).trigger( "click" );
	});
	$( document ).on( "change", "#avatar" , function() {
		$( "#my_form" ).trigger( "submit" );
	});
	$('#my_form').on('submit', function(e){
		e.preventDefault();
		var $that = $(this),
		formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
		$.ajax({
		  url: $that.attr('action'),
		  type: $that.attr('method'),
		  contentType: false, // важно - убираем форматирование данных по умолчанию
		  processData: false, // важно - убираем преобразование строк по умолчанию
		  data: formData,
		  dataType: 'html',
		  success: function(response){
			  console.log(response);
			$(".lk-img_edit").css("background-image","url('"+response+"')");
		  }
		});
	});
    //подтверждение удаления адреса
	$( document ).on( "click", ".deleteadr .yes", function() {
		vall=$(".deleteadr .yes").attr("val");
		$(".deleteadr .yes").remove();
		$(".deleteadr .no").text("Загрузка...");
		$(".deleteadr .no").attr("disabled","disabled");
		$.ajax({
			type:"POST",
			url:"/ajax.content/form.profile.php",
			data:{
				DELETE: 'Y',
				ADRES:vall,
            },
			success: function(response){
				console.log(response);
				$(".region-item:eq("+$(".deleteadr .yes").attr("val")+")").remove();
				location.reload();
			}
		});
	});
	$( document ).on( "click", ".deleteadr .no", function() {
		$(".popup.deleteadr").fadeOut();

	});
    //нажатие кнопки отправить в обратной связи
    $( document ).on( "click", ".go-feedback", function() {
		$(this).val("Загрузка");
		$(".mfeedback").addClass("loading");
	});
        //подтверждение удаления сданной вещи
	$( document ).on( "click", ".deletehandthing .yes", function() {
		vall=$(".deletehandthing .yes").attr("val");
		/*$(".deletehandthing .yes").remove();
		$(".deletehandthing .no").text("Загрузка...");
		$(".deletehandthing .no").attr("disabled","disabled");*/
		$.ajax({
			type:"POST",
			url:"/ajax.content/form.deletehandthing.php",
			data:{
				ID:vall,
                ACTIVE:"N"
            },
			success: function(response){
                $(".deletehandthing").fadeOut();
				$("#returnhandthing"+vall).show();
                $("#deletehandthing"+vall).hide();
			}
		});
	});
	$( document ).on( "click", ".deletehandthing .no", function() {
		$(".popup.deletehandthing").fadeOut();

	});
    //смена адреса в личном кабинете
	$( document ).on( "click", ".lk-regions .region", function() {
		$(".lk-regions .region").removeClass("selected");
		$(this).addClass("selected");
		$(".selectedadres").val($(this).attr("index"));
		$(".lk-double .country").val($(this).attr("country"));
		$(".lk-double .city").val($(this).attr("city"));
		$(".lk-double .street").val($(this).attr("street"));
		$(".lk-double .house").val($(this).attr("house"));
		$(".lk-double .flat").val($(this).attr("flat"));
		$(".lk-double .adresname").val($(this).attr("adresname"));
	});
    //добавление в вишлист
	$( document ).on( "click", ".add-to-wishlist", function() {
		var product=$(this).attr("product_id");
        var product_name=$(this).attr("product_name");
        var oldtxt=$(this).text();
        $(this).text("Загрузка...");
		$.ajax({
			type:"POST",
			url:"/ajax.content/addtowishlist.php",
			data:{
				product: product
            },
			success: function(response){
				$(".popup.addtowishlist .popup-content .h2").text(product_name);
                $(".popup.addtowishlist .popup-content .popup-text").html("<a class='inverted-link' href='/personal/wishlist/'>Посмотреть отложенные товары</a>");
                $(".popup.addtowishlist").fadeIn();
                $('.popup.addtowishlist .popup-body').animateCss('fadeInUp');
                if(response.indexOf("okidoki")!=-1){
                    //попап
                }
                $(".add-to-wishlist").remove();
			}
		});
        return false;
	});
    //подгрузка вишлиста
	$( document ).on( "click", ".loadmorewishlist", function() {
		var d=parseInt($(this).attr("pagenum"));
		var maxp=parseInt($(this).attr("maxpages"));
		 $(".morewishlist").addClass("load_circle-parent");
		$(".load_circle-container").show();
		d=d+1;
		$.ajax({
			type:"POST",
			url:"",
			data:{
				AJAX: 'Y',
				PAGEN_1: d
            },
			success: function(response){
				if(d+1>maxp){
					$(".loadmorewishlist").hide();
				}
				$(".tiny-items").append(response);
				$(".loadmorewishlist").attr("pagenum",d);
				$(".moreinsta").removeClass("load_circle-parent");
				$(".load_circle-container").hide();
			}
		});
	});
    //поиск в вишлисте
    var xhr;
	$( document ).on( "keyup", ".wishlistsearch input", function() {
		if(xhr){
			xhr.abort();
			$( ".itemsandbutton-wishlist" ).addClass("loading");
			$(".itemsandbutton-wishlist .cssload-container").fadeIn();
		 }
		 xhr = $.ajax({
			url: '',
			data: {
				AJAXS:"Y",
				q:$(".wishlistsearch input").val()
			},
			success: function(ans){
				$( ".itemsandbutton-wishlist" ).removeClass("loading");
				$(".itemsandbutton-wishlist").html(ans);
			}
		});
	});
    //поиск в шапке
    $( document ).on( "keyup", ".head .search input", function(e) {
         var code = e.keyCode || e.which;
         if(code == 13) { //Enter keycode
           location.replace("/search/?q="+$(this).val());
         }
		$(".head .search input").val($(this).val());
		if(xhr){
			xhr.abort();
		 }
		 if($(this).val().length>2){
			 xhr = $.ajax({
				url: '/ajax.content/head.search.php',
				data: {
					AJAXSEARCH:"Y",
					SEARCHQUERY:$(this).val()
				},
				success: function(ans){
					$(".head .search .search_results").html(ans);
					if(ans.length==0){
						$(".head .search .search_results").addClass("empty");
                        $(".head .search .search_results").removeClass("active");
					}
					else{
                        $(".head .search .search_results").addClass("active");
						$(".head .search .search_results").removeClass("empty");
					}
				}
			});
		}
		else{
			$(".search_results").addClass("empty");
			$(".search_results").html("");
		}
	});
    //добавление вещи в "сдать вещи"
	$( document ).on( "click", ".handthings-add", function() {
		var elem=$( ".handthings-inputs" ).last().clone();
		elem.find("input").val("");
		elem.find("textarea").text("");
		elem.find("textarea").val("");
		elem.find("input").removeClass("error");
		elem.find(".hs-img").removeClass("error");
		elem.find("textarea").removeClass("error");
		elem.removeClass("success-send");
		elem.find(".hs-img").css("background","#f5f5f5");
		elem.find(".hs-add-button").removeClass("active");
		elem.find(".region-delete").css("display", "block");
		$(".handthings-inputs").last().find(".region-delete").css("display", "block");
		$(elem).insertBefore( $( ".handthings-add" ) );
	});
	//удаление вещи в "сдать вещи"
	$( document ).on( "click", ".handthings-delete", function() {
		$(this).parent().remove();
		var count_things = 0;
		$(".handthings-inputs").each(function() {
			count_things++;
		});
		if (count_things == 1)
		{
			$(".handthings-delete").each(function() {
				$(this).css("display", "none");
			});
		}
	});
    //выбор фотки в "сдать вещи"
	$( document ).on( "click", ".handthings-inputs .hs-add-button", function() {
		$
		if(!$(this).hasClass("active")){
			$(this).siblings("input").trigger("click");
		}
		else{
			$(this).removeClass("active");
			var reader = new FileReader(),
			div = $(this).parents(".hs-img");
			div.css('background-image', 'url("")');
			$(this).siblings("input").val("");
			div.css('background-color', '#f5f5f5');
		}
	});
    $( document ).on( "change", ".handthings-inputs input[type=file]", function() {
        var reader = new FileReader(),
        file = this.files[0], div = $(this).parent();
        reader.onloadend = function() {
          div.css('background-image', 'url(' + this.result + ')');
        }
		console.log('s');
        file ? reader.readAsDataURL(file) : div.css('background-color', '#f5f5f5');
		file ? div.find(".hs-add-button").addClass("active") : div.find(".hs-add-button").removeClass('active');
    });

    //отправка формы сдать вещи
	$( document ).on( "click", ".go-handthings", function() {
        var oldtxt=$(".go-handthings").text();
        $(".handthings-form").addClass("loading");
        $(".go-handthings").text("Загрузка...");
		var formid="handthings-form";
        var form = $('.handthings-form');
        $(".handthings-form input").removeClass("error");
        $(".handthings-form textarea").removeClass("error");
		$(".hs-img-1").removeClass("error");
		$(".hs-img-2").removeClass("error");
        var formdata = false;
        if (window.FormData){
            formdata = new FormData(form[0]);
        }
		$(".handthings-errors-text").html("");
        var forms=(form.serialize());
        $.ajax({
            type:"POST",
            url: '/ajax.content/form.handthings.php',
            data:formdata,
            cache: false,
            contentType: false,
            processData: false,
            success: function(ans){
				var oldtxt = "Отправить";
                console.log(ans);
                if(ans.indexOf("error-name")!=-1){
                    $(".handthings-form input[name=name]").addClass("error");
					$(".handthings-errors-text").append("<p>Вы не заполнили Ваше имя</p>");
                }
                if(ans.indexOf("error-email")!=-1){
                    $(".handthings-form input[name=email]").addClass("error");
					$(".handthings-errors-text").append("<p>Вы не заполнили Ваш E-mail</p>");
                }
                if(ans.indexOf("error-phone")!=-1){
                    $(".handthings-form input[name=phone]").addClass("error");
					$(".handthings-errors-text").append("<p>Вы не заполнили Ваш телефон</p>");
                }
                for(i=0;i<100;i++){
                    if(ans.indexOf("error-description"+i)!=-1){
                        $(".handthings-form .txt-description:eq("+i+")").addClass("error");
						$(".handthings-errors-text").append("<p>Не заполнено описание</p>");
                    }
                    if(ans.indexOf("error-price"+i)!=-1){
                        $(".handthings-form .txt-price:eq("+i+")").addClass("error");
						$(".handthings-errors-text").append("<p>Не заполнена цена продажи</p>");
                    }
                    if(ans.indexOf("error-size"+i)!=-1){
                        $(".handthings-form .txt-size:eq("+i+")").addClass("error");
						$(".handthings-errors-text").append("<p>Не заполнен размер вещи</p>");
                    }
					 if(ans.indexOf("error-img1-"+i)!=-1){
                        $(".handthings-form .hs-img-1:eq("+i+")").addClass("error");
						 $(".handthings-errors-text").append("<p>Нет фото</p>");
                    }
					 if(ans.indexOf("error-img2-"+i)!=-1){
                        $(".handthings-form .hs-img-2:eq("+i+")").addClass("error");
						 $(".handthings-errors-text").append("<p>Нет фото</p>");
                    }
                }
                if(ans.indexOf("success")!=-1){
					$(".handthings-errors-text").html("");
					//ga('send', 'event', 'forms', 'application_submit');
					ingEvents.listTrackers(); ingEvents.GAEvent({category:'forms', action:'submit', label:'application_submit'});  
					ym(20825989,'reachGoal','application_submit');
                    $(".handthings-form").addClass("success");
					$(".handthings-form .top-hs-group").hide();
					$(".handthings-form .handthings-inputs").hide();
					$(".handthings-form .handthings-add").hide();
					$(".handthings-form .go-handthings").hide();
					var oldtxt=$(".handthings-form  .h2").html();
					$(".handthings-form  .h2").html("Спасибо, ваша заявка отправлена!<br>В течение дня с вами свяжется менеджер магазина.");
                    setTimeout(function() {
                            location.reload();
                    }, 10000);
                }
                $(".handthings-form").removeClass("loading");
                $(".go-handthings").text(oldtxt);
            }
        });
        return false;
	});
    $( ".nav-item-dropdown" ).each(function( index ) {
        console.log($(this).text().replace(/[\s{2,}]+/g, ' ').length+"-");
      if($(this).text().replace(/[\s{2,}]+/g, ' ').length==1){
          $(this).remove();
      }
    })
	//вызываем искусственный клик по первому адресу
	$(".lk-regions .region").first().trigger('click');
    //табы товаров на главной
    if($(".swap-products a").length>0){
        $("#novinki").trigger("click");
    }
    //копируем нижнюю пагинацию в верхнюю
    if($(".options-container.top .pagination").length>0)
        $(".options-container.top .pagination").html($(".options-container.bottom .pagination").html());
});
function show_all(page,endpage){
    var oldtxt=$("#load-all").text();
    $("#load-all").text("Загрузка...");
    $("#load-all").attr("disabled","disabled");
    if($(".products").length>0){
        $(".catalog-products .products-container").addClass("loading");
    }
    if($(".blog-container").length>0){
        $(".blog-container").addClass("loading");
    }
    $.ajax({
        url: '?show_all=Y&SHOWALL_1=1',
        data:{
			LOAD: "Y"
        },
        success: function(ans){
            $(".load-all").hide();
            $(".pagination a").removeClass("active");
            $(".options-container.top .pagination a:last").addClass("active");
            $(".options-container.bottom .pagination a:last").addClass("active");
            var loadUrl = location.href.split('?')[0];
            if(~loadUrl.indexOf("page"))
                loadUrl=loadUrl.replace(page, endpage)
            else
                loadUrl  += 'page' + (endpage)+"/";
            loadUrl = loadUrl.replace(/.html/g, ".html/");
                window.history.replaceState(null, null, loadUrl);
            if($(".products").length>0){
               $(".products").html(ans);
               $(".catalog-products .products-container").removeClass("loading");
            }
            if($(".blog-container").length>0){
                $(".blog-container .row").html(ans);
                $(".blog-container").removeClass("loading");
            }
            $("#load-all").removeAttr("disabled");
            $("#load-all").text(oldtxt);
        }
    });
}
function sort_catalog(field,order,el){
    $(".catalog-products .products-container").addClass("loading");
    $(".sorting button").removeClass("active");
    $(".sorting button span").remove();
    if(order=="asc"){
        $(el).prepend("<span></span>");
        $(el).children("span").html("&#8593");
        $(el).attr("onclick","sort_catalog('"+field+"','desc',this)");
    }
    else{
        $(el).prepend("<span></span>");
        $(el).children("span").html("&#8595");
         $(el).attr("onclick","sort_catalog('"+field+"','asc',this)");
    }
    $.ajax({
        url: '?sort='+field+'&order='+order,
        data:{
			LOAD: "Y"
        },
        success: function(ans){
            if($(".unsort-button").length==0&&location.href.indexOf("novinki")!=-1){
                $('<button onclick="sort_catalog(\'NONE\',\'asc\')" class="styless unsort-button">Сбросить</button>').insertAfter($(".sorting button").last());
            }
            if(field=="NONE"){
                $(".unsort-button").remove();
                $(".sorting button span").remove();
            }
           $(".products").html(ans);
            $(el).addClass("active");
           $(".catalog-products .products-container").removeClass("loading");
        }
    });
}
function addtocart(product_id,size,color,name){
    if($(".cart-products").length>0){
        $(".cart-products .products-container").addClass("loading");
        $(".cart-info").addClass("loading");
    }
    var oldtxt=$("#addtocart"+product_id).text();
    $("#addtocart"+product_id).text("Загрузка...");
    $(".megabasket").addClass("loading");
    $.ajax({
        url: '/cart/addtocart.php',
        data: {
           cmd:"addP2B",
           product_id:product_id,
           size:size,
           color:color
        },
        success: function(ans){
            console.log(ans);
            if(ans.indexOf("disabled")!=-1){
                $("#addtocart"+product_id).addClass("disabled");
                $("#addtocart"+product_id).text(oldtxt);
            }
            else{
                ans=JSON.parse(ans);
				        try {
                  // statements
                  yaCounter20825989.reachGoal('KUPIT1');
                } catch(e) {
                  // statements
                  console.log(e);
                }
                if(typeof dataLayer != 'undefined'){
                  dataLayer.push({
                       'event': 'addToCart',
                       'ecommerce': {
                           'currencyCode': 'RUB',
                           'actionField': {'list':window.location.pathname},    // Если добавление произошло в списке товаров, необходимо передать в каком листе.
                           'add': {
                                'products': [
                               {
                                  'name': ans.name,     // Название продукта
                                  'id': ans.id, 			// ID товара
                                  'price': ans.price,			// Цена товара
                                  'brand': ans.brand,			// Бренд товара
                                  'category': ans.category    //Тип акции на товар, если 2, то через /
                               }
                               ]
                          }
                       }
                  });
                }
                 $.ajax({
                    url: '/cart/addtocart.php',
                    data: {
                        cmd:"getHeadBasket"
                    },
                    success: function(ans){
                        $(".popup.addtocart .popup-content .h2").text(name);
                        $(".popup.addtocart .popup-content .popup-text").html("<a class='inverted-link' href='/cart/'>Перейти в корзину</a>");
                        $(".popup.addtocart").fadeIn();
                        $('.popup.addtocart .popup-body').animateCss('fadeInUp');
                        $("#addtocart"+product_id).text("Перейти в корзину");
                        $("#addtocart"+product_id).attr("onclick","location.replace('/cart/')");
						$("#addtocart"+product_id).attr("href","/cart/");
						$(".addtocartid"+product_id).each(function() {
							$(this).text("Перейти в корзину");
							$(this).attr("onclick","location.replace('/cart/')");
							$(this).attr("href","/cart/");
						});
                        $(".basket").html(ans);
                        // basket scroll
                       $('.basket .scroll').mCustomScrollbar();
                        setTimeout(function() {
                                $(".popup.addtocart").fadeOut();
                                $('.popup.addtocart .popup-body').animateCss('fadeOutDown');
                        }, 10000);
                    }
                });
                $.ajax({
                    url: '/cart/addtocart.php',
                    data: {
                        cmd:"getHeadBasket2"
                    },
                    success: function(ans){
                        $(".megabasket").removeClass("loading");
                        $(".megabasket").html(ans);
                    }
                });
                if($(".cart-products").length>0){
                    $.ajax({
                        url: '',
                        data: {
                            UPDATE_BASKET:""
                        },
                        success: function(ans){
                            $(".cart-products .products-container").removeClass("loading");
                            $(".for-ul-basket").html(ans);
                        }
                    });
                    $.ajax({
                        url: '',
                        data: {
                            UPDATE_CART_INFO:""
                        },
                        success: function(ans){
                            $(".cart-info").removeClass("loading");
                            $(".cart-info").html(ans);
                        }
                    });
                }
            }
        }
    });
}
//изменение значения обычного поля
function changeuserfield(name,id){
	$.ajax({
		url: '/ajax.content/form.profile.php',
		data: {
			NAME:name,
			VALUE: $(".lk-body ."+id).val(),
		},
		success: function(ans){
			$(".lk-body ."+id).addClass("success");
			setTimeout(function() {
				$(".lk-body ."+id).removeClass("success");
			}, 2000);
			console.log(ans);
		}
	});
	if(name=="PERSONAL_PHONE"){
		$.ajax({
			url: '/ajax.content/form.approvecode.php',
			data: {
				CODE:$(".code-sms-val").val(),
				VALUE:0
			},
			success: function(ans){
				console.log(ans);
				$(".approve-sms-block").show();
				$(".approved-phone").hide();
			}
		});
	}
}
//проверка пароля
function checkpass(){
	$.ajax({
		url: '/ajax.content/form.profile.php',
		data: {
			NAME:"password",
			VALUE: $(".active_password").val(),
		},
		success: function(ans){
			$(".active_password").removeClass("error");
			$(".active_password").removeClass("success");
			if(ans=="1"){
				$(".active_password").addClass("success");
				$(".active_password").attr("disabled","disabled");
				$(".new_pass1").removeAttr("disabled","disabled");
				$(".new_pass2").removeAttr("disabled","disabled");
				$('.new_pass1').trigger( "focus" );
			}
			else{
				$(".active_password").addClass("error");
			}
		}
	});
}
//проверка нового пароля
function checknewpass(){
	$.ajax({
		url: '/ajax.content/form.profile.php',
		data: {
			NAME:"password",
			VALUE1: $(".new_pass1").val(),
			VALUE2: $(".new_pass2").val(),
			VALUE3:$(".active_password").val()
		},
		success: function(ans){
			$(".new_pass1").removeClass("success");
			$(".new_pass2").removeClass("success");
			$(".new_pass1").removeClass("error");
			$(".new_pass2").removeClass("error");
			if(ans=="1"){
				$(".new_pass1").addClass("success");
				$(".new_pass2").addClass("success");
			}
			else{
				$(".new_pass1").addClass("error");
				$(".new_pass2").addClass("error");
			}
			console.log(ans);
		}
	});
}
//изменения поля адреса
function changeaddress(){
	$.ajax({
		url: '/ajax.content/form.profile.php',
		data: {
			ADRES:$(".selectedadres").val(),
			CITY:$(".lk-body .city").val(),
			STREET:$(".lk-body .street").val(),
			HOUSE:$(".lk-body .house").val(),
			FLAT:$(".lk-body .flat").val(),
			ADRESNAME:$(".lk-body .adresname").val()
		},
		success: function(ans){
			$("button.region.selected").attr("city",$(".lk-body .city").val());
			$("button.region.selected").attr("street",$(".lk-body .street").val());
			$("button.region.selected").attr("house",$(".lk-body .house").val());
			$("button.region.selected").attr("flat",$(".lk-body .flat").val());
			$("button.region.selected").attr("adresname",$(".lk-body .adresname").val());
			$("button.region.selected").text($(".lk-body .adresname").val());
			$(".lk-custom_double.addresses").addClass("success");
			setTimeout(function() {
				$(".lk-custom_double.addresses").removeClass("success");
			}, 2000);
			console.log(ans);
		}
	});
}
function deleteaddress(index){
	$(".popup.deleteadr").fadeIn();
    $('.popup.deleteadr .popup-body').animateCss('fadeInUp');
	$(".deleteadr .yes").attr("val",index);
}
function addaddress(){

	$("button.region").removeClass("selected");
	$("<div class='region-item'><button index='"+(parseInt($("button.region").last().attr("index"))+1)+"' class='region selected'>Мой новый адрес</button><button onclick='deleteaddress("+(parseInt($("button.region").last().attr("index"))+1)+")' class='region-delete'><svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 232.3 239.9' style='enable-background:new 0 0 232.3 239.9;' xml:space='preserve'><g><path d='M130.5,0.7v105.2h101.2v26.9H130.5v106.1h-28.7V132.8H0.7v-26.9h101.2V0.7H130.5z'></path></g></svg></button></div>").insertBefore($(".add-region"));
	$(".selectedadres").val($("button.selected.region").attr("index"));
	$(".lk-body .city").val("");
	$(".lk-body .street").val("");
	$(".lk-body .house").val("");
	$(".lk-body .flat").val("");
	$(".lk-body .adresname").val("");
}
function delchar(input) {
	var value = input.value;
	var rep = /[-;,":'a-zA-Zа-яА-Я\\=`ё/\*++!@#$%\^&_№?><]/;
	if (rep.test(value)) {
		value = value.replace(rep, '');
		input.value = value;
	}
}
function deletehandthing(id){
	$(".popup.deletehandthing").fadeIn();
    $('.popup.deletehandthing .popup-body').animateCss('fadeInUp');
	$(".deletehandthing .yes").attr("val",id);
}
function returnhandthing(id){
        vall=id;
		$.ajax({
			type:"POST",
			url:"/ajax.content/form.deletehandthing.php",
			data:{
				ID:vall,
                ACTIVE:"Y"
            },
			success: function(response){
				$("#returnhandthing"+id).hide();
                $("#deletehandthing"+id).show();
			}
		});
}
function delwish(id){
    $( ".itemsandbutton-wishlist" ).addClass("loading");
	$(".itemsandbutton-wishlist .cssload-container").fadeIn();
	$.ajax({
		type:"POST",
		url:"/personal/wishlist/",
		data:{
			AJAX: 'Y',
			DELETEWISH:"Y",
			ID:id,
        },
		success: function(response){
             $( ".itemsandbutton-wishlist" ).removeClass("loading");
			$(".tiny-items").html(response);
            $.ajax({
		  		url: '/cart/addtocart.php',
		  		data: {
		  			cmd:'getHeadBasket'
		  		},
		  		success: function(ans){
		  			$(".basket").html(ans);
                    // basket scroll
	               $('.basket .scroll').mCustomScrollbar();
		  		}
		  	});
		}
	});
}
function deletefrombasket(id){
    $(".megabasket").addClass("loading");
    $(".cart-info").addClass("loading");
    $(".cart-products .products-container").addClass("loading");
	$.ajax({
		type:"POST",
		url:"/ajax.content/deletefrombasket.php",
		data:{
			ID:id,
        },
		success: function(response){
            if($(".products.inbasket").length>0){
                // Measure the removal of a product from a shopping cart.
                if(typeof dataLayer != 'undefined'){
                  dataLayer.push({
                    'event': 'removeFromCart',
                    'ecommerce': {
                      'remove': {                               // 'remove' actionFieldObject measures.
                        'products': [{                          //  removing a product to a shopping cart.
                            'name': $("#inbasket"+id+" .product-header a").text(),
                            'id': id,
                            'price': $("#inbasket"+id+" .product-new-price").text(),
                            'brand': $("#inbasket"+id).attr("brand"),
                            'quantity': 1
                        }]
                      }
                    }
                  });
                }
                $("#inbasket"+id).remove();
                $(".cart-products .products-container").removeClass("loading");
                if($(".products.inbasket li").length==0){
                    location.reload();
                }
            }
            $.ajax({
                url: '',
                data: {
                    UPDATE_CART_INFO:""
                },
                success: function(ans){
                    $(".cart-info").removeClass("loading");
                    $(".cart-info").html(ans);
                }
            });
            $.ajax({
                url: '/cart/addtocart.php',
                data: {
                    cmd:"getHeadBasket"
                },
                success: function(ans){
                    $(".basket").html(ans);
                    // basket scroll
                   $('.basket .scroll').mCustomScrollbar();
                }
            });
            $.ajax({
                url: '/cart/addtocart.php',
                data: {
                    cmd:"getHeadBasket2"
                },
                success: function(ans){
                    $(".megabasket").removeClass("loading");
                    $(".megabasket").html(ans);
                }
            });
		}
	});
}
function deleteallfrombasket(){
    $(".megabasket").addClass("loading");
    $.ajax({
		type:"POST",
		url:"/ajax.content/deleteallfrombasket.php",
		data:{

        },
		success: function(response){
             $.ajax({
                url: '/cart/addtocart.php',
                data: {
                    cmd:"getHeadBasket"
                },
                success: function(ans){
                    $(".basket").html(ans);
                    // basket scroll
                   $('.basket .scroll').mCustomScrollbar();
                }
            });
            $.ajax({
                url: '/cart/addtocart.php',
                data: {
                    cmd:"getHeadBasket2"
                },
                success: function(ans){
                    $(".megabasket").removeClass("loading");
                    $(".megabasket").html(ans);
                }
            });
            if($(".products.inbasket").length>0){
                $(".products.inbasket li").remove();
                if($(".products.inbasket li").length==0){
                    location.reload();
                }
            }
		}
	});

}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : "";
}
//задавать свои куки
function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}
//удаление кук
function deleteCookie(name) {
  setCookie(name, "", {
    expires: -1,
	path: "/",
  })
}
function ecommerce_viewed(url,name,id,price,brend,category,discount,position) {
  if(typeof dataLayer != 'undefined'){
    dataLayer.push({
        'event': 'productClick',
        'ecommerce': {
          'click': {
    'actionField': {'list': url},
           'products': [{
           'name': name,
           'id': id,
           'price': price,
           'brand': brend,
           'category': category,
           'variant': discount,
           'list': url,
           'position': position
           }]
           }
         }
      });
  }
}
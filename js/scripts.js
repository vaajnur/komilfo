$(document).ready(function () {


	// добавлять hash в url на странице "размеры", при переходе по ссылкеактивный конкретный таб
	var url = document.location.toString();
	if (url.match('#')) {
		$('.page-sizes .nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
	}
	$('.page-sizes .nav-tabs a').on('click', function (e) {
		$(this).tab('show');
	    var scrollmem = $('body').scrollTop();
	    window.location.hash = this.hash;
	    $('html,body').scrollTop(scrollmem)
	})
	if (location.hash) {
	  setTimeout(function() {
	    window.scrollTo(0, 0);
	  }, 1);
	}
	//

	//показать/скрыть блок с телефоном в header при скролле - start
	var lastScrollTop = 0;
	$(window).scroll(function(event){
		var st = $(this).scrollTop();
		if (st > lastScrollTop){
		   if (!$('.header-wrapper').hasClass('phone-hide')) {
		   		$('.header-wrapper').addClass('phone-hide')
		   }
		} else {
		   if ($('.header-wrapper').hasClass('phone-hide')) {
		   		$('.header-wrapper').removeClass('phone-hide')
		   }
		}
		lastScrollTop = st;
	});
	//показать/скрыть блок с телефоном в header при скролле - end

	$(document).on('click', '.multi-city-popup__link', function(ev){
		ev.preventDefault()
		var city = $(this).text()
		$.post('/ajax.content/city_select.php', {'city': city}).done(function(resp){
			console.log(resp.match(/###(\w)*###/g))
			location.reload();
		}).fail(function(err){
			console.log(err)
		})
	})

	$(document).on('click', '.multi-city-dropdown__accept', function(ev){
		ev.preventDefault()
		var city = $('.multi-city-dropdown__your-city span').text()
		$.post('/ajax.content/city_select.php', {'city': city}).done(function(resp){
			console.log(resp.match(/###(\w)*###/g))
			location.reload();
		}).fail(function(err){
			console.log(err)
		})
	})


	$('.detail_picture').slick({
	  infinite: true,
	  slidesToShow: 6,
	  slidesToScroll: 3
	});
	
	// detect mobile
	function mobile() {
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			return (true);
		}
		else {
			return (false);
		}
	}
	// item page replace breadcrumbs
	if($('.product-inner-info').length && mobile()){
		$('.item-top-space').prependTo('.bx_item_detail');
		$('.first-item-breadcrumbs').prependTo('.bx_item_detail');
		$('.first-item-breadcrumbs').addClass('hide-breadcrumds-info');
	}
	// detect resize
	var prodConstainer = $('.products').outerWidth();
	$(window).resize(function () {
		prodConstainer = $('.products').outerWidth();
		console.log('resized, mobile is ' + mobile());
	});
	// activate side menu
	$('.activate-pusher').click(function () {
		if ( !$('html').hasClass('mm-menu-active') ){
			$('html').addClass('mm-menu-active');
		}else{
			$('html').removeClass('mm-menu-active');
		}
	});
	// close side menu
	$('.close-area').click(function () {
		$('html').removeClass('mm-menu-active');
	});
	// scrolling functions
	$(document).scroll(function () {
		if ($(this).scrollTop() > 0) {
			$('.header-wrapper').addClass('active');
		}
		else {
			$('.header-wrapper').removeClass('active');
		}
	});
	// masonry
	// if ($('.tiles').length) {
	// 	$('.tiles').masonry({
	// 		itemSelector: '.tile'
	// 		, columnWidth: '.tiles-sizer'
	// 		, gutter: '.tiles-gutter'
	// 		, percentPosition: true
	// 	});
	// }

	$('.start-slider').lightSlider({
		adaptiveHeight: false,
        item:1,
        slideMargin:0,
        loop:true,
        pager: false,
        slideMargin: 15,
        auto: true,
        pause: 3000
	})

	$('select').selectpicker();
	$('#lightSlider').lightSlider({
		item: 1
		, autoWidth: false
		, slideMove: 1, // slidemove will be 1 if loop is true
		slideMargin: 0
		, mode: "slide"
		, useCSS: true
		, cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
		easing: 'linear', //'for jquery animation',////
		speed: 400, //ms'
		auto: true
		, loop: true
		, slideEndAnimation: true
		, pause: 5000
		, keyPress: false
		, controls: true
		, prevHtml: ''
		, nextHtml: ''
		, rtl: false
		, adaptiveHeight: false
		, vertical: false
		, verticalHeight: 500
		, vThumbWidth: 100
		, thumbItem: 10
		, pager: true
		, gallery: false
		, galleryMargin: 5
		, thumbMargin: 5
		, currentPagerPosition: 'middle'
		, enableTouch: true
		, enableDrag: true
		, freeMove: true
		, swipeThreshold: 40
	});
	$('#lightSlider-gallery').lightSlider({
		item: 1
		, autoWidth: false
		, slideMove: 1, // slidemove will be 1 if loop is true
		slideMargin: 0
		, mode: "slide"
		, useCSS: true
		, cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
		easing: 'linear', //'for jquery animation',////
		speed: 400, //ms'
		auto: false
		, loop: true
		, slideEndAnimation: true
		, pause: 5000
		, keyPress: false
		, controls: true
		, prevHtml: ''
		, nextHtml: ''
		, rtl: false
		, adaptiveHeight: true
		, vertical: false
		, verticalHeight: 500
		, vThumbWidth: 100
		, thumbItem: 10
		, pager: false
		, gallery: false
		, galleryMargin: 5
		, thumbMargin: 5
		, currentPagerPosition: 'middle'
		, enableTouch: true
		, enableDrag: true
		, freeMove: true
		, swipeThreshold: 40
	});
	$('#autoWidth').lightSlider({
        autoWidth: false,
		item: 1,
		auto: true,
        loop: true,
        onSliderLoad: function() {
            $('#autoWidth').removeClass('cS-hidden');
        } 
    });
    $()
	// search
	$('.search-icon').click(function () {
		$('.search').addClass('active');
    $(this).parent().find('input').focus();
	});
	$('body').click(function (evt) {
		if (!$(evt.target).closest('.search').length) {
			$('.search').removeClass('active');
		}
	});
	$('.filter-header').click(function () {
		var parent = $(this).parent();
		if (parent.hasClass('active')) {
			parent.removeClass('active');
		}
		else {
			parent.addClass('active');
			parent.siblings().removeClass('active');
		}
	});
	$(".products-slider .products").lightSlider({
		item: 5
		, autoWidth: false
		, slideMove: 1, // slidemove will be 1 if loop is true
		slideMargin: 0
		, mode: "slide"
		, useCSS: true
		, cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
		easing: 'linear', //'for jquery animation',////
		speed: 400, //ms'
		auto: false
		, loop: false
		, slideEndAnimation: true
		, pause: 2000
		, slideMargin: 10
		, keyPress: false
		, controls: true
		, prevHtml: '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="35" viewBox="0 0 18 35"><path id="Shape-211-copy" d="M17.994,17.516 C17.994,17.372 17.940,17.228 17.830,17.119 C17.830,17.119 0.944,0.190 0.944,0.190 C0.725,-0.030 0.371,-0.030 0.152,0.190 C-0.066,0.409 -0.066,0.764 0.152,0.983 C0.152,0.983 16.643,17.516 16.643,17.516 C16.643,17.516 0.152,34.048 0.152,34.048 C-0.066,34.267 -0.066,34.622 0.152,34.842 C0.371,35.061 0.725,35.061 0.944,34.842 C0.944,34.842 17.830,17.912 17.830,17.912 C17.940,17.803 17.994,17.659 17.994,17.516 z" fill="#E0BA60" /></svg>'
		, nextHtml: '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="35" viewBox="0 0 18 35"><path id="Shape-211-copy" d="M17.994,17.516 C17.994,17.372 17.940,17.228 17.830,17.119 C17.830,17.119 0.944,0.190 0.944,0.190 C0.725,-0.030 0.371,-0.030 0.152,0.190 C-0.066,0.409 -0.066,0.764 0.152,0.983 C0.152,0.983 16.643,17.516 16.643,17.516 C16.643,17.516 0.152,34.048 0.152,34.048 C-0.066,34.267 -0.066,34.622 0.152,34.842 C0.371,35.061 0.725,35.061 0.944,34.842 C0.944,34.842 17.830,17.912 17.830,17.912 C17.940,17.803 17.994,17.659 17.994,17.516 z" fill="#E0BA60" /></svg>'
		, rtl: false
		, adaptiveHeight: false
		, vertical: false
		, // vThumbWidth: 100,
		thumbItem: 10
		, pager: false
		, gallery: true
		, galleryMargin: 10
		, thumbMargin: 5
		, currentPagerPosition: 'middle'
		, enableTouch: true
		, enableDrag: true
		, freeMove: true
		, swipeThreshold: 40
		, responsive: [{
			breakpoint: 1199
			, settings: {
				item: 3
			}
        }, {
			breakpoint: 551
			, settings: {
				item: 2
			}
        }, {
			breakpoint: 401
			, settings: {
				item: 1
			}
        }]
	});
    // animations
	$.fn.extend({
		animateCss: function (animationName) {
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			this.addClass('animated ' + animationName).one(animationEnd, function() {
				$(this).removeClass('animated ' + animationName);
			});
		}
	});

	$('.hdn-item').click(function(){
		$('.hand-items').fadeIn();
	});
	// popup close
	$('.popup-close-button').click(function(){
		$(this).parents('.popup').fadeOut();
		$(this).parents('.popup-body').animateCss('fadeOutDown');
	});
	$('.popup-close-area').click(function(){
		$(this).parents('.popup').fadeOut();
		$(this).parent().find('.popup-body').animateCss('fadeOutDown');
	});
	// select
	//$('.select-custom').multiSelect();
	$('.searchable').multiSelect({
		selectableHeader: "<input type='text' class='search-input filter-name' autocomplete='off' placeholder='Каталог'>"
		, selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Выбранное'>"
		, afterInit: function (ms) {
			var that = this
				, $selectableSearch = that.$selectableUl.prev()
				, $selectionSearch = that.$selectionUl.prev()
				, selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)'
				, selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';
			//console.log(that.$container.parents(".filter li").find(".filter-header").text().trim());
			that.$container.find(".filter-name").attr("placeholder",that.$container.parents(".filter li").find(".filter-header").text().trim());
			that.qs1 = $selectableSearch.quicksearch(selectableSearchString).on('keydown', function (e) {
				if (e.which === 40) {
					that.$selectableUl.focus();
					return false;
				}
			});
			that.qs2 = $selectionSearch.quicksearch(selectionSearchString).on('keydown', function (e) {
				if (e.which == 40) {
					that.$selectionUl.focus();
					return false;
				}
			});
			$('.ms-list').mCustomScrollbar();
		}
		, afterSelect: function () {
			this.qs1.cache();
			this.qs2.cache();
			$('.ms-list').mCustomScrollbar();
		}
		, afterDeselect: function () {
			this.qs1.cache();
			this.qs2.cache();
		}
	});
	// basket scroll
	$('.basket .scroll').mCustomScrollbar();

	// multi-ity scroll scroll
	$('.multi-city-popup .multi-city-popup__container .multi-city-popup__list').mCustomScrollbar({
		theme:"my-theme"
	});
	
	// range
	$('.range').ionRangeSlider({
		min: 0,
		max: 10000,
		from: 1000,
		to: 9000,
		type: 'double',
		prefix: "₽",
		grid: true,
	});
	// tabs
	$('.nav-item-dropdown-tabs a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});
	// mobile menu
	var openedLi;
	$('.mobile-menu a.parent-item,.mobile-menu span').click(function(){
		openedLi = $(this).parent();
		openedLi.addClass('active');
		return false;
	});
	$('.mm-parent li p').click(function(){
		$(this).closest('.active').removeClass('active');
	});
	// checkbox
	$('.checkbox').click(function(){
		if(!$(this).hasClass('active')){
			$(this).addClass('active');
		}else{
			$(this).removeClass('active');
		}
	});
    // floating basket
    $( document ).on( "click", ".mb-open", function() {
        console.log('1');
        var main = $(this).parent();
        if (main.hasClass('active')){
            main.removeClass('active');
        }else{
            main.addClass('active');
        }
    });
    $(document).mouseup(function (e){
        var megabasket = $('.megabasket');
        if (!megabasket.is(e.target) && megabasket.has(e.target).length === 0) {
            megabasket.removeClass('active');
        }
    });
    // mask
    $("#ORDER_PROP_3,.handthings-check .phone_masked_input, .chat-form__row .phone-inp").mask("+7(999) 999-99-99",{placeholder:"+7(___) ___-__-__"});

    // WHATSAPP FORM
    $('.chat-form').on('submit', function(ev){
    	ev.preventDefault()
    	var data = $(this).serialize()
    	$('.chat-form__submit').text('Загурзка...')
    	$.post('/ajax.content/form.whatsapp.php', data, function(resp){
	    	if(resp == 'okidoki')
	    		$('.chat-form__submit').text('Отправлено')
	    	else
	    		$('.chat-form__submit').text('Не отправлено')
    		// console.log(resp)
    	})
    })
    

});


	window.globals = {
	    gallery: ""
	};
	$( document ).on( "click", ".open-quickview-popup", function() {
    $(".popup.quickview").fadeIn();
    $('.popup.quickview .popup-body').animateCss('fadeInUp');
    var slider = $(".quickview-slider").lightSlider({
      item: 1,
      loop: true,
      slideMargin: 0,
      adaptiveHeight: true
    });


    window.globals = {
        gallery: slider
    };
    return false;
  });
  $(document).ready(function () {
    setTimeout(checkAjaxProducts, 1000);
  });
  var sl = false;
  function checkAjaxProducts() {
    if ($('.products-container .products .product').length == 0) return setTimeout(checkAjaxProducts, 1000);
    $('.products-container .products .product-img').on('mouseover', function() {
      var second = $(this).data('second-photo');
      if (second != '') $(this).css('background-image', 'url(' + second + ')');
    });
    $('.products-container .products .product-img').on('mouseleave', function() {
      var first = $(this).data('first-photo');
      if (first != '') $(this).css('background-image', 'url(' + first + ')');
    });
    /*$('.products-container .products .product a').on('mouseover', function() {*/
    /*$('.products-container .products .product a').on('click', function() {*/
    $('.qwiev').on('click', function() {
      var jscontent = $(this).closest('.product').find('.qw').data('js');
      
      var popup = $('.popup.quickview');

      var sliderItemHtml = popup.find('.quickview-slider-templ .item')[0].outerHTML;
      popup.find('.quickview-slider').html(sliderItemHtml);
      popup.find('.quickview-slider .item img').attr('src',jscontent.img.src);
      $.each( jscontent.more_photo, function( key, value ) {
        popup.find('.quickview-slider').append(sliderItemHtml);
        popup.find('.quickview-slider .item img').last().attr('src',value);
      });
      popup.find('.product-title').html(jscontent.name);
      popup.find('.old-price-val').html(jscontent.tsena);
      popup.find('.price-val').html(jscontent.price);
      popup.find('.product-desc').html(jscontent.desc);
      popup.find('.btn-more').attr('href',jscontent.url);
      popup.find('.order-fit').attr('product_id',jscontent.id);
      popup.find('.order-fit').attr('product_name',jscontent.name);
      popup.find('.btn-oneclick').attr('product_id',jscontent.id);
      popup.find('.btn-oneclick').attr('product_name',jscontent.name);
      if (jscontent.disabled==1) {
        popup.find('.addtocartid').attr('disabled','disabled');
        popup.find('.btn-oneclick').attr('disabled','disabled');
      }
      
      popup.find('.addtocartid').on('click', function() {
        $(this).parents('.popup').fadeOut();
        $(this).parents('.popup-body').animateCss('fadeOutDown');
        addtocart(jscontent.id,jscontent.razmer,jscontent.tsvet,jscontent.name);
        return false;
      });
      popup.find('.btn-oneclick').on('click', function() {
        $(this).parents('.popup').fadeOut();
        $(this).parents('.popup-body').animateCss('fadeOutDown');
        return true;
      });
      popup.find('.order-fit').on('click', function() {
        $(this).parents('.popup').fadeOut();
        $(this).parents('.popup-body').animateCss('fadeOutDown');
        return true;
      });
      
      $(".popup.quickview").fadeIn();
      $('.popup.quickview .popup-body').animateCss('fadeInUp');
      if (sl !== false) sl.destroy();
      sl = $(".quickview-slider").lightSlider({
        item: 1,
        loop: true,
        adaptiveHeight: true
      });
      return false;
    });

    //multi-city-popup
    $('.multi-city-dropdown__change').on('click', ()=> {
    	$('.multi-city-popup').removeClass('close')
    	$('.multi-city-popup-wrapp').removeClass('close')
    })

    $(document).on('click', function(event) {
		var target = $( event.target );

	  if ( target.is( ".multi-city-popup-wrapp" ) ) {
	    $('.multi-city-popup').addClass('close')
    	$('.multi-city-popup-wrapp').addClass('close')
	  }
	})

    $('.multi-city-popup__close').on('click', ()=> {
    	$('.multi-city-popup').addClass('close')
    	$('.multi-city-popup-wrapp').addClass('close')
    })

    

    function getCookie(name) {
	  let matches = document.cookie.match(new RegExp(
	    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	  ));
	  return matches ? decodeURIComponent(matches[1]) : undefined;
	}

    let currentCity = getCookie('BITRIX_SM_multi_city');

    if (currentCity == undefined) {
		$('.multi-city').addClass('active');
    } else {}

    $('.multi-city').hover(
		function(){
		 	$('.multi-city').addClass('active');
		},
		function(){
		  $('.multi-city').removeClass('active');
		});


  }



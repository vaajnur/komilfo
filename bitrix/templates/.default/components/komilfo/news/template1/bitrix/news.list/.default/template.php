<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//echo $arResult["NAV_STRING"];
$arrayUL = strip_tags($arResult["NAV_STRING"]);
$pieces =  explode("\t", $arrayUL);
$pieces=array_unique($pieces);
$curPage=isset($_GET['PAGEN_1'])?$_GET['PAGEN_1']:1;
$totalPages=1;
$i=0;
foreach($pieces as $piece){
    $i++;
    if($i==count($pieces)-2)
        $totalPages=$piece;
}
?>
<div class="container blog-container">
        <!-- анимация загрузки ниже, срабатывает если есть класс loading у родителя выше -->
    <div class="load-container">
        <div class="load-circle"></div>
        <div class="load-circle"></div>
    </div>
    <?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?
     $APPLICATION->IncludeComponent("komilfo:new.pagenavigation","",Array(
         "buttontxt"=>"Показать еще статьи",
         "SHOW_ALL"=>"N",
         "top"=>"top",
         "LAST_ELEMENT_NUMBER"=>$end,
         "SHOW_ALL_TEXT"=>"Показать все",
         "curPage"=>$curPage,
         "totalPages"=>$totalPages,
         "navNum"=>$navNum,
         "show_ajax"=>$show_ajax,
         "root"=>"catalog-root",
         "newsBlock"=>"catalog-body"
         )
     );
     ?>
    <?endif;?>
    
    <div class="row">
    <? if ($_REQUEST['LOAD']=="Y")
       $APPLICATION->RestartBuffer();
    ?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="col-md-4 col-sm-6" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="blog-item">
                <div class="blog-item-img" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a></div>
                <div class="blog-item-bottom">
                    <p><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a></p>
                    <span><?echo $arItem["PREVIEW_TEXT"]?>.</span>
                    <div class="blog-item-controls">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Читать</a>
                        <p><?echo date("d.m.Y",strtotime($arItem["DATE_CREATE"]))?></p>
                    </div>
                </div>
            </div>
        </div>
    <?endforeach;?>
    <? if ($_REQUEST['LOAD']=="Y")
        die();
    ?>
    </div>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?
     $APPLICATION->IncludeComponent("komilfo:new.pagenavigation","",Array(
         "buttontxt"=>"Показать еще статьи",
         "SHOW_ALL"=>"Y",
         "LAST_ELEMENT_NUMBER"=>$end,
         "SHOW_ALL_TEXT"=>"Показать все",
         "curPage"=>$curPage,
         "totalPages"=>$totalPages,
         "navNum"=>$navNum,
         "show_ajax"=>$show_ajax,
         "root"=>"catalog-root",
         "newsBlock"=>"catalog-body"
         )
     );
     ?>
    <?endif;?>
</div>

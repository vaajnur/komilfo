<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="container news-detail">
		<? if(strlen($arResult["DETAIL_PICTURE"]["SRC"])>0){?>
		<div class="detail_picture" style="background-image: url(<?=$arResult["DETAIL_PICTURE"]["SRC"]?>)"></div>
		<?}?>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
				<?endif?>
				<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
					<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
				<?endif;?>
				<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
					<h3><?=$arResult["NAME"]?></h3>
				<?endif;?>
					<div class="blog-innner-text"><?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?></div>
					<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
				<?endif;?>
				<?if($arResult["NAV_RESULT"]):?>
					<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
					<?echo $arResult["NAV_TEXT"];?>
					<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
				<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
					<?echo $arResult["DETAIL_TEXT"];?>
				<?else:?>
					<?echo $arResult["PREVIEW_TEXT"];?>
				<?endif?>
				<div style="clear:both"></div>
				<br />
				<?foreach($arResult["FIELDS"] as $code=>$value):
					if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
					{
						?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
						if (!empty($value) && is_array($value))
						{
							?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
						}
					}
					else
					{
						?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
					}
					?><br />
				<?endforeach;
				?>
				
            </div>
            <ul id="lightSlider-gallery">
                <!-- нужно задавать цвет для контейнера, чтобы он был на всю ширину. впринципе можно и без контейнера вставлять картинку если не важна центровка -->
                <? foreach($arResult["DISPLAY_PROPERTIES"]['GALLERY']['VALUE'] as $img){?>
                    <? 
                    $img_src_full=CFile::GetPath($img);
                    $img_src_thumb=CFile::GetPath($img); ?>
                <li style="background-color: <?=strlen($item['COLOR'])>0?$item['COLOR']:"#000";?>">
                    <div class="containerr"><a class="img-popups" data-fancybox="images" href="<? echo $img_src_full; ?>"><img class="" src="<? echo $img_src_thumb; ?>" alt=""></a>
                    </div>
                </li> 
                <?}?>
            </ul>
		</div>
</div></div>
<? if(count($arResult["DISPLAY_PROPERTIES"]['GALLERY']['VALUE'])>0){?>
<div class="container">

 </div>
<?}?>
<div class="products-slider"> 
<?
    $GLOBALS["FILTER_LINKED_CATALOG"]=array("ID"=>$arResult["DISPLAY_PROPERTIES"]['GOODS']['VALUE'],">CATALOG_PRICE2"=>0,">CATALOG_QUANTITY"=>0,"!DETAIL_PICTURE"=>false);
    $APPLICATION->IncludeComponent(
        "komilfo:catalog.section",
        "slider",
        Array(
            "TEMPLATE_THEME" => "blue",
            "PRODUCT_DISPLAY_MODE" => "N",
            "PRODUCT_SUBSCRIPTION" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_CLOSE_POPUP" => "Y",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "SEF_MODE" => "Y",
            "IBLOCK_ID" => CATALOG_IBLOCK_ID_CONST,
            "SECTION_CODE" => "",
            "SECTION_USER_FIELDS" => array(),
            "ELEMENT_SORT_FIELD" => "RAND",
            "ELEMENT_SORT_ORDER" => "ASC",
            "ELEMENT_SORT_FIELD2" => "CATALOG_AVAILABLE",
            "ELEMENT_SORT_ORDER2" => "DESC",
            "FILTER_NAME" => "FILTER_LINKED_CATALOG",
            "INCLUDE_SUBSECTIONS" => "Y",
            "SECTION_CODE_PATH"=>"",
            "SHOW_ALL_WO_SECTION" => "Y",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "SECTION_ID_VARIABLE" => "",
            "ADD_SECTIONS_CHAIN" => "N",
            "DISPLAY_COMPARE" => "N",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "BROWSER_TITLE" => "-",
            "SET_META_KEYWORDS" => "N",
            "META_KEYWORDS" => "",
            "SET_META_DESCRIPTION" => "N",
            "META_DESCRIPTION" => "",
            "SET_LAST_MODIFIED" => "Y",
            "SET_STATUS_404" => "N",
            "PAGE_ELEMENT_COUNT" => 10,
            "LINE_ELEMENT_COUNT" => "3",
            "PROPERTY_CODE" => array("RAZMER_TOVARA","TSVET","CML2_MANUFACTURER","TSENA_V_OFITS_BUTIKE","IZNOS","SOSTAV","AKTSIYA"),
            "OFFERS_FIELD_CODE" => array(),
            "OFFERS_PROPERTY_CODE" => array(),
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_ORDER" => "asc",
            "OFFERS_SORT_FIELD2" => "active_from",
            "OFFERS_SORT_ORDER2" => "desc",
            "OFFERS_LIMIT" => "0",
            "BACKGROUND_IMAGE" => "-",
            "PRICE_CODE" => array(
                0 => "Интернет - магазин",
            ),
            "USE_PRICE_COUNT" => "Y",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_PROPERTIES" => array(),
            "USE_PRODUCT_QUANTITY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",

            "HIDE_NOT_AVAILABLE" => "N",
            "OFFERS_CART_PROPERTIES" => array(),
            "CONVERT_CURRENCY" => "Y",
            "CURRENCY_ID" => "RUB",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "PAGER_BASE_LINK_ENABLE" => "Y",
            "SET_STATUS_404" => "Y",
            "SHOW_404" => "Y",
            "MESSAGE_404" => "",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "PAGER_BASE_LINK" => "",
            "PAGER_PARAMS_NAME" => "arrPager"
        )
    );?>
</div>

<div class="container">
    <div class="row signup-in-blog">
        <div class="col-md-8 col-md-offset-2 no-md-padding">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-in-blog">
                            <p class="h2">Понравилась статья?</p>
                            <p>Получайте свежие новости, советы от<br>стилистов, информацию о закрытых<br>распродажах и акциях на почту!</p>
                            <?$APPLICATION->IncludeComponent("komilfo:subscribe.form","blog_detail_form",Array(
                                "USE_PERSONALIZATION" => "Y",
                                "PAGE" => "/ajax.content/subscribe.php",
                                "SHOW_HIDDEN" => "Y",
                                "RUBRIC_ID"=>3,
                                "CODE_FORM"=>"MAINPAGE",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "3600"
                                    )
                            );?>
                        </div>
                        <br>
                        <p class="h3">Полезная статья? Отправьте друзьям:</p>
                        <div class="soc-in-blog">
                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                            <script src="//yastatic.net/share2/share.js"></script>
                            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter"></div>
                        </div>
                    </div>
                </div>

        </div>
    </div>
</div>
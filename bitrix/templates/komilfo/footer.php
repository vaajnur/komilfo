<!-- parts/footer.html --> 
</div>
<div class="prefooter">
    <div class="container">
        <div class="prefooter-container" itemscope itemtype="http://schema.org/SiteNavigationElement">
            <?$APPLICATION->IncludeComponent("komilfo:menu","links",Array(
                    "ROOT_MENU_TYPE" => "prefooter", 
                    "MAX_LEVEL" => "1", 
                    "CHILD_MENU_TYPE" => "prefooter", 
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "N", 
                    "MENU_CACHE_TIME" => "3600", 
                    "MENU_CACHE_USE_GROUPS" => "Y", 
                    "MENU_CACHE_GET_VARS" => "" 
                )
            );?>
        </div>
    </div>
</div>
<div class="footer" itemscope itemtype="http://schema.org/WPFooter">
	<meta itemprop="copyrightYear" content="<?=date('yy');?>">
	<meta itemprop="copyrightHolder" content="Комильфо-бутик">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-xs-6" itemscope itemtype="http://schema.org/SiteNavigationElement">
                        <p class="header">Комильфо-бутик</p>
                        <?$APPLICATION->IncludeComponent("komilfo:menu","links",Array(
                                "ROOT_MENU_TYPE" => "footer1", 
                                "MAX_LEVEL" => "1", 
                                "CHILD_MENU_TYPE" => "footer1", 
                                "USE_EXT" => "Y",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "Y",
                                "MENU_CACHE_TYPE" => "N", 
                                "MENU_CACHE_TIME" => "3600", 
                                "MENU_CACHE_USE_GROUPS" => "Y", 
                                "MENU_CACHE_GET_VARS" => "" 
                            )
                        );?>
                        <div class="payment">
                            <? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-visa.svg' ?>
                            <? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-mastercard.svg' ?>
                        </div>
                    </div>
                    <div class="col-xs-6" itemscope itemtype="http://schema.org/SiteNavigationElement">
                        <p class="header">Покупателям</p>
                        <?$APPLICATION->IncludeComponent("komilfo:menu","links",Array(
                                "ROOT_MENU_TYPE" => "footer2", 
                                "MAX_LEVEL" => "1", 
                                "CHILD_MENU_TYPE" => "footer2", 
                                "USE_EXT" => "Y",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "Y",
                                "MENU_CACHE_TYPE" => "N", 
                                "MENU_CACHE_TIME" => "3600", 
                                "MENU_CACHE_USE_GROUPS" => "Y", 
                                "MENU_CACHE_GET_VARS" => "" 
                            )
                        );?>
                        <a href="/howtospotfake">Проверка на оригинальность</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-offset-4 col-sm-4 text-right contacts-footer">
                <p class="header">Контакты</p>
                <div class="socials" itemscope itemtype="http://schema.org/SiteNavigationElement">
                    <a itemprop="url" target="_blank" href="https://vk.com/butikkomilfo"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-vk.svg' ?></a>
                    <a itemprop="url" target="_blank" href="https://facebook.com/komilfobutik/"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-fb.svg' ?></a>
                    <a itemprop="url" target="_blank" href="https://www.instagram.com/komilfo_resale/"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-inst.svg' ?></a>

                </div>
                <a class="not-p-b" href="tel:+78003336794">8 (800) 333-67-94</a>
                <a class="not-p-b" href="mailto:info@komilfo-butik.com">info@komilfo-butik.com</a>
                <p>Санкт-Петербург</p>
                <p>ул. Садовая, 28</p>
                <p>м. Гостиный двор</p>
                <p>Малая Посадская, 2</p>
                <p>м. Горьковская</p>
                <p>Невский проспект, 10</p>
                <p>м. Адмиралтейская</p>
            </div>
        </div>
        <div class="row top-margin">
            <div class="col-sm-4" itemscope itemtype="http://schema.org/SiteNavigationElement">
                <p>© 2013-<?=date("Y");?> Комильфо-бутик</p>
                <a itemprop="url" class="line inline right-margin" href="/sitemap/">Карта сайта</a>
                <a itemprop="url" class="line inline" href="/privacy_policy/">Политика конфиденциальности</a>
            </div>
            <div class="col-sm-offset-4 col-sm-4 text-right footer-develop">
                <noindex><a href="http://arkvision.pro" target="_blank" >
                    <span>Разработано с умом</span>
                    <? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-ark.svg' ?>
					</a></noindex>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/popups.php" //Указываем путь к файлу
  )
);?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/lightslider.min.js"></script>
<script src="/js/masonry.pkgd.min.js"></script>
<script src="/js/jquery.fancybox.js"></script>
<script src="/js/ion.rangeSlider.min.js"></script>
<script src="/js/multiselect.js"></script>
<script src="/js/custom-scroll.js"></script>
<script src="/js/quicksearch.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/scripts-back.js"></script>
<script src="/js/ingevents.4.0.8.js"></script>

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.min.css"/>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox.min.js"></script>

 <!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '67972';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<div itemscope itemtype="http://schema.org/Organization">
    <meta itemprop="name" content="'Комильфо-бутик' ИП Федотовский Андрей Владимирович">
    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <meta itemprop="streetAddress" content="м. Гостиный двор, ул. Садовая д. 28">
        <meta itemprop="postalCode" content="191023">
        <meta itemprop="addressLocality" content="г. Санкт-Петербург">
    </div>
    <meta itemprop="telephone" content="+7 (812) 425-37-93">
    <meta itemprop="email" content="info@komilfo-butik.com">
</div>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "address": {
            "@type": "PostalAddress",
            "addressLocality": "г. Санкт-Петербург",
            "postalCode": "191023",
            "streetAddress": "м. Гостиный двор, ул. Садовая д. 28"
        },
        "email": "info@komilfo-butik.com",
        "name": "'Комильфо-бутик' ИП Федотовский Андрей Владимирович",
        "telephone": "+7 (812) 425-37-93"
    }
</script>
</body>
</html>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
// var_dump('<pre>',$arResult,'</pre>');
if (!empty($arResult['ITEMS']))
{
?>
  <div class="line"></div>
  <div class="container">
      <p class="h3">Вы недавно просматривали</p>
  </div>
  <div class="products-slider">
  
<div class="products-container loading">
    <!-- анимация загрузки ниже, срабатывает если есть класс loading у родителя выше -->
    <div class="load-container">
        <div class="load-circle"></div>
        <div class="load-circle"></div>
    </div>
    <div class="container">
        <ul class="products">
<?
$index=0;
$tmp_script="";
$qwJs=array();
foreach($arResult["ITEMS"] as $arElement):
$index++;
$nav = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
$category="";
while ($arNav=$nav->GetNext()):
	$category.=$arNav["NAME"].'/';
endwhile;
$myImg= CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array('width'=>330, 'height'=>330), BX_RESIZE_IMAGE_PROPORTIONAL , true);
            $qwJs['more_photo'] = array();
            $js_second_photo = '';
            if($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"]!=null){
              foreach($arElement['PROPERTIES']['MORE_PHOTO']["VALUE"] as $img){
                $qwJs['more_photo'][] = CFile::GetPath($img);
              }
              if (!empty($qwJs['more_photo'][0])) $js_second_photo = $qwJs['more_photo'][0];
            }
?>
    <li class="product <? if($arElement["PROPERTIES"]["AKTSIYA"]["VALUE"]!=""){?>product-on-sale<?}?> lslide">
        <div class="product-img" style="background-image: url('<?=(isset($myImg['src']) ? $myImg['src'] : '/upload/iblock/base-pic.jpg');?>')" data-first-photo="<?=$myImg['src']?>" data-second-photo="<?=$js_second_photo?>">
            <!-- линк на товар -->
            <a href="<?=$arElement["DETAIL_PAGE_URL"];?>"></a>
            <img src="<?=$js_second_photo?>" style="display:none;">
        </div>
        <div class="product-price">
            <?
            $priceDiscount = 0;
            $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arElement['ID'], 'CATALOG_GROUP_ID' => 2)); 
	       if ($arPrice = $rsPrices->Fetch()) $price = floor($arPrice["PRICE"]);
            ?>
            <? if(strlen($arElement["DISPLAY_PROPERTIES"]["TSENA_V_OFITS_BUTIKE"]["VALUE"])>0){?>
                <?  $priceDiscount = ($price/$arElement["DISPLAY_PROPERTIES"]["TSENA_V_OFITS_BUTIKE"]["VALUE"])*100;?>
            <div class="product-old-price">
            <? switch($_COOKIE['valute']){
                case "USD":
                ?><?=number_format(CCurrencyRates::ConvertCurrency($arElement["DISPLAY_PROPERTIES"]["TSENA_V_OFITS_BUTIKE"]["VALUE"], "RUB", "USD"), 0, ',', ' ');?><i> $</i><?
                break;
                case "EUR":
                ?><?=number_format(CCurrencyRates::ConvertCurrency($arElement["DISPLAY_PROPERTIES"]["TSENA_V_OFITS_BUTIKE"]["VALUE"], "RUB", "EUR"), 0, ',', ' ');?><i> €</i><?
                break;

                default:
                ?><? echo $arElement["DISPLAY_PROPERTIES"]["TSENA_V_OFITS_BUTIKE"]["VALUE"];?><i><? echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/images/komilfo/icon-ruble.svg") ; ?></i><?
                break;
            }?>
            </div>
            <?}?>
            <div class="product-new-price">
            <? switch($_COOKIE['valute']){
                case "USD":
                ?><?=number_format(CCurrencyRates::ConvertCurrency($price, "RUB", "USD"), 0, ',', ' ');?><i>$</i><?
                break;
                case "EUR":
                ?><?=number_format(CCurrencyRates::ConvertCurrency($price, "RUB", "EUR"), 0, ',', ' ');?><i>€</i><?
                break;

                default:
                ?><?=number_format($price, 0, ',', ' ');?><i><? echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/images/komilfo/icon-ruble.svg") ; ?></i><?
                break;
            }?>
            </div>
        </div>
        <? if($priceDiscount>0){ ?>
            <div class="product-discount">-<?= ceil($priceDiscount) ?>%</div>
        <?}?>
        <? if($arElement["PROPERTIES"]["AKTSIYA"]["VALUE"]!=""){ ?><div class="product-label ">АКЦИЯ</div><?} else{?><?}?>
        <div class="product-info">
            <p class="product-header"><?=explode(" ",$arElement["NAME"])[0]?> <?=$arElement["DISPLAY_PROPERTIES"]["CML2_MANUFACTURER"]["VALUE"];?></p>
            <div class="product-parameters">
                <? if(strlen($arElement["PROPERTIES"]["RAZMER_TOVARA"]["VALUE"])>0){?>
                <div class="product-select">
                    <p>Размер: <?=strlen($arElement["PROPERTIES"]["RAZMER_TOVARA"]["VALUE"])>0?$arElement["PROPERTIES"]["RAZMER_TOVARA"]["VALUE"]:"не указан";?></p>
                </div>
                <?}?>
                <? if(strlen($arElement["PROPERTIES"]["IZNOS"]["VALUE"])>0){?>
                <div class="product-select">
                    <?
                    if(strlen($arElement["PROPERTIES"]["IZNOS"]["VALUE"])>0){
                        switch ($arElement["PROPERTIES"]["IZNOS"]["VALUE"]){
                            case 0:
                                $arElement["PROPERTIES"]["IZNOS"]["VALUE"]="Новое";
                                break;
                            case 5:
                                $arElement["PROPERTIES"]["IZNOS"]["VALUE"]="Идеальное";
                                break;
                            case 10:
                                $arElement["PROPERTIES"]["IZNOS"]["VALUE"]="Хорошее";
                                break;
                            default:
                                $arElement["PROPERTIES"]["IZNOS"]["VALUE"]=$arElement["PROPERTIES"]["IZNOS"]["VALUE"]."%";
                        }
                        
                    }
                    ?>
                    <p>Состояние: <?=strlen($arElement["PROPERTIES"]["IZNOS"]["VALUE"])>0?$arElement["PROPERTIES"]["IZNOS"]["VALUE"]."":"не указано";?></p>
                </div>
                <?}?>
            </div>
            <?
             //количество товаров в корзине (без цикла) 
             $cntBasketItems = CSaleBasket::GetList(
                array(),
                array( 
                   "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                   "LID" => SITE_ID,
                   "ORDER_ID" => "NULL",
                   "PRODUCT_ID"=> $arElement["ID"]
                ), 
                array()
             );
             $product=CCatalogProduct::GetByID(
              $arElement["ID"]
             );
             ?>
            <div class="product-actions">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>">Подробнее</a>
                <a  class="<?if($cntBasketItems>=$product['QUANTITY']){?>disabled<?}?>" id="addtocart<?=$arElement["ID"];?>" onclick="addtocart('<?=$arElement["ID"]?>','<?=$arElement["DISPLAY_PROPERTIES"]["RAZMER_TOVARA"]["VALUE"];?>','<?=$arElement["DISPLAY_PROPERTIES"]["TSVET"]["VALUE"];?>','<?=explode(" ",$arElement["NAME"])[0]?> <?=$arElement["DISPLAY_PROPERTIES"]["CML2_MANUFACTURER"]["VALUE"];?>'); return false;" href="">В корзину</a>
            </div>
        </div>
        <?
            $tmp_script.="{  'id': '".$arElement["ID"]."',
              'list': '".$_SERVER['REQUEST_URI']."',
              'price': '".$price."',		// Цена товара	
              'brand': '".addslashes($arElement["PROPERTIES"]["CML2_MANUFACTURER"]["VALUE"])."',
              'category': '".$category."',
              'position': ".$index."
           },";
        ?>
    </li>
<?endforeach;?>
</ul>
     <script>
      console.log('viewed prods')
      if(typeof dataLayer != 'undefined'){
      dataLayer.push({
        'ecommerce': {
          'event': 'impression',                       
          'impressions': 
          [
              <?=$tmp_script;?>
          ]
        }
      });
    }
    </script>
    </div>
</div>

</div>
<? } ?>
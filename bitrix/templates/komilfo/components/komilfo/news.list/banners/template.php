<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>    <div class="container tiles-container">
      <div class="tiles">
        <?
        $myImg= CFile::ResizeImageGet($arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['IMAGE']['VALUE'], array('width'=>600, 'height'=>600), BX_RESIZE_IMAGE_PROPORTIONAL , true);
        ?>
        <div class="tile width-3 tile-image">
            <a href="<?=$arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['LINK']['VALUE'];?>" class="tile-inner" style="background-image: url('<?=$myImg['src'];?>')">
            <? if($arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['SHOW_BUTTON']['VALUE']=="Y"){?>
                    <? if(strlen($arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                    <span class="tile-line"></span>
                    <span class="tile-header"><?=$arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['HEAD']['VALUE'];?></span>
                    <?}?>
                    <? if(strlen($arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"])>0){?>
                    <span class="tile-description"><?=$arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"];?></span>
                    <?}?>
                    <span class="banner-button"><span class="btn btn-orange"><?=$arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['TEXT']['VALUE'];?></span></span>
                <?} else{
                    if(strlen($arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['LINK']['VALUE'])>0){?>
                    <a href="<?=$arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['LINK']['VALUE'];?>" class="tile-phantom-link"></a>
                    <?}?>
                    <? if(strlen($arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                    <div class="tile-line"></div>
                    <? if(strlen($arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                    <div class="tile-header"><?=$arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['HEAD']['VALUE'];?></div>
                    <?}?>
                    <? if(strlen($arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"])>0){?>
                    <div class="tile-description"><?=$arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"];?></div>
                    <?}?>
                    <?}?>
                <?}?>
            </a>
        </div>
        <div class="tile-wrapper">
            <?
            $myImg= CFile::ResizeImageGet($arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['IMAGE']['VALUE'], array('width'=>600, 'height'=>600), BX_RESIZE_IMAGE_PROPORTIONAL , true);
            ?>
            <div class="tile width-2 tile-image">
                <a href="<?=$arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['LINK']['VALUE'];?>" class="tile-inner" style="background-image: url('<?=$myImg['src'];?>')">
                <? if($arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['SHOW_BUTTON']['VALUE']=="Y"){?>
                        <? if(strlen($arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                        <span class="tile-line"></span>
                        <span class="tile-header"><?=$arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['HEAD']['VALUE'];?></span>
                        <?}?>
                        <? if(strlen($arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"])>0){?>
                        <span class="tile-description"><?=$arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"];?></span>
                        <?}?>
                        <span class="banner-button"><span class="btn btn-orange"><?=$arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['TEXT']['VALUE'];?></span></span>
                    <?} else{
                        if(strlen($arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['LINK']['VALUE'])>0){?>
                        <a href="<?=$arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['LINK']['VALUE'];?>" class="tile-phantom-link"></a>
                        <?}?>
                        <? if(strlen($arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                        <div class="tile-line"></div>
                        <? if(strlen($arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                        <div class="tile-header"><?=$arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['HEAD']['VALUE'];?></div>
                        <?}?>
                        <? if(strlen($arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"])>0){?>
                        <div class="tile-description"><?=$arResult["ITEMS"][1]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"];?></div>
                        <?}?>
                        <?}?>
                    <?}?>
                </a>
            </div>
            <?
            $myImg= CFile::ResizeImageGet($arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['IMAGE']['VALUE'], array('width'=>600, 'height'=>600), BX_RESIZE_IMAGE_PROPORTIONAL , true);
            ?>
            <div class="tile width-2 tile-image">
                <a href="<?=$arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['LINK']['VALUE'];?>" class="tile-inner" style="background-image: url('<?=$myImg['src'];?>')">
                <? if($arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['SHOW_BUTTON']['VALUE']=="Y"){?>
                        <? if(strlen($arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                        <span class="tile-line"></span>
                        <span class="tile-header"><?=$arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['HEAD']['VALUE'];?></span>
                        <?}?>
                        <? if(strlen($arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"])>0){?>
                        <span class="tile-description"><?=$arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"];?></span>
                        <?}?>
                        <span class="banner-button"><span class="btn btn-orange"><?=$arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['TEXT']['VALUE'];?></span></span>
                    <?} else{
                        if(strlen($arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['LINK']['VALUE'])>0){?>
                        <a href="<?=$arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['LINK']['VALUE'];?>" class="tile-phantom-link"></a>
                        <?}?>
                        <? if(strlen($arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                        <div class="tile-line"></div>
                        <? if(strlen($arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['HEAD']['VALUE'])>0){?>
                        <div class="tile-header"><?=$arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['HEAD']['VALUE'];?></div>
                        <?}?>
                        <? if(strlen($arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"])>0){?>
                        <div class="tile-description"><?=$arResult["ITEMS"][2]['DISPLAY_PROPERTIES']['DESCRIPTION']['VALUE']["TEXT"];?></div>
                        <?}?>
                        <?}?>
                    <?}?>
                </a>
            </div>
        </div>
    </div>
</div>
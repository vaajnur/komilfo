<ul id="lightSlider">
     <!-- нужно задавать цвет для контейнера, чтобы он был на всю ширину. впринципе можно и без контейнера вставлять картинку если не важна центровка -->
    <? foreach($arResult['ITEMS'] as $item){?>
     <li style="background-color: <?=strlen($item['COLOR'])>0?$item['COLOR']:"#000";?>">
		 <a href="<?=strlen($item['LINK'])>0?$item['LINK']:"/zhenskoe/";?>" class="container lightSlider-container" style="background-image: url(<?=$item['IMAGE']?>)">
			<span class="lightSlider-wrapper">
				<span class="lightSlider-title">Бренды класса люкс <br>со скидками до 80%</span>
				<span class="lightSlider-text">Разумное потребление <br>для <br>современных людей</span>
			 </span>
            <span class="btn btn-fullorange lightSlider-link"><?=strlen($item['TEXT'])>0?$item['TEXT']:"Перейти в каталог";?></span>
         </a>
     </li>
    <?}?>
 </ul>
 <div class="teaser-footer">
     <div class="container">
         <div class="space-between">

         </div>
     </div>
 </div>
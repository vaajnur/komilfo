<!-- parts/head.html -->
<!DOCTYPE html>
<html lang="ru">
<head itemscope itemtype="http://schema.org/WPHeader">
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1054581177924045');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1054581177924045&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(20825989, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/20825989" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

	<?
    global $USER;
	if($_GET['logout']=="Y")
		$USER->Logout();
	if ($_REQUEST['valute']=="USD") {
		setcookie("valute", "USD", time()+360000, "/");
	}
	if ($_REQUEST['valute']=="EUR") {
		setcookie("valute", "EUR", time()+360000, "/");
	}
	if ($_REQUEST['valute']=="RUB") {
		setcookie("valute", "RUB", time()+360000, "/");
	}
	?>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WLD7Z6F');</script>
	<!-- End Google Tag Manager -->



<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79303504-1', 'auto');

  function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)" 
    ));

    return matches ? decodeURIComponent(matches[1]) : "";
 }

  ga('set', 'dimension1', getCookie("_ga"));
  ga('require', 'displayfeatures'); 
  ga('send', 'pageview');

/* Accurate bounce rate by time */
if (!document.referrer ||
     document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'Новый посетитель', location.pathname);
 }, 15000);
  ga('send', 'pageview');

</script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/x-icon" href="/favicon.ico">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon16.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon32.png">
	<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/favicon57.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/favicon72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/favicon114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/favicon144.png">
	<link rel="mask-icon" href="/images/favicon/favicon.svg" color="#252324">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<title itemprop="headline"><?$APPLICATION->ShowTitle()?></title>	<?$APPLICATION->ShowMeta("keywords")?>
	<?$APPLICATION->ShowMeta("description")?>
	<?$APPLICATION->ShowCSS();?>
	<?$APPLICATION->ShowHeadStrings();?>
	<?$APPLICATION->ShowHeadScripts();?>
    <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@200;400&display=swap" rel="stylesheet">
    <link href="/css/bootstrap.min.css?v=<?= time() ?>" rel="stylesheet">
    <link href="/css/ion.rangeSlider.css?v=<?= time() ?>" rel="stylesheet">
	<link href="/css/jquery.fancybox.css?v=<?= time() ?>" rel="stylesheet">
    <link href="/css/ion.rangeSlider.skinFlat.css?v=<?= time() ?>" rel="stylesheet">
    <link href="/css/lightslider.min.css?v=<?= time() ?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/css/multiselect.css?v=<?= time() ?>" rel="stylesheet">
    <link href="/css/custom-scroll.css?v=<?= time() ?>" rel="stylesheet">
    <link href="/css/style.css?v=<?= time() ?>" rel="stylesheet">
    <link href="/css/mobile.css?v=<?= time() ?>" rel="stylesheet">
    
	<?$APPLICATION->ShowProperty("canonical")?>
	<?$APPLICATION->SetPageProperty("canonical",'<link rel="canonical" href="http://'.$_SERVER["HTTP_HOST"].$APPLICATION->GetCurPage().'" />')?>
    <meta property="og:url" content="<?="http://".$_SERVER['HTTP_HOST'].$APPLICATION->GetCurPage();?>" />
    <meta property="og:title" content="<? $APPLICATION->ShowTitle();?>" />
    <meta property="og:description" content="<? echo $APPLICATION->ShowProperty("description");?>" />
	<meta itemprop="description" name="description" content="<?=$APPLICATION->ShowProperty("description");?>">
	<meta itemprop="keywords" name="keywords" content="<?=$APPLICATION->ShowProperty("description");?>">
    <script src="/js/jquery.min.js"></script>
	<script type="text/javascript">
	    (function(_,r,e,t,a,i,l){_['retailCRMObject']=a;_[a]=_[a]||function(){(_[a].q=_[a].q||[]).push(arguments)};_[a].l=1*new Date();l=r.getElementsByTagName(e)[0];i=r.createElement(e);i.async=!0;i.src=t;l.parentNode.insertBefore(i,l)})(window,document,'script','https://collector.retailcrm.pro/w.js','_rc');

    	_rc('create', 'RC-10299369080-2');

	    _rc('send', 'pageView');
	</script>
<meta name="yandex-verification" content="8b1612e5ecfe5640" />
<meta name="google-site-verification" content="nxX_Jq6FgOiNuS28YUTjZBomQd_fEoCQ95l2BqEjYRo" />


</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WLD7Z6F"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>

    <div class="megabasket">
    <? $APPLICATION->IncludeComponent("komilfo:sale.basket.basket.line", "float_basket", Array(
        "PATH_TO_BASKET" => "/personal/basket.php",	// Страница корзины
        "PATH_TO_PERSONAL" => "/personal/",	// Персональный раздел
        "SHOW_PERSONAL_LINK" => "Y",	// Отображать ссылку на персональный раздел
        ),
        false
    );?>
    </div>
    <div class="pusher">
        <div class="close-area"></div>
        <div class="mobile-menu">
            <?$APPLICATION->IncludeComponent("komilfo:menu","top_mobile",Array(
                    "ROOT_MENU_TYPE" => "top",
                    "MAX_LEVEL" => "3",
                    "CHILD_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "Y",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            );?>
        </div>
        <div class="content">
                <div class="header-wrapper">
                	<div class="container flex-end">
                		<a class="phone_head" href="tel:+78003336794">8 (800) 333-67-94</a>
                		<a class="btn_call_back open-callbackheader-popup" href="javascript:void(0);">Звонок от стилиста</a>
                	</div>
	    <div class="container">
			<!-- <div class="pre-head">
				<div class="pre-head__link">
					<a href="/kontakty/" class="pr">Бутики в Петербурге: Садовая 28</a>
					<a href="/kontakty/">Малая Посадская, 2</a>
				</div>
				<div class="pre-head__title">
					Доставка заказов по РФ
				</div>
				<div class="enter-and-reg">
					<a class="" href="/kontakty/">Контакты</a>
					<span>|</span>
					<? if (!$USER->IsAuthorized()){?>
					<a class="open-enter-popup" href="">Вход</a>
					<span>|</span>
					<a class="open-reg-popup" href="">Регистрация</a>
					<? } else { ?>
					<a class="" href="/personal/profile/">Личный кабинет</a>
					<span>|</span>
					<a class="open-exit-popup" href="/?logout=Y">Выйти</a>
					<?}?>
				</div>
			</div> -->
	        <div class="head">
	            <div class="activate-pusher"><div class="activate-pusher-line"></div></div>
				<div class="logo">
	                <a href="/" ><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-logo.svg' ?></a>
	                <!-- <a href="/about">resale boutique</a> -->
	            </div>
	            <nav>
	                <div class="search-hidder">
	                    <?$APPLICATION->IncludeComponent("komilfo:menu","top",Array(
                            "ROOT_MENU_TYPE" => "top",
                            "MAX_LEVEL" => "3",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        	)
                    	);?>
	                </div>
	            </nav>
	            <? 
	            // var_dump(urldecode($APPLICATION->get_cookie("multi_city")));
            	$city_selected = false;
	            if('' != $APPLICATION->get_cookie("multi_city")){
	            	$city = urldecode($APPLICATION->get_cookie("multi_city"));
	            	$city_selected = true;
	            }else{
		            $city = getCity()['city'];
		            $APPLICATION->set_cookie("multi_city", urlencode($city), time()+60*60*24*30*12*2, "/");
	            }
	            ?>
	            <div class="multi-city" style="<?=$USER->IsAdmin() ? '' : ''?>">
	            	<div class="multi-city-title"  style="<?=$city_selected==true?'':'display:none';?>">
	            		<span><?=$city != false? $city : 'Москва';?></span><?=$city_selected==true?'':'?';?>
	            	</div>
	            	<div class="multi-city-icon">
	            		<svg class="multi-city-icon" xmlns="http://www.w3.org/2000/svg" width="14.898" height="20.165" viewBox="0 0 14.898 20.165">
						  <g id="pin" transform="translate(-48.08)">
						    <g id="Group_2" data-name="Group 2" transform="translate(48.08)">
						      <g id="Group_1" data-name="Group 1">
						        <path id="Path_1" data-name="Path 1" d="M55.529,0A7.457,7.457,0,0,0,48.08,7.449a8.594,8.594,0,0,0,.632,2.868,12.013,12.013,0,0,0,.861,1.61l5.107,7.739a.971.971,0,0,0,1.7,0l5.108-7.739a11.963,11.963,0,0,0,.861-1.61,8.592,8.592,0,0,0,.631-2.868A7.457,7.457,0,0,0,55.529,0Zm6.01,9.977a11.008,11.008,0,0,1-.785,1.468l-5.108,7.739c-.1.153-.133.153-.234,0L50.3,11.445a11.012,11.012,0,0,1-.785-1.468,7.8,7.8,0,0,1-.564-2.527,6.573,6.573,0,1,1,13.146,0A7.808,7.808,0,0,1,61.539,9.977Z" transform="translate(-48.08)"/>
						        <path id="Path_2" data-name="Path 2" d="M116.024,64.008a3.944,3.944,0,1,0,3.944,3.944A3.948,3.948,0,0,0,116.024,64.008Zm0,7.011a3.067,3.067,0,1,1,3.067-3.067A3.071,3.071,0,0,1,116.024,71.019Z" transform="translate(-108.575 -60.502)"/>
						      </g>
						    </g>
						  </g>
						</svg>
	            	</div>
	            	
	            	<div class="multi-city-dropdown">
	            		<div class='multi-city-dropdown__triangle'>
	            			<div class="diff-arrow"></div>
	            		</div>
	            		<div class="multi-city-dropdown__container">
	            			<div class='multi-city-dropdown__your-city'>
	            				Ваш город: <span><?=$city != false? $city : 'Москва';?></span><?=$city_selected==true?'':'?';?>
	            			</div>
	            			<div class="multi-city-dropdown__controls">
	            				<button class="multi-city-dropdown__accept" style="<?=$city_selected==true?'display:none':'';?>">Да</button>
	            				<button class="multi-city-dropdown__change">Выбрать другой</button>
	            			</div>
	            		</div>
	            	</div>
	            </div>
	            <div class="header-info">
	                <!-- div class="enter-and-reg">
                       <a class="" href="/kontakty/">Контакты</a>
	                    <span>|</span>
                        <? if (!$USER->IsAuthorized()){?>
	                    <a class="open-enter-popup" href="">Вход</a>
	                    <span>|</span>
	                    <a class="open-reg-popup" href="">Регистрация</a>
                        <? } else { ?>
                        <a class="" href="/personal/profile/">Личный кабинет</a>
	                    <span>|</span>
	                    <a class="open-exit-popup" href="/?logout=Y">Выйти</a>
                        <?}?>
	                </div -->
                    <!--<div class="second-menu">
						<select class="choose-currency col-2">
							<option value="RUB" <?=$_COOKIE['valute']=="RUB"?"selected":"";?>>RUB</option>
							<option value="USD" <?=$_COOKIE['valute']=="USD"?"selected":"";?>>USD</option>
							<option value="EUR" <?=$_COOKIE['valute']=="EUR"?"selected":"";?>>EUR</option>
						</select>
						<a href="/handthings/check/" class="lighgreyclass topmenu-btn">Выплаты</a>
                    </div> -->
					<div class="search">
	                    <div class="search-icon"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-search.svg' ?></div>
	                    <input type="text" placeholder="поиск">
                        <div class="search_results-container">
                            <div class="search_results"></div>
                        </div>
	                </div>
	                <div class="phone-and-basket">
						<div class="lk">
							<? if (!$USER->IsAuthorized()){?>
							<a class="open-enter-popup" href="">Вход</a>
							<span>|</span>
							<a class="open-reg-popup" href="">Регистрация</a>
							<? } else { ?>
							<a class="" href="/personal/profile/">Личный кабинет</a>
							<span>|</span>
							<a class="open-exit-popup" href="/?logout=Y">Выйти</a>
							<?}?>
						</div>
	                    <!-- <a href="tel:+78003336794">8 (800) 333-67-94</a> -->
	                    <div class="basket">
	                    <? $APPLICATION->IncludeComponent("komilfo:sale.basket.basket.line", "top_cart_wc", Array(
                            "PATH_TO_BASKET" => "/personal/basket.php",	// Страница корзины
                            "PATH_TO_PERSONAL" => "/personal/",	// Персональный раздел
                            "SHOW_PERSONAL_LINK" => "Y",	// Отображать ссылку на персональный раздел
                            ),
                            false
                        );?>
	                    </div>
						<a href="/handthings/" class="topmenu-btn">Сдать вещи</a>
	                </div>
	                <!-- end phone-and-basket -->
	            </div>

	            <!-- end header-info -->
	        </div>
			<div class="mobile-navigation">
				<a href="/zhenskoe/">Женское</a>
				<a href="/muzhskoe/">Мужское</a>
				<a href="/brands/">Бренды</a>
				<a href="/handthings/">Сдать вещи</a>
			</div>
		</div>
	</div>
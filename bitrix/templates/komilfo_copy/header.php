<!-- parts/head.html -->
<!DOCTYPE html>
<html lang="ru">
<head>
	<?
    global $USER;
	if($_GET['logout']=="Y")
		$USER->Logout();
	if ($_REQUEST['valute']=="USD") {
		setcookie("valute", "USD", time()+360000, "/");
	}
	if ($_REQUEST['valute']=="EUR") {
		setcookie("valute", "EUR", time()+360000, "/");
	}
	if ($_REQUEST['valute']=="RUB") {
		setcookie("valute", "RUB", time()+360000, "/");
	}
	?>
	<!-- VK Pixel Code -->
	<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-239419-9gaxY';</script>
	<!-- End VK Pixel Code -->
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '165056337426784');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	 src="https://www.facebook.com/tr?id=165056337426784&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1576896619025897');
	fbq('track', 'PageView');
	</script>
	<script>
	function getRetailCrmCookie(name) {
		var matches = document.cookie.match(new RegExp(
			'(?:^|; )' + name + '=([^;]*)'
		));
		return matches ? decodeURIComponent(matches[1]) : '';
	}
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1576896619025897&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

    <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WLD7Z6F');</script>
	<!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/x-icon" href="/favicon.ico">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon16.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon32.png">
	<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/favicon57.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/favicon72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/favicon114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/favicon144.png">
	<link rel="mask-icon" href="/images/favicon/favicon.svg" color="#252324">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<title><?$APPLICATION->ShowTitle()?></title>	<?$APPLICATION->ShowMeta("keywords")?>
	<?$APPLICATION->ShowMeta("description")?>
	<?$APPLICATION->ShowCSS();?>
	<?$APPLICATION->ShowHeadStrings();?>
	<?$APPLICATION->ShowHeadScripts();?>
    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/ion.rangeSlider.css" rel="stylesheet">
	<link href="/css/jquery.fancybox.css" rel="stylesheet">
    <link href="/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="/css/lightslider.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/css/multiselect.css" rel="stylesheet">
    <link href="/css/custom-scroll.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/mobile.css" rel="stylesheet">
    
	<?$APPLICATION->ShowProperty("canonical")?>
	<?$APPLICATION->SetPageProperty("canonical",'<link rel="canonical" href="http://'.$_SERVER["HTTP_HOST"].$APPLICATION->GetCurPage().'" />')?>
    <meta property="og:url" content="<?="http://".$_SERVER['HTTP_HOST'].$APPLICATION->GetCurPage();?>" />
    <meta property="og:title" content="<? $APPLICATION->ShowTitle();?>" />
    <meta property="og:description" content="<? echo $APPLICATION->ShowProperty("description");?>" />
    <script src="/js/jquery.min.js"></script>
	<script type="text/javascript">
	    (function(_,r,e,t,a,i,l){_['retailCRMObject']=a;_[a]=_[a]||function(){(_[a].q=_[a].q||[]).push(arguments)};_[a].l=1*new Date();l=r.getElementsByTagName(e)[0];i=r.createElement(e);i.async=!0;i.src=t;l.parentNode.insertBefore(i,l)})(window,document,'script','https://collector.retailcrm.pro/w.js','_rc');

    	_rc('create', 'RC-10299369080-2');

	    _rc('send', 'pageView');
	</script>
</head>
<style>
	
</style>
<body>
	<!-- Rating Mail.ru counter -->
	<script type="text/javascript">
	var _tmr = window._tmr || (window._tmr = []);
	_tmr.push({id: "3169407", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID"});
	(function (d, w, id) {
	  if (d.getElementById(id)) return;
	  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
	  ts.src = "https://top-fwz1.mail.ru/js/code.js";
	  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
	  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
	})(document, window, "topmailru-code");
	</script><noscript><div>
	<img src="https://top-fwz1.mail.ru/counter?id=3169407;js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" />
	</div></noscript>
	<!-- //Rating Mail.ru counter -->

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WLD7Z6F"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
	
    <div class="megabasket">
    <? $APPLICATION->IncludeComponent("komilfo:sale.basket.basket.line", "float_basket", Array(
        "PATH_TO_BASKET" => "/personal/basket.php",	// Страница корзины
        "PATH_TO_PERSONAL" => "/personal/",	// Персональный раздел
        "SHOW_PERSONAL_LINK" => "Y",	// Отображать ссылку на персональный раздел
        ),
        false
    );?>
    </div>
    <div class="pusher">
        <div class="close-area"></div>
        <div class="mobile-menu">
            <?$APPLICATION->IncludeComponent("komilfo:menu","top_mobile",Array(
                    "ROOT_MENU_TYPE" => "top",
                    "MAX_LEVEL" => "3",
                    "CHILD_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "Y",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            );?>
        </div>
        <div class="content">
            <div class="footer-liner">
                <div class="header-wrapper">
	    <div class="container">
			<div class="pre-head">
				<div class="pre-head__link">
					<a href="/kontakty/" class="pr">Бутики в Петербурге: Садовая 28</a>
					<a href="/kontakty/">Малая Посадская, 2</a>
				</div>
				<div class="pre-head__title">
					Доставка заказов по РФ
				</div>
				<div class="enter-and-reg">
					<a class="" href="/kontakty/">Контакты</a>
					<span>|</span>
					<? if (!$USER->IsAuthorized()){?>
					<a class="open-enter-popup" href="">Вход</a>
					<span>|</span>
					<a class="open-reg-popup" href="">Регистрация</a>
					<? } else { ?>
					<a class="" href="/personal/profile/">Личный кабинет</a>
					<span>|</span>
					<a class="open-exit-popup" href="/?logout=Y">Выйти</a>
					<?}?>
				</div>
			</div>
			
	        <div class="head">
				
	            <div class="activate-pusher"><div class="activate-pusher-line"></div></div>
	            <nav>
	                <div class="search-hidder">
	                    <?$APPLICATION->IncludeComponent("komilfo:menu","top",Array(
                            "ROOT_MENU_TYPE" => "top",
                            "MAX_LEVEL" => "3",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        	)
                    	);?>
	                </div>
	                <div class="search">
	                    <div class="search-icon"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-search.svg' ?></div>
	                    <input type="text" placeholder="поиск">
                        <div class="search_results-container">
                            <div class="search_results"></div>
                        </div>
	                </div>
	            </nav>
	            <div class="logo">
	                <a href="/" ><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-logo.svg' ?></a>
	                <a href="/about">resale boutique</a>
	            </div>
	            <div class="header-info">
	                <!-- <div class="enter-and-reg">
                       <a class="" href="/kontakty/">Контакты</a>
	                    <span>|</span>
                        <? if (!$USER->IsAuthorized()){?>
	                    <a class="open-enter-popup" href="">Вход</a>
	                    <span>|</span>
	                    <a class="open-reg-popup" href="">Регистрация</a>
                        <? } else { ?>
                        <a class="" href="/personal/profile/">Личный кабинет</a>
	                    <span>|</span>
	                    <a class="open-exit-popup" href="/?logout=Y">Выйти</a>
                        <?}?>
	                </div> -->
                    <div class="second-menu">
						<select class="choose-currency col-2">
							<option value="RUB" <?=$_COOKIE['valute']=="RUB"?"selected":"";?>>RUB</option>
							<option value="USD" <?=$_COOKIE['valute']=="USD"?"selected":"";?>>USD</option>
							<option value="EUR" <?=$_COOKIE['valute']=="EUR"?"selected":"";?>>EUR</option>
						</select>
                        <a href="/handthings/" class="topmenu-btn">Сдать вещи</a>
						<a href="/handthings/check/" class="lighgreyclass topmenu-btn">Выплаты</a>
                    </div>
	                <div class="phone-and-basket">
	                    <a href="tel:+78003336794">8 (800) 333-67-94</a>
	                    <div class="basket">
	                    <? $APPLICATION->IncludeComponent("komilfo:sale.basket.basket.line", "top_cart_wc", Array(
                            "PATH_TO_BASKET" => "/personal/basket.php",	// Страница корзины
                            "PATH_TO_PERSONAL" => "/personal/",	// Персональный раздел
                            "SHOW_PERSONAL_LINK" => "Y",	// Отображать ссылку на персональный раздел
                            ),
                            false
                        );?>
	                    </div>
	                </div>
	                <!-- end phone-and-basket -->
	            </div>
	            <!-- end header-info -->
	        </div>
	    </div>
	</div>
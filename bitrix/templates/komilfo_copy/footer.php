<!-- parts/footer.html --> 
</div>
<div class="prefooter">
    <div class="container">
        <div class="prefooter-container">
            <?$APPLICATION->IncludeComponent("komilfo:menu","links",Array(
                    "ROOT_MENU_TYPE" => "prefooter", 
                    "MAX_LEVEL" => "1", 
                    "CHILD_MENU_TYPE" => "prefooter", 
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "N", 
                    "MENU_CACHE_TIME" => "3600", 
                    "MENU_CACHE_USE_GROUPS" => "Y", 
                    "MENU_CACHE_GET_VARS" => "" 
                )
            );?>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="header">Комильфо-бутик</p>
                        <?$APPLICATION->IncludeComponent("komilfo:menu","links",Array(
                                "ROOT_MENU_TYPE" => "footer1", 
                                "MAX_LEVEL" => "1", 
                                "CHILD_MENU_TYPE" => "footer1", 
                                "USE_EXT" => "Y",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "Y",
                                "MENU_CACHE_TYPE" => "N", 
                                "MENU_CACHE_TIME" => "3600", 
                                "MENU_CACHE_USE_GROUPS" => "Y", 
                                "MENU_CACHE_GET_VARS" => "" 
                            )
                        );?>
                        <div class="payment">
                            <? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-visa.svg' ?>
                            <? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-mastercard.svg' ?>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <p class="header">Покупателям</p>
                        <?$APPLICATION->IncludeComponent("komilfo:menu","links",Array(
                                "ROOT_MENU_TYPE" => "footer2", 
                                "MAX_LEVEL" => "1", 
                                "CHILD_MENU_TYPE" => "footer2", 
                                "USE_EXT" => "Y",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "Y",
                                "MENU_CACHE_TYPE" => "N", 
                                "MENU_CACHE_TIME" => "3600", 
                                "MENU_CACHE_USE_GROUPS" => "Y", 
                                "MENU_CACHE_GET_VARS" => "" 
                            )
                        );?>
                        <a href="/howtospotfake">Проверка на оригинальность</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 text-center">
                <p class="header">Контакты</p>
                <div class="socials">
                    <a target="_blank" href="https://vk.com/butikkomilfo"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-vk.svg' ?></a>
                    <a target="_blank" href="https://facebook.com/komilfobutik/"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-fb.svg' ?></a>
                    <a target="_blank" href="https://www.instagram.com/komilfo_resale/"><? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-inst.svg' ?></a>
                </div>
                <a href="tel:+78124253794">+7 (812) 425-37-94</a>
                <a href="mailto:info@komilfo-butik.com">info@komilfo-butik.com</a>
                <p>Санкт-Петербург</p>
                <p>ул. Садовая, 28</p>
                <p>м. Гостиный двор</p>
            </div>
        </div>
        <div class="row top-margin">
            <div class="col-sm-4">
                <p>© 2013-<?=date("Y");?> Комильфо-бутик</p>
                <a class="dotted inline right-margin" href="/sitemap/">Карта сайта</a>
                <a class="dotted inline" href="/privacy_policy/">Политика конфиденциальности</a>
            </div>
            <div class="col-sm-4 text-center">
                <a href="http://arkvision.pro" target="_blank">
                    <span>Разработано с умом</span>
                    <? include $_SERVER['DOCUMENT_ROOT'].'/images/komilfo/icon-ark.svg' ?>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/popups.php" //Указываем путь к файлу
  )
);?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/lightslider.min.js"></script>
<script src="/js/masonry.pkgd.min.js"></script>
<script src="/js/jquery.fancybox.js"></script>
<script src="/js/ion.rangeSlider.min.js"></script>
<script src="/js/multiselect.js"></script>
<script src="/js/custom-scroll.js"></script>
<script src="/js/quicksearch.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/scripts-back.js"></script>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-165284-bX8yO';</script>
 <!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '67972';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-125549-eg1tT';</script>
</body>
</html>
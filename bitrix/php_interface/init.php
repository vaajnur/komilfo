<?
//проверяем курсы валют и обновляем
$xml_file = $_SERVER['DOCUMENT_ROOT']."/bitrix/cache/currency_rates.xml";

if (filemtime($xml_file)+3600*6 < time() OR !file_exists($xml_file)) { // Проверяем обновление каждые 6 часов

   if ($xml_data) {
    file_put_contents($xml_file, $xml_data);
    $P = new CDataXML();
    $xml = simplexml_load_string($xml_data, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $data = json_decode($json,TRUE);
    $data_values = array();
    $data_nominal = array();
    print_r($data);
    foreach ($data['Valute'] as $valute) {
        echo $valute;
        $data_values[$valute['CharCode']] = str_replace(",", ".", $valute['Value']);
        $data_nominal[$valute['CharCode']] = $valute['Nominal'];
    }
    if (CModule::IncludeModule("currency") AND date("N") != 6 AND date("N") != 7 AND $data) { // Не обновляем курс валют по выходным
        $currencies = array();
        $rsCurrency = CCurrency::GetList($by, $order);
        $base_currency = CCurrency::GetBaseCurrency();
        while ($arCurrency = $rsCurrency->GetNext()) {
            if ($arCurrency['CURRENCY'] != $base_currency) {
                $arCurrency['TIMESTAMP'] = MakeTimeStamp($arCurrency['DATE_UPDATE'], "YYYY-MM-DD HH:MI:SS");
                $r = CCurrencyRates::Add(array(
                'CURRENCY' => $arCurrency['CURRENCY'],
                'DATE_RATE' => $data['@attributes']['Date'],
                'RATE_CNT' => $data_nominal[$arCurrency['CURRENCY']],
                'RATE' => $data_values[$arCurrency['CURRENCY']],
                ));
            }
        }
    }
   }
}
// файл /bitrix/php_interface/init.php
AddEventHandler("catalog", "OnSuccessCatalogImport1C", "Add1CAgent");

function Add1CAgent($arg1, $arg2 = false){
    CAgent::AddAgent("OnSuccessCatalogImport1C();",'','N',300);
}
function OnSuccessCatalogImport1C($arg1, $arg2 = false){
    AddMessage2Log('1с прошел');
	CModule::IncludeModule("main");
	CModule::IncludeModule("catalog");
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("sale");

	$arFields = Array(
	  "ACTIVE" => "Y"
	  );
	$rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST,'ELEMENT_SUBSECTIONS' => 'Y'), true, array());
	while ($arSection = $rsSection->GetNext()) {
	  $activeElements=$arSection['ELEMENT_CNT'];
		if($activeElements>0){
			$bs = new CIBlockSection;
			$res = $bs->Update($arSection['ID'], $arFields);
			echo $arSection['NAME']."-activated<br>";
		}
	}
} 
AddEventHandler("main", "OnEpilog", "My404PageInSiteStyle");
function My404PageInSiteStyle()
{
    if(defined('ERROR_404') && ERROR_404 == 'Y')
    {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/header.php") ; 
		include($_SERVER["DOCUMENT_ROOT"]."/404.php") ; 
		include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/footer.php") ; 
    }
}
// поле описание переписываем в поле сортировка
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", Array("SectionAddUpdater", "OnBeforeIBlockSectionUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockSectionAdd", Array("SectionAddUpdater", "OnBeforeIBlockSectionUpdateHandler"));
class SectionAddUpdater
{
    // создаем обработчик события "OnBeforeIBlockSectionUpdate"
    function OnBeforeIBlockSectionUpdateHandler(&$arFields)
    {
        if(is_numeric($arFields["DESCRIPTION"])&&$arFields['IBLOCK_ID']==CATALOG_IBLOCK_ID_CONST)
        {
			$rsSection = CIBlockSection::GetList(array(), array('ID' => $arFields['ID'], 'ELEMENT_SUBSECTIONS' => 'Y'), true, array());
			if ($arSection = $rsSection->GetNext()) {
			  $activeElements=$arSection['ELEMENT_CNT'];
			}
			if($activeElements>0) $arFields['ACTIVE']="Y";
           $arFields["SORT"]=$arFields["DESCRIPTION"];
			unset($arFields["DESCRIPTION"]);
        }
    }
}
//добавляем свои поля в письмо после заказа
AddEventHandler("main", "OnBeforeEventAdd", array("MailPost", "OnBeforeEventAddHandler"));
class MailPost
{
    function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
    if ($event=="SALE_NEW_ORDER") {
      $order = CSaleOrder::GetByID($arFields['ORDER_ID']);
    
     if(CModule::IncludeModule('sale')) {

		 if ($arOrder = CSaleOrder::GetByID($arFields['ORDER_ID'])) {

		  }
		  if ($arOrderPropsValue = CSaleOrderPropsValue::GetOrderProps($arFields['ORDER_ID'])) {
			  while ($orderProp = $arOrderPropsValue->Fetch()) {
				$orderProp["ORDER_PROPS_ID"] = intval($orderProp["ORDER_PROPS_ID"]);
				switch ($orderProp["ORDER_PROPS_ID"]) {
				  case 1:
					$arFields["FIO"] = $orderProp["VALUE"];
					break;
				  case 3:
					$arFields["PHONE"] = $orderProp["VALUE"];
					break;
				  case 5:
					$arLocs = CSaleLocation::GetByID($orderProp["VALUE"], LANGUAGE_ID);
					$arFields["LOCATION"] = $arLocs["COUNTRY_NAME"].' / '.$arLocs["REGION_NAME"].' / '.$arLocs["CITY_NAME_ORIG"];
					break;
				  case 7:
					$arFields["ADDRESS"] = $orderProp["VALUE"];
					break;
				}

			}

		  }	

       }
    }
        
    }
}
// регистрируем обработчик
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("MyClass", "OnAfterIBlockElementUpdateHandler"));

class MyClass
{
    // создаем обработчик события "OnAfterIBlockElementUpdate"
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        if($arFields['IBLOCK_ID']==12){
            //AddMessage2Log("+инфоблок".$arFields['PROPERTY_VALUES']["110"]['680303']['VALUE']);
            if($arFields['PROPERTY_VALUES']['111'][0]["VALUE"]=='1417'){
                //письмо в обработке
                //отправляем письмо
                //AddMessage2Log("письмо в обработке");
                $arEventFields = array( 
                    "EMAIL" => $arFields['PROPERTY_VALUES']["110"]['680303']['VALUE']
                ); 
                if (CEvent::Send("HANDTHINGS_MAIL", "s1", $arEventFields,"Y",81)): 
                   echo "ok<br>"; 
                endif; 
            }
            if($arFields['PROPERTY_VALUES']['111'][0]["VALUE"]=='1418'){
                //письмо в продаже
                //AddMessage2Log("письмо в продаже");
                $arEventFields = array( 
                    "EMAIL" => $arFields['PROPERTY_VALUES']["110"]['680303']['VALUE']
                ); 
                if (CEvent::Send("HANDTHINGS_MAIL", "s1", $arEventFields,"Y",83)): 
                   echo "ok<br>"; 
                endif; 
            }
            if($arFields['PROPERTY_VALUES']['111'][0]["VALUE"]=='1419'){
                //письмо продан
                //AddMessage2Log("письмо продан");
                $arEventFields = array( 
                    "EMAIL" => $arFields['PROPERTY_VALUES']["110"]['680303']['VALUE']
                ); 
                if (CEvent::Send("HANDTHINGS_MAIL", "s1", $arEventFields,"Y",85)): 
                   echo "ok<br>"; 
                endif; 
            }
            if($arFields['PROPERTY_VALUES']['111'][0]["VALUE"]=='1420'){
                //письмо возврат
               // AddMessage2Log("письмо в ожидании клиента");
                $arEventFields = array( 
                    "EMAIL" => $arFields['PROPERTY_VALUES']["110"]['680303']['VALUE']
                ); 
                if (CEvent::Send("HANDTHINGS_MAIL", "s1", $arEventFields,"Y",84)): 
                   echo "ok<br>"; 
                endif; 
            }
            if($arFields['PROPERTY_VALUES']['111'][0]["VALUE"]=='1421'){
                //письмо в ожидании клиента
                 //AddMessage2Log("письмо в ожидании клиента");
                $arEventFields = array( 
                    "EMAIL" => $arFields['PROPERTY_VALUES']["110"]['680303']['VALUE']
                );  
                if (CEvent::Send("HANDTHINGS_MAIL", "s1", $arEventFields,"Y",82)): 
                   echo "ok<br>"; 
                endif; 
            }
        }
    }
}

function pr($arr){
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

function pr2($arr){
    echo "<!--<pre>";
    print_r($arr);
    echo "</pre>-->";
}

function getIPYou(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;    
}

function getCity(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://ip-api.com/json/'.getIPYou() . '?lang=ru');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $city = curl_exec($ch);
    $city = json_decode($city, true);
    curl_close($ch);
    return $city;
}


function getStoreCities(){
    \Bitrix\Main\Loader::includeModule(
     'catalog'
    );
    $dbResultList = CCatalogStore::GetList(array(), $filter, false, false, array('ID', 'UF_CITY'));
    while ($arResult = $dbResultList->Fetch()){
        $arStoreCities[] = $arResult['UF_CITY'];
    }
    $arStoreCities = array_unique(array_filter($arStoreCities));
    return $arStoreCities;
}


function getStoreAmountAll(){
    \Bitrix\Main\Loader::includeModule(
     'catalog'
    );

    $obCache = new CPHPCache();
    // $cacheLifetime = 86400*7; 
    $cacheLifetime = 6000;  // 100 минут
    $cacheID = 'AllItemsIDs'; 
    $cachePath = '/'.$cacheID;
    if( $obCache->InitCache($cacheLifetime, $cacheID, $cachePath) )
    {
       $vars = $obCache->GetVars();
       $store_am = $vars['arAllItemsIDs'];
       return $store_am;
    }
    elseif( $obCache->StartDataCache()  )
    {
           // $store_am = getStoreAmountAll();
        // }

        $dbResultList = CCatalogStore::GetList(array(), $filter, false, false, array('ID'));
        while ($arResult = $dbResultList->Fetch()){
            $arStoreIDS[] = $arResult['ID'];
        }


        $res = ciblockelement::getlist(array(), array('IBLOCK_ID' => '20', 'ACTIVE' => 'Y'), false, false, array('ID', 'IBLOCK_ID'));
        while ($ob =  $res->GetNext()) {
            // print_r($ob);
            $ELEMS[] = $ob['ID'];
        }
        // print_r($arStoreIDS);


        $dbResult = CCatalogStore::GetList(
                   array('PRODUCT_ID'=>'ASC','ID' => 'ASC'),
                   array(
                    'ACTIVE' => 'Y',
                    'PRODUCT_ID'=>$ELEMS, 
                    'ID' => $arStoreIDS // Склад на ленина
                   ),
                   false,
                   false,
                   array("ID","TITLE","ACTIVE","PRODUCT_AMOUNT","ELEMENT_ID", "UF_*")
                   // array("*")
                );

        while ($ob = $dbResult->GetNext(true, false)) {
            // print_r($ob);
            if($ob['PRODUCT_AMOUNT'] > 0){
                // $store_am[$ob['ID']][$ob['ELEMENT_ID']] = $ob['PRODUCT_AMOUNT'];
                // $store_am[$ob['ID']][] = $ob['ELEMENT_ID'];
                if($ob['UF_CITY'] != '')
                    $store_am[strtolower($ob['UF_CITY'])][] = $ob['ELEMENT_ID'];
                else
                    $store_am[$ob['ID']][] = $ob['ELEMENT_ID'];

            }
        }
        $obCache->EndDataCache(array('arAllItemsIDs' => $store_am));
    }


    return $store_am;

}
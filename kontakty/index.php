<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container static-page">
	<div class="row">
		<div class="col-xs-12">
			<table cellpadding="0" cellspacing="0" style="color:#2b587a;">
			<tbody>
			<tr>
				<td>
 <b>Бутик на Садовой:</b> г. Санкт-Петербург, м. Гостиный двор, ул. Садовая д. 28, Угол Садовой улицы и улицы Ломоносова.
				</td>
			</tr>
			<tr>
				<td>
 <b>Бутик на Малой Посадской:</b> г. Санкт-Петербург, м. Горьковское, ул. Малая Посадская д.2.
				</td>
			</tr>
			<tr>
				<td>
					 &nbsp;
				</td>
			</tr>
			</tbody>
			</table>
			<div>
				 &nbsp;
			</div>
			<div>
				<table class="contacts-table" cellpadding="0" cellspacing="0">
				<tbody>
				<tr>
					<td valign="top">
 <b>Позвоните нам:</b>
					</td>
					<td valign="top">
 <b>Напишите нам:</b>
					</td>
				</tr>
				<tr>
					<td valign="top">
 <span class="autogrow-textarea comagic_phone3">8 (800) 333-67-94</span>
					</td>
					<td valign="top">
 <a href="mailto:info@komilfo-butik.com">info@komilfo-butik.com</a>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<p>
							График работы бутика:
						</p>
					</td>
					<td valign="top">
						<p>
							Ежедневно, с 11:00 до 21:00;
						</p>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<p>
							График приема вещей:
						</p>
					</td>
					<td valign="top">
						<p>
							С&nbsp;понедельника по пятницу, с 14:00 до 19:00;
						</p>
					</td>
				</tr>
				</tbody>
				</table>
				<p>
					<img src="/images/komilfo/SPzC1vpGTjQ.jpg" alt="" style="max-width: 500px;">
				</p>
				<p>
					&nbsp;
				</p>
				<table align="JUSTIFY" cellpadding="0" cellspacing="0" style="margin-left: 0.19cm; margin-right: 0.16cm; text-indent: 0.64cm; margin-top: 0.42cm; margin-bottom: 0cm; font-size:10px;">
				<tbody>
				<tr>
					<td>
 <span style="font-size:11px;"><span style="font-family:times new roman,times,serif;">ИП Федотовский Андрей Владимирович ИНН 471506571856 ОГРНИП 308471521000046</span></span>
					</td>
				</tr>
				<tr>
					<td>
 <span style="font-size:11px;"><span style="font-family:times new roman,times,serif;">Филиал "Санкт-Петербургский" ОАО "АЛЬФА-БАНК" р/с 40802810032030000307 БИК 044030786 к/счет 30101810600000000786</span></span>
					</td>
				</tr>
				</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="yandex-map">
						 <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa24701967aecb748f0527994efbe4e0a8a7bf454ce46af9f554cd742607179a6&amp;width=100%&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="yandex-map">
						<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7dfa7e2ffa83577f556ac5464a6ccdf457b71b09382506fc36cd46d091886ba3&amp;width=100%25&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Проверка на оригинальность");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container">
    <div class="row">
        <div class="col-md-12 how-to-spot">
            <h2>Проверяем оригинальность сумок</h2>
            <p>С помощью специализированного устройства от Entrupy.</p>
        </div>
    </div>
    <div class="row bags-container">
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-2.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-3.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-4.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-5.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-6.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-7.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-8.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-9.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-10.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-11.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-12.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-13.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-14.png" alt="">
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <img src="/images/komilfo/image-15.png" alt="">
        </div>
    </div>
    <div class="row how-to-spot">
        <div class="col-md-12">
            <h2 class="orig">Заполните заявку на проверку вашей сумки
                <br>
                на оригинальность и мы свяжемся с вами
            </h2>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
            <input type="text" placeholder="Егор" class="your_name">
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
            <input type="text" placeholder="57567567567" class="your_phone">
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
            <input type="email" placeholder="some@mail.com" class="your_email">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <button class="btn-orange btn btn-fake">Отправить заявку</button>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12">
            <h3>Стоимость: 3000 руб.</h3>
            <div class="how-to-spot-hermes">Стоимость проверки сумок HERMES: 5000 руб.</div>
            <span>По итогу проверки выдается сертификат</span>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="/images/komilfo/sert.png" alt="">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
        <p class="adress-there">
            <b>Наш адрес:</b> г.Санкт-Петербург, м. Гостиный двор, ул. Садовая, 28-30, к1.
            <br>Угол Садовой улицы и улицы Ломоносова.
        </p>   
        </div>
    </div>
</div>
<div class="container static-page">
	<div class="row">
		<div class="col-xs-12">
         <table border="0" cellpadding="0" cellspacing="0" style="color:#2b587a;">
            <tbody>
                <tr>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </tbody>
        </table>
        <div>
            &nbsp;</div>
        <div>
            <table class="contacts-table" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td valign="top">
                            <b>Позвоните нам:</b></td>
                        <td valign="top">
                            <b>Напишите нам:</b></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span class="autogrow-textarea comagic_phone3">8 (800) 333-67-94</span></td>
                        <td valign="top">
                            info@komilfo-butik.com</td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <p>График работы бутика:</p>
                        </td>
                        <td valign="top">
                            <p>Ежедневно, с 11:00 до 21:00;</p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <p>График приема вещей:</p>
                        </td>
                        <td valign="top">
                            <p>С понедельника по пятницу, с 14:00 до 19:00;</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="yandex-map">
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa24701967aecb748f0527994efbe4e0a8a7bf454ce46af9f554cd742607179a6&amp;width=100%&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
		</div>

		</div>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
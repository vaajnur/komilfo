<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
//если 1, значит галка доп фотки и проверять емейл еще нужно
$errors=0;
if($_REQUEST['check']=="1"){
    if(strlen($_REQUEST['email'])==0||check_email($_REQUEST['email'])==false){
        $errors++;
        echo "email-error";
    }
        
}
if(strlen($_REQUEST['name'])==0){
    echo "name-error";
    $errors++;
}

if(strlen($_REQUEST['phone'])==0){
    echo "phone-error";
    $errors++;
}
    
if(strlen($_REQUEST['adres'])==0){
    $errors++;
    echo "adres-error";
}
//все ок, добавляем в инфоблок и отправляем письмо
if($errors==0){
    echo "okidoki";
    CModule::IncludeModule("catalog");
    CModule::IncludeModule("iblock");
    $name_product="";
    $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_CML2_MANUFACTURER");
    $arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST, "ID"=>$_REQUEST['product']);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
    while($ob = $res->GetNextElement())
    {
     $arFields = $ob->GetFields();
     $name_product="<a href='".$arFields['DETAIL_PAGE_URL']."'>".explode(" ",$arFields['NAME'])[0]." ".$arFields['PROPERTY_CML2_MANUFACTURER_VALUE']."</a>";
    }
    if($_REQUEST['place']=='home'){
      $_REQUEST['place'] = 'дома';
    }
    if($_REQUEST['place']=='butik'){
      $_REQUEST['place'] = 'в бутике';
    }

    if($_REQUEST['check']==1)
        $check="Да";
    else
        $check="Нет";
    $val=555;
    if($_REQUEST['check']==1)
        $val=593;
    //Добавляем элемент в инфоблок
    $el = new CIBlockElement;
    $PROP = array();
    $PROP[76] = $_REQUEST['name'];
    $PROP[77] = $_REQUEST['phone'];
    $PROP[78] = $_REQUEST['adres'];
    $PROP[79] = $_REQUEST['email'];
    $PROP[80] = $_REQUEST['product'];
    $PROP[264] = $_REQUEST['place'];
    $PROP[81] = Array("VALUE" => $val );
    $arLoadProductArray = Array(
      "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
      "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
      "IBLOCK_ID"      => 8,
      "PROPERTY_VALUES"=> $PROP,
      "NAME"           => "Заказ примерки от ".$_REQUEST['name'],
      "ACTIVE"         => "Y"
      );

    if($PRODUCT_ID = $el->Add($arLoadProductArray))
      echo "New ID: ".$PRODUCT_ID;
    else
      echo "Error: ".$el->LAST_ERROR;
    //отправляем письмо, ставим метку, что sended=1
     $arEventFields = array( 
        "EMAIL" => ''.$_REQUEST['email'], 
        "CHECK" => ''.$check,
        "PHONE" => ''.$_REQUEST['phone'],
        "NAME" => ''.$_REQUEST['name'],
        "ADDRESS" => ''.$_REQUEST['adres'],
         "PLACE" => ''.$_REQUEST['place'],
        "PRODUCT" => ''.$name_product,
    ); 
    if (CModule::IncludeModule("main")): 
       if (CEvent::Send("ORDER_FITTING", "s1", $arEventFields,"Y",80)): 
          echo "ok<br>"; 
       endif; 
    endif;
}
?>
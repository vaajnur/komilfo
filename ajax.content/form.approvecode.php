<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
$USER=new Cuser();
CModule::IncludeModule("iblock");
if(!isset($_REQUEST['CODE'])){
	require_once 'sms.ru.php';

	$smsru = new SMSRU('C582EF37-E861-6040-9090-746115D40300'); // Ваш уникальный программный ключ, который можно получить на главной странице
	$_SESSION['CODE']=rand(1000,9999);
	$_SESSION['PHONE']=str_replace("+7","8",$_REQUEST['PHONE']);
	$data = new stdClass();
	$data->to = $_REQUEST['PHONE'];
	$data->from = 'Komilfo';
	$data->text = 'Код подтверждения: '.$_SESSION['CODE']; // Текст сообщения
	$sms = $smsru->send_one($data); // Отправка сообщения и возврат данных в переменную

	if ($sms->status == "OK") { // Запрос выполнен успешно
		echo "success";
		echo "Сообщение отправлено успешно. ";
		echo "ID сообщения: $sms->sms_id.";
	} else {
		echo "error";
		echo "Сообщение не отправлено. ";
		echo "Код ошибки: $sms->status_code. ";
		echo "Текст ошибки: $sms->status_text.";
	}
}
else{
	if($_REQUEST["CODE"]==$_SESSION['CODE']){
		$fields=array(
        	"UF_APPROVE_PHONE"=>1
        );
        $USER->Update($USER->GetID(), $fields);
	}
	else{
		$fields=array(
        	"UF_APPROVE_PHONE"=>0
        );
        $USER->Update($USER->GetID(), $fields);
		echo "error";
	}
}
?>
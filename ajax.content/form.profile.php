<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
$USER=new Cuser();
CModule::IncludeModule("iblock");
?>
<?
function isUserPassword($userId, $password)
{
    $userData = CUser::GetByID($userId)->Fetch();

    $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));

    $realPassword = substr($userData['PASSWORD'], -32);
    $password = md5($salt.$password);

    return ($password == $realPassword);
}
if(isset($_REQUEST['NAME'])&&$_REQUEST['NAME']!="password"){
	echo htmlspecialcharsbx($_REQUEST['NAME']);
	echo htmlspecialcharsbx($_REQUEST['VALUE']);
    if($_REQUEST['NAME']=="PERSONAL_BIRTHDAY"){
        $_REQUEST['VALUE']=$_REQUEST['VALUE'].".".date("Y");
    }
	if($USER->IsAuthorized()){
		if($_REQUEST['NAME']=="UF_SIZE_SHOES"||$_REQUEST['NAME']=="UF_SIZE"||$_REQUEST['NAME']=="UF_PANTS_SIZE"){
			CModule::IncludeModule("subscribe");
			// Вывод рубрик можно производить таким способом
			$arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"); 
			$arFilter = Array("ACTIVE"=>"Y");
			$rsRubric = CRubric::GetList($arOrder, $arFilter); 
			$arRubrics = array(); 
			$rubric_id=0;
			while($arRubric = $rsRubric->GetNext()) 
			{
				if(strlen($_REQUEST['VALUE'])>0){
					if($_REQUEST['VALUE']==$arRubric['DESCRIPTION']){
						if($_REQUEST['NAME']=="UF_SIZE_SHOES"&&$arRubric['NAME']=="Размер обуви - ".$_REQUEST['VALUE'])
							$rubric_id=$arRubric['ID'];
						if($_REQUEST['NAME']=="UF_PANTS_SIZE"&&$arRubric['NAME']=="Размер брюк - ".$_REQUEST['VALUE'])
							$rubric_id=$arRubric['ID'];
						if($_REQUEST['NAME']=="UF_SIZE"&&$arRubric['NAME']=="Размер одежды - ".$_REQUEST['VALUE'])
							$rubric_id=$arRubric['ID'];
					}
				}
			}
			if($rubric_id==0){
				$rubric = new CRubric;
				if(strlen($_REQUEST['VALUE'])>0){
					$DESCRIPTION=$_REQUEST['VALUE'];
				}
				$name=$APPLICATION->GetTitle(false);
				if($_REQUEST['NAME']=="UF_SIZE_SHOES")
					$name="Размер обуви - ".$DESCRIPTION;
				if($_REQUEST['NAME']=="UF_PANTS_SIZE")
					$name="Размер брюк - ".$DESCRIPTION;
				if($_REQUEST['NAME']=="UF_SIZE")
					$name="Размер одежды - ".$DESCRIPTION;
				$arFields = Array(
					"ACTIVE" => "Y",
					"NAME" => $name,
					"DESCRIPTION" => $DESCRIPTION,
					"LID" => "s1"
				);
				$rubric_id = $rubric->Add($arFields);
				$subscription = CSubscription::GetByEmail($USER->GetEmail());
				if($arSub = $subscription->Fetch()){
					$aSubscrRub = CSubscription::GetRubricArray($arSub['ID']);
					if(!in_array($rubric_id,$aSubscrRub)){
						array_push($aSubscrRub,$rubric_id);
						$arFields_sub = Array(
							"RUB_ID" => $aSubscrRub
						);
						$subscr = new CSubscription;
						if($subscr->Update($arSub['ID'],$arFields_sub,"s1"))
						{ $submited=1;  }
					}
					
				}
			}
			$subscription = CSubscription::GetByEmail($USER->GetEmail());
			if($arSub = $subscription->Fetch()){
				require_once('MCAPI.class.php');
				$api_key = '7b66957fcf1fb0de20e652cd508ded6a-us6';
				$listid = '2dde958d7c';
				define('MC_API_KEY', $api_key);
				define('MC_LIST_ID', $listid);
				$api = new MCAPI(MC_API_KEY);
				$listID = MC_LIST_ID;
				if($_REQUEST['NAME']=="UF_SIZE_SHOES")
					$name="MMERGE2";
				if($_REQUEST['NAME']=="UF_PANTS_SIZE")
					$name="MMERGE5";
				if($_REQUEST['NAME']=="UF_SIZE")
					$name="MMERGE4";
				$vars=$api->listUpdateMember($listID, $USER->GetEmail(), array($name=>$_REQUEST['VALUE']));
			}
		}
        $fields=array(
        $_REQUEST['NAME']=>$_REQUEST['VALUE']
        );
        $USER->Update($USER->GetID(), $fields);
	}
}
if(isset($_REQUEST['NAME'])&&$_REQUEST['NAME']=="password"){
	if(isset($_REQUEST['VALUE'])){
		echo isUserPassword($USER->GetID(), $_REQUEST['VALUE']);
	}
	if(isset($_REQUEST['VALUE1'])&&isset($_REQUEST['VALUE2'])){
		if(strlen($_REQUEST['VALUE1'])>0&&strlen($_REQUEST['VALUE1'])>0){
			if($_REQUEST['VALUE1']==$_REQUEST['VALUE2']){
				echo "1";
				$fields=array(
					"PASSWORD"          => $_REQUEST['VALUE1'],
					"CONFIRM_PASSWORD"  => $_REQUEST['VALUE2'],
				);
				$USER->Update($USER->GetID(), $fields);
				echo $USER->LAST_ERROR;
				//тут меняем пароль на новый
			}
		}
	}
}
if(isset($_REQUEST['ADRES'])){
	CModule::IncludeModule("sale");
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	if(isset($_REQUEST['CITY'])){
		$db_vars = CSaleLocation::GetList(
			array(
					"SORT" => "ASC",
					"COUNTRY_NAME_LANG" => "ASC",
					
				),
			array("LID" => LANGUAGE_ID,"CITY_NAME" => $_REQUEST['CITY']),
			false,
			false,
			array()
		);
	   while ($vars = $db_vars->Fetch()):
		$cityid=$vars["ID"];
	   endwhile;
	}
   if($_REQUEST['DELETE']=="Y"){
	   $arUser['UF_ADRESSES'][$_REQUEST['ADRES']]="";
   }
   else if($_REQUEST['ADD']=="Y"){
	   array_push($arUser['UF_ADRESSES'],$cityid.";".$_REQUEST['STREET'].";".$_REQUEST['HOUSE'].";".$_REQUEST['FLAT'].";".$_REQUEST['ADRESNAME']);
   }
   else{
		if(strlen($cityid)>0){
			$arUser['UF_ADRESSES'][$_REQUEST['ADRES']]=$cityid.";".$_REQUEST['STREET'].";".$_REQUEST['HOUSE'].";".$_REQUEST['FLAT'].";".$_REQUEST['ADRESNAME'];
		}
	}
	$fields=array(
		'UF_ADRESSES'=>$arUser['UF_ADRESSES']
	);
	print_r($arUser['UF_ADRESSES']);
	$USER->Update($USER->GetID(), $fields);
} 
function getExtension($str)
{
     $i = strrpos($str,".");
     if (!$i) { return ""; }
     $l = strlen($str) - $i;
     $ext = substr($str,$i+1,$l);
     return $ext;
}
if(isset($_FILES['avatar'])){
  $valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
  //get the extension of the file in a lower case format
  $filename = stripslashes($_FILES['avatar']['name']);
   $ext = getExtension($filename);
   $ext = strtolower($ext);
  if(in_array($ext,$valid_formats))
  {
	  move_uploaded_file($_FILES["avatar"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["avatar"]["name"]);
	  $arFile = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["avatar"]["name"]);
      $arFile['del'] = "Y";           
      $arFile["MODULE_ID"] = "main";
      $fields['PERSONAL_PHOTO'] = $arFile;
		echo '/upload/tmp/'. $_FILES["avatar"]["name"];
		$USER->Update($USER->GetID(), $fields);
	}
}
?>
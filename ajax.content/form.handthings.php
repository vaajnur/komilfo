<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
$errors=0;
CModule::IncludeModule("iblock");
function getExtension($str)
{
     $i = strrpos($str,".");
     if (!$i) { return ""; }
     $l = strlen($str) - $i;
     $ext = substr($str,$i+1,$l);
     return $ext;
}
if(strlen($_REQUEST['name'])==0){
    echo "error-name";
    $errors++;
}
if(strlen($_REQUEST['email'])==0){
    echo "error-email";
    $errors++;
}
if(!check_email($_REQUEST['email'])){
    echo "error-email";
    $errors++;
}
if(strlen($_REQUEST['phone'])==0){
    echo "error-phone";
    $errors++;
}
foreach($_REQUEST['description'] as $key=>$item){
    if(strlen($item)==0){
        echo "error-description".$key;
        $errors++;
    }
}
foreach($_REQUEST['size'] as $key=>$item){
    if(strlen($item)==0){
        echo "error-size".$key;
        $errors++;
    }
}
foreach($_REQUEST['price'] as $key=>$item){
    if(strlen($item)==0){
        echo "error-price".$key;
        $errors++;
    }
}
foreach($_FILES['img1']['name'] as $key=>$item){
    if(strlen($item)==0){
        echo "error-img1-".$key;
        $errors++;
    }
}
foreach($_FILES['img2']['name'] as $key=>$item){
    if(strlen($item)==0){
        echo "error-img2-".$key;
        $errors++;
    }
}
if($errors==0){
  // file_put_contents(__DIR__.'/filename.log', var_export($_REQUEST, true) . PHP_EOL . var_export($_FILES, true));
    //добавляем в инфоблок и все ок
	$success=0;
    for($i=0;$i<count($_REQUEST['description']);$i++){
        //Добавляем элемент в инфоблок
        $el = new CIBlockElement;
        $PROP = array();
        $PROP[108] = $_REQUEST['name'];
        $PROP[109] = $_REQUEST['phone'];
        $PROP[110] = $_REQUEST['email'];
        $PROP[107] = $_REQUEST['price'][$i];
        $PROP[106] = $_REQUEST['size'][$i];
        $PROP[105] = $_REQUEST['description'][$i];
        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
        //get the extension of the file in a lower case format
        if(strlen($_FILES['img1']['name'][$i])>0){
            $filename = stripslashes($_FILES['img1']['name'][$i]);
             $ext = getExtension($filename);
             $ext = strtolower($ext);
            if(in_array($ext,$valid_formats))
            {
                move_uploaded_file($_FILES["img1"]["tmp_name"][$i], $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["img1"]["name"][$i]);
                $arFile = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["img1"]["name"][$i]);
                // $arFile['del'] = "Y";           
                $arFile["MODULE_ID"] = "main";

            }
        }
        if(strlen($_FILES['img2']['name'][$i])>0){
            $filename = stripslashes($_FILES['img2']['name'][$i]);
            $ext = getExtension($filename);
            $ext = strtolower($ext);
            if(in_array($ext,$valid_formats))
            {
                move_uploaded_file($_FILES["img2"]["tmp_name"][$i], $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["img2"]["name"][$i]);
                $arFile2 = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["img2"]["name"][$i]);
                // $arFile2['del'] = "Y";           
                $arFile2["MODULE_ID"] = "main";

            }
        }
        $PROP['104']=array($arFile,$arFile2);
        $arLoadProductArray = Array(
          "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
          "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
          "IBLOCK_ID"      => 12,
          "PROPERTY_VALUES"=> $PROP,
          "NAME"           => "Сдать вещи ".$_REQUEST['name'],
          "ACTIVE"         => "Y"
          );
        if($PRODUCT_ID = $el->Add($arLoadProductArray)){
			$success=1;
			echo "success".$PRODUCT_ID;
		}
          
        else
          echo "Error: ".$el->LAST_ERROR;
    }
	if($success==1){
		//отправка письма админу
		 $arEventFields = array( 
			"EMAIL" => "info@komilfo-butik.com", 
			 "NAME"=>$_REQUEST['name'],
			 "CLIENT_PHONE"=>$_REQUEST['phone'],
			 "CLIENT_EMAIL"=>$_REQUEST['email'],
		); 
		if (CModule::IncludeModule("main")): 
		   if (CEvent::Send("HANDTHINGS_MAIL", "s1", $arEventFields,"Y",95)): 
			  echo ""; 
		   endif; 
		endif;
	}
   /* if(isset($_FILES['avatar'])){
      $valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
      //get the extension of the file in a lower case format
      $filename = stripslashes($_FILES['avatar']['name']);
       $ext = getExtension($filename);
       $ext = strtolower($ext);
      if(in_array($ext,$valid_formats))
      {
          move_uploaded_file($_FILES["avatar"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["avatar"]["name"]);
          $arFile = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["avatar"]["name"]);
          $arFile['del'] = "Y";           
          $arFile["MODULE_ID"] = "main";

      }
    }*/
}


?>
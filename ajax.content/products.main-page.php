<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
$GLOBALS['FILTER_MAIN_CATALOG']=array(">CATALOG_PRICE_2"=>0,"!CML2_MANUFACTURER"=>false,">DETAIL_PICTURE"=>0);
$sort="created";
$asc="desc";
if($_REQUEST['id']=="discounts")
{
	$GLOBALS['FILTER_MAIN_CATALOG']["!PROPERTY_TSENA_V_OFITS_BUTIKE_TOVAR"]=false;
    $sort="property_DISCOUNT_VALUE";
    $asc="desc";
}
if($_REQUEST['id']=="novinki")
{
    $sort="created";
    $asc="desc";
}
if($_REQUEST['id']=="top")
{
    ?>
        <?$APPLICATION->IncludeComponent("komilfo:sale.bestsellers","leaders_id",
        Array(
            "LINE_ELEMENT_COUNT" => "8",
            "TEMPLATE_THEME" => "blue",
            "BY" => "AMOUNT",
            "PERIOD" => "0",
            "FILTER" => array("N", "P", "F"),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "86400",
            "AJAX_MODE" => "N",
            "DETAIL_URL" => "",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "DISPLAY_COMPARE" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "PRICE_CODE" => array("Интернет - магазин"),
            "SHOW_PRICE_COUNT" => "1",
            "PRODUCT_SUBSCRIPTION" => "N",
            "PRICE_VAT_INCLUDE" => "Y",
            "USE_PRODUCT_QUANTITY" => "N",
            "SHOW_NAME" => "Y",
            "SHOW_IMAGE" => "Y",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "PAGE_ELEMENT_COUNT" => "30",
            "SHOW_PRODUCTS_3" => "Y",
            "PROPERTY_CODE_3" => array("MANUFACTURER", "MATERIAL"),
            "CART_PROPERTIES_3" => array("CORNER"),
            "ADDITIONAL_PICT_PROP_3" => "MORE_PHOTO",
            "LABEL_PROP_3" => "SPECIALOFFER",
            "PROPERTY_CODE_4" => array("COLOR"),
            "CART_PROPERTIES_4" => array(),
            "OFFER_TREE_PROPS_4" => array("-"),
            "HIDE_NOT_AVAILABLE" => "N",
            "CONVERT_CURRENCY" => "Y",
            "CURRENCY_ID" => "RUB",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
        )
    );
    $GLOBALS['FILTER_MAIN_CATALOG']['ID']=$GLOBALS['BESTSELLERS_IDS'];
    ?>
    <?
    
}
?>

<?
$APPLICATION->IncludeComponent(
    "komilfo:catalog.section",
    "main_page",
    Array(
        "TEMPLATE_THEME" => "blue",
        "PRODUCT_DISPLAY_MODE" => "N",
        "OFFER_ADD_PICT_PROP" => "FILE",
        "OFFER_TREE_PROPS" => array("-"),
        "PRODUCT_SUBSCRIPTION" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_CLOSE_POPUP" => "Y",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "SEF_MODE" => "N",
        "IBLOCK_TYPE" => "books",
        "IBLOCK_ID" => CATALOG_IBLOCK_ID_CONST,
        "SECTION_ID" => $_REQUEST["SECTION_ID"],
        "SECTION_CODE" => "",
        "SECTION_USER_FIELDS" => array(),
        "ELEMENT_SORT_FIELD" => $sort,
        "ELEMENT_SORT_ORDER" => $asc,
        "ELEMENT_SORT_FIELD2" => "name",
        "ELEMENT_SORT_ORDER2" => "asc",
        "FILTER_NAME" => "FILTER_MAIN_CATALOG",
        "INCLUDE_SUBSECTIONS" => "Y",
        "SHOW_ALL_WO_SECTION" => "Y",
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "ADD_SECTIONS_CHAIN" => "N",
        "DISPLAY_COMPARE" => "N",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "BROWSER_TITLE" => "-",
        "SET_META_KEYWORDS" => "N",
        "META_KEYWORDS" => "",
        "SET_META_DESCRIPTION" => "N",
        "META_DESCRIPTION" => "",
        "SET_LAST_MODIFIED" => "Y",
        "USE_MAIN_ELEMENT_SECTION" => "Y",
        "SET_STATUS_404" => "N",
        "PAGE_ELEMENT_COUNT" => "8",
        "LINE_ELEMENT_COUNT" => "1",
        "PROPERTY_CODE" => array("RAZMER_TOVARA","TSVET","CML2_MANUFACTURER","TSENA_V_OFITS_BUTIKE","IZNOS","SOSTAV","AKTSIYA"),
        "OFFERS_FIELD_CODE" => array(),
        "OFFERS_PROPERTY_CODE" => array(),
        "OFFERS_SORT_FIELD" => "sort",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_FIELD2" => "active_from",
        "OFFERS_SORT_ORDER2" => "desc",
        "OFFERS_LIMIT" => "5",
        "BACKGROUND_IMAGE" => "-",
        "PRICE_CODE" => array(),
        "USE_PRICE_COUNT" => "Y",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(),
        "USE_PRODUCT_QUANTITY" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
       
        "HIDE_NOT_AVAILABLE" => "Y",
        "OFFERS_CART_PROPERTIES" => array(),
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "SET_STATUS_404" => "Y",
        "SHOW_404" => "Y",
        "MESSAGE_404" => "",
        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager"
    )
);?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
CModule::IncludeModule("subscribe");
function checkEmail($email)
{
		if (!preg_match ( "/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/", $email)) {
			return FALSE;
		}	
		return TRUE;		
}
$submited=0;
$ERROR='';
if($_POST['do_subscribe']=="Y")
{
	if(!checkEmail($_POST['email']))
	{
		$ERROR='Введите корректный E-mail<br>';
	}else{
		$subscription = CSubscription::GetByEmail($_POST['email']);
		$subscription->ExtractFields("str_");
		if($str_ID){ 
			$ERROR='Ваш E-mail уже находится в списке рассылок<br>';
            $aSubscrRub = CSubscription::GetRubricArray($str_ID);
            if(!in_array($_POST['rubric_id'],$aSubscrRub)){
                array_push($aSubscrRub,$_POST['rubric_id']);
                $arFields_sub = Array(
                    "RUB_ID" => $aSubscrRub
                );
                $subscr = new CSubscription;
                if($subscr->Update($str_ID,$arFields_sub,"s1"))
                { $submited=1;  }
            }
			/*if($_POST['rubric_id']==4){
				
				require_once('MCAPI.class.php');
				$api_key = '39ee144e6c1a2f942bfb5422617c6c9a-us6';
				$listid = '2dde958d7c';
				define('MC_API_KEY', $api_key);
				define('MC_LIST_ID', $listid);
				$api = new MCAPI(MC_API_KEY);
				$listID = MC_LIST_ID;
				$vars=$api->listMergeVars($listID);
				print_r($vars);
				//$api->listSubscribe($listID, $_POST['email'], array());
                $submited=1; echo 'ok';  die();
            }*/
		}
	}
	if(!$ERROR)
	{
		$arFields = Array(
			"USER_ID" => ($USER->IsAuthorized()? $USER->GetID():false),
			"FORMAT" => ($FORMAT <> "html"? "text":"html"),
			"EMAIL" => $_POST['email'],
			"CONFIRMED" => "Y",
      		"SEND_CONFIRM" => "N",
			"ACTIVE" => "Y",
			"RUB_ID" => isset($_POST['rubric_id'])?array($_POST['rubric_id']):array(1)
		);
		$subscr = new CSubscription;
		$ID = $subscr->Add($arFields);
		if($ID>0)
			CSubscription::Authorize($ID);
		else
			$strWarning .= "Ошибка при добавлении рассылки: ".$subscr->LAST_ERROR."";
		$submited=1;
	}
}
echo $ERROR;
if($_REQUEST['unsubscribe']=='Y')
{
	$res = CSubscription::Delete($str_ID);
	echo 'Рассылка удалена.';
}

if($submited){
	require_once('MCAPI.class.php');
	$api_key = '39ee144e6c1a2f942bfb5422617c6c9a-us6';
	$listid = '2dde958d7c';
	define('MC_API_KEY', $api_key);
	define('MC_LIST_ID', $listid);
	$api = new MCAPI(MC_API_KEY);
	$listID = MC_LIST_ID;
	/*if($_POST['rubric_id']==4){
		$vars=$api->listMergeVars($listID);
		print_r($vars);
		//$api->listSubscribe($listID, $_POST['email'], array());
    }
	else*/
	$api->listSubscribe($listID, $_POST['email'], array());
	echo 'ok';
}
<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
$errors=0;
if(strlen($_REQUEST['email'])==0||check_email($_REQUEST['email'])==false){
    $errors++;
    echo "email-error";
}
if(strlen($_REQUEST['name'])==0){
    echo "name-error";
    $errors++;
}

if(strlen($_REQUEST['phone'])==0){
    echo "phone-error";
    $errors++;
}
    
//все ок, добавляем в инфоблок и отправляем письмо
if($errors==0){
    echo "okidoki";
    CModule::IncludeModule("catalog");
    CModule::IncludeModule("iblock");
    //Добавляем элемент в инфоблок
    $el = new CIBlockElement;
    $PROP = array();
    $PROP[258] = $_REQUEST['name'];
    $PROP[260] = $_REQUEST['phone'];
    $PROP[259] = $_REQUEST['email'];
    $arLoadProductArray = Array(
      "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
      "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
      "IBLOCK_ID"      => 26,
      "PROPERTY_VALUES"=> $PROP,
      "NAME"           => "Проверка на оригинальность ".$_REQUEST['name'],
      "ACTIVE"         => "Y"
      );

    if($PRODUCT_ID = $el->Add($arLoadProductArray))
      echo "New ID: ".$PRODUCT_ID;
    else
      echo "Error: ".$el->LAST_ERROR;
    //отправляем письмо, ставим метку, что sended=1
     $arEventFields = array( 
        "EMAIL" => ''.$_REQUEST['email'], 
        "PHONE" => ''.$_REQUEST['phone'],
        "NAME" => ''.$_REQUEST['name'],
    ); 
    if (CModule::IncludeModule("main")): 
       if (CEvent::Send("HANDTHINGS_MAIL", "s1", $arEventFields,"Y",99)): 
          echo "ok<br>"; 
       endif; 
    endif;
}
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Связаться с нами");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<style>
    div[id^="wait_comp_"]{
        display:none;
    }
</style>
<div class="container static-page feedback">
    <p class="h2">Наше местонахождение</p>
	<div class="row">
	    <div class="col-md-6">
	        <span><b>Адрес:</b></span>
	        <p>Komilfo-butik - сеть магазинов брендовой одежды<br>м. Гостиный двор, Ул. Садовая д 28, угол ул.Ломоносова/ул.Садовая</p>
	        <span><b>Телефон:</b></span>
	        <p><a class="comagic_phone3" href="tel:+78003336794">8 (800) 333-67-94</a></p>
	        <span><b>E-mail:</b></span>
	        <p><a href="mailto:info@komilfo-butik.com">info@komilfo-butik.com</a></p>
	        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa24701967aecb748f0527994efbe4e0a8a7bf454ce46af9f554cd742607179a6&amp;width=100%&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
	    </div>
	    <div class="col-md-6">
            <div class="feedback-form">
                <?$APPLICATION->IncludeComponent(
	"komilfo:main.feedback", 
	".default", 
	array(
		"USE_CAPTCHA" => "Y",
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"EMAIL_TO" => "alihachev@arkvision.pro",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "MESSAGE",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "7",
		),
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
            </div>
	    </div>
    </div>
    <div class="row">
	    <div class="col-md-6">
	    	<br>
	        <span><b>Адрес:</b></span>
	        <p>г. Санкт-Петербург, Каменноостровский проспект д. 9/2.</p>
	        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A172fd8a20544ec84db4dbf51e08d7d400e5042255b691416a755803269bfd86f&amp;width=100%25&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
	    </div>
    </div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
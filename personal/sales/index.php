<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои продажи");?>
<? if (!$USER->IsAuthorized())
    header("Location: /");
?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
  )
);?>
<div class="lk-indent"></div>
<div class="container">
	<?
	if($USER->IsAuthorized()){
        $rsUser = CUser::GetByID($USER->GetID());
        $arUser = $rsUser->Fetch();
        //echo "<pre>"; print_r($arUser); echo "</pre>";
        ?>
        <div class="row">
            <div class="col-md-3">
                <? $APPLICATION->IncludeComponent("komilfo:menu", "left_profile", Array(
                    "ROOT_MENU_TYPE" => "left_profile",	// Тип меню для первого уровня
                    ),
                    false
                  );?>
            </div>
            <div class="col-md-9">
				<button class="enroll_payment">Записаться на выплату</button>
            <?
			if($arUser['UF_APPROVE_PHONE']==1){
				/* ЗДЕСЬ ПРОДАЖИ */
				$arUser['PERSONAL_PHONE']=str_replace("+7","8",$arUser['PERSONAL_PHONE']);
				$arFilter = array('IBLOCK_ID' => 22,"UF_PHONE_CLIENT"=>$arUser['PERSONAL_PHONE']);
				$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);?>
				<table id="lk-order_table">
					<thead>
						<tr>
							<th>Номер документа</th>
							<th>Товар</th>
							<th>Кол-во</th>
							<!--<th>Цена</th>
							<th>Сумма</th>-->
							<th>Статус</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tbody>
				<?
				$goods=array();
				$docs=false;
				while ($arSection = $rsSections->Fetch())
				{
					$docs=true;
					$arSelect = Array();
					$arFilter = Array("IBLOCK_ID"=>22, "SECTION_ID"=>$arSection['ID']);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
					$goods=array();
					$index=0;
					while($ob = $res->GetNextElement())
					{
						$arFields = $ob->GetFields();
						$arProps = $ob->GetProperties();
						$goods[$index]['name']=$arFields['NAME'];
						$goods[$index]['amount']=$arProps['amount']['VALUE'];
						$goods[$index]['amount_back']=$arProps['amount_back']['VALUE'];
						$goods[$index]['good']=$arProps['good']['VALUE'];
						$goods[$index]['price']=$arProps['price']['VALUE'];
						$goods[$index]['cost']=$arProps['cost']['VALUE'];
						$goods[$index]['measurement']=$arProps['measurement']['VALUE'];
						$index++;
					}
					?><? foreach($goods as $key=>$good){?>
							<tr id="bx_2592568946_177494">
								<? if ($key==0){?>
								<td rowspan="<?=count($goods);?>"><?=$arSection['NAME'];?></td>
								<?}?>
								<td>
								<?
								
								$arSelect = Array("ID","NAME","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER");
								$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST, "ID"=>$good['good'],'ACTIVE'=>"Y","CATALOG_AVAILABLE"=>"Y");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
								if(strlen($good['good'])>0&&$ob = $res->GetNextElement())
								{
								$arFields=$ob->GetFields();
									?>
								<a href="<?=$arFields['DETAIL_PAGE_URL'];?>"><?=explode(" ",$arFields['NAME'])[0]." ".$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];?></a>
								<?} else{?>
								<?=$good['name'];?>
								<?}?>
								
								</td>
								<td><?=$good['amount']." ".$good['measurement'];?>.</td>
								<!--<td><?=$good['price'];?></td>
								<td><?=$good['cost'];?></td>-->
								<?
								$arSelect = Array("ID","NAME","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER");
								$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST, "ID"=>$good['good'],'ACTIVE'=>"Y","CATALOG_AVAILABLE"=>"Y");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
								if(strlen($good['good'])>0&&$ob = $res->GetNextElement())
								{?>
									<? if(intval($good['amount'])-intval($good['amount_back'])<=0){
											?><td>Возврат</td><?
										}
										else if(intval($good['amount'])>intval($good['amount_back'])&&intval($good['amount_back'])>0){
											?><td>Частично продан</td><?
										}
										else{?>
											<td>В наличии</td>
										<?}?>
								<?} else{
									if(strlen($good['good'])==0){?>
									<td>Нет в продаже</td>
									<?} else{?>
										<? if(intval($good['amount'])-intval($good['amount_back'])<=0){
											?><td>Есть возврат</td><?
										}
										else if(intval($good['amount'])>intval($good['amount_back'])&&intval($good['amount_back'])>0){
											?><td>Частично продан</td><?
										}
										else{?>
											<td>Продан</td>
										<?}?>
									<?}?>
								<?}?>
							</tr>
							<?}?>

					<?}?>
					<? if($docs===false){?>
						<tr><td colspan="6">У вас еще нет продаж</td></tr>
					<?}?>
				</tbody>
			</table>
			<?
			}
			else{
				echo "Для просмотра данной страницы необходимо подтвердить свой телефон на странице «Мой профиль»";
			} ?>
            </div>
        </div>
	<?}?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
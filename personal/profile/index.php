<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>
<? if (!$USER->IsAuthorized())
    header("Location: /");
?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
  )
);?>
<div class="lk-indent"></div>
<div class="container lk-body">
	<?
	if($USER->IsAuthorized()){
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	//echo "<pre>"; print_r($arUser); echo "</pre>";
	?>
	<div class="row">
		<div class="col-md-3">
			<? $APPLICATION->IncludeComponent("komilfo:menu", "left_profile", Array(
                "ROOT_MENU_TYPE" => "left_profile",	// Тип меню для первого уровня
                ),
                false
              );?>
		</div>
		<div class="col-md-9">
			<div class="lk-double">
				<div class="left-side move_down basicdata">
					<p class="h3">Личные данные</p>
					<button class="lk-edit">
						<?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/lk-edit.svg"; ?>
					</button>
					<button style="display:none;" class="lk-edit lk-close-edit">
						<?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/approve-circular-button.svg"; ?>
					</button>
					<p class="fieald_title">Имя:</p>
					<input type="text" onkeyup="changeuserfield('NAME', 'user_name')" disabled="disabled" class="gray_field user_name" value="<?=$arUser['NAME'];?>" tabindex="1">
					<p class="fieald_title">E-mail:</p>
					<input type="text" disabled="disabled" onkeyup="changeuserfield('EMAIL', 'user_email')" class="gray_field user_email" value="<?=$arUser['EMAIL'];?>" tabindex="2">
					<p class="fieald_title">Дата рождения:</p>
					<input type="hidden" disabled="disabled" class="user_bday_hidden" value="<?=$arUser['PERSONAL_BIRTHDAY'];?>" tabindex="3">
					<input type="text" disabled="disabled" onchange="changeuserfield('PERSONAL_BIRTHDAY', 'user_bday')" class="gray_field user_bday masked-birthday" value="<?=$arUser['PERSONAL_BIRTHDAY'];?>" tabindex="3">
					<p class="fieald_title">Номер телефона:</p>
					<input type="text" disabled="disabled" onkeyup="changeuserfield('PERSONAL_PHONE', 'user_phone')" class="gray_field user_phone phone_masked_input" value="<?=$arUser['PERSONAL_PHONE'];?>" tabindex="4">
					
					<div class="approve-sms-block" <?if($arUser["UF_APPROVE_PHONE"]==1){?>style="display:none;"<?}?>>
					<button disabled="disabled" class="sms-approve-button approve-phone">Подтвердить телефон</button>
					<div class="code-sms" style="display:none;">
						<input disabled="disabled" type="text" class="gray_field code-sms-val"/>
						<button disabled="disabled" class="sms-approve-button enter-code">Ввести код</button>
					</div>
					</div>
					<div <?if($arUser["UF_APPROVE_PHONE"]!=1){?>style="display:none;"<?}?> class="approved-phone">Телефон подтвержден</div>
					<div  class="sms_box disabled <?if($arUser["UF_SMS"]) echo "checked";?>">
						<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="44.236px" height="44.236px" viewBox="0 0 44.236 44.236" style="enable-background:new 0 0 44.236 44.236;" xml:space="preserve"><g><g><path d="M22.118,44.236C9.922,44.236,0,34.314,0,22.119C0,9.923,9.922,0,22.118,0s22.118,9.922,22.118,22.119S34.314,44.236,22.118,44.236z M22.118,1.5C10.75,1.5,1.5,10.75,1.5,22.119s9.25,20.619,20.618,20.619s20.618-9.25,20.618-20.619S33.486,1.5,22.118,1.5z"/><path d="M18.674,27.842c-0.192,0-0.384-0.072-0.53-0.219l-4.333-4.327c-0.293-0.293-0.293-0.768-0.001-1.061c0.293-0.294,0.769-0.293,1.061-0.001l3.803,3.798l10.693-10.693c0.293-0.293,0.768-0.293,1.061,0s0.293,0.768,0,1.061L19.204,27.623C19.058,27.77,18.866,27.842,18.674,27.842z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
						<p class="fieald_title">Получать информацию о заказах по SMS</p>
					</div>
				</div>
				<div class="right-side move_top">
					<div class="lk-img_edit" style="background-image:url('<?=CFile::GetPath($arUser['PERSONAL_PHOTO']);?>')">
						
						<div class="lk-img_edit-overlay">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 232.3 239.9" style="enable-background:new 0 0 232.3 239.9;" xml:space="preserve"><g><path d="M130.5,0.7v105.2h101.2v26.9H130.5v106.1h-28.7V132.8H0.7v-26.9h101.2V0.7H130.5z"></path></g></svg>
							<p>Обновить фото</p>
						</div>
					</div>
					<form  style="display:none;" action="/ajax.content/form.profile.php" method="post" id="my_form" enctype="multipart/form-data">
						<input type="file" name="avatar" id="avatar"><br>
						<input type="submit" id="submit" value="Отправить">
					</form>
				</div>
			</div>
			<div class="lk-custom_double addresses">
				<div class="lk-double">
					<div class="left-side">
						<p class="h3">Адреса доставки</p>
						<button class="lk-edit">
							<?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/lk-edit.svg"; ?>
						</button>
						<button style="display:none;" class="lk-edit lk-close-edit">
							<?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/approve-circular-button.svg"; ?>
						</button>
					</div>
				</div>
				<div class="lk-double">
					<div class="left-side">
						<p class="fieald_title">Страна:</p>
						<input disabled="disabled" type="text" disabled="disabled" value="" class="gray_field country" tabindex="5">
					</div>
					<div onkeyup="changeaddress()" class="right-side">
						<p class="fieald_title">Город:</p>
						<input disabled="disabled" type="text" class="gray_field city" tabindex="6">
					</div>
				</div>
				<div class="lk-double">
					<p class="fieald_title">Улица:</p>
					<input onkeyup="changeaddress()" disabled="disabled" type="text" class="gray_field street" tabindex="7">
				</div>
				<div class="lk-double">
					<div class="left-side">
						<p class="fieald_title">Дом:</p>
						<input onkeyup="changeaddress()" disabled="disabled" type="text" class="gray_field house" tabindex="8">
					</div>
					<!--
					<div class="lk-triple_item">
						<p class="fieald_title">Корпус:</p>
						<input disabled="disabled" type="text" class="gray_field " tabindex="9">
					</div>
					-->
					<div class="right-side">
						<p class="fieald_title">Квартира / Офис:</p>
						<input onkeyup="changeaddress()" disabled="disabled" type="text" class="gray_field flat" tabindex="10">
					</div>
				</div>
				<div class="lk-double">
					<div class="left-side">
						<p class="fieald_title">Название адреса:</p>
						<input onkeyup="changeaddress()" disabled="disabled" type="text" class="gray_field adresname" tabindex="11">
					</div>
				</div>
				<div class="lk-double lk-regions disabled">
				<input type="hidden" class="selectedadres"/>
				<? 
					$index=0;
					foreach($arUser["UF_ADRESSES"] as $adres){
						$adres=explode(";",$adres);
						$arLocs = CSaleLocation::GetByID($adres[0], LANGUAGE_ID);
						$country=$arLocs['COUNTRY_NAME'];
						$city=$arLocs['CITY_NAME'];
						?>
						<div class="region-item">
							<button index="<?=$index;?>" city="<?=$city?>" country="<?=$country?>" street="<?=$adres[1]?>" house="<?=$adres[2]?>" flat="<?=$adres[3]?>" adresname="<?=$adres[4]?>" class="region"><?=$adres[4]?></button>
							<button onclick="deleteaddress(<?=$index;?>)" class="region-delete"><?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/plus.svg"; ?></button>
						</div>
						<?
						$index++;
					}
                    if(strlen($arUser["UF_ADRESSES"][0])==0){?>
                        <div class="region-item">
							<button index="<?=$index;?>" city="" country="" street="" house="" flat="" adresname="" class="region">Мой адрес</button>
							<button onclick="deleteaddress(<?=$index;?>)" class="region-delete"><?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/plus.svg"; ?></button>
						</div>
                    <?
                        $index++;
                    }
					?>		
					<button onclick="addaddress()" class="add-region">
						<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 232.3 239.9" style="enable-background:new 0 0 232.3 239.9;" xml:space="preserve"><g><path d="M130.5,0.7v105.2h101.2v26.9H130.5v106.1h-28.7V132.8H0.7v-26.9h101.2V0.7H130.5z"></path></g></svg>
					</button>
				</div>
			</div>
			<div class="lk-custom_double sizesdata">
				<div class="lk-double">
					<div class="left-side">
						<p class="h3">Мои размеры</p>
						<button class="lk-edit">
							<?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/lk-edit.svg"; ?>
						</button>
						<button style="display:none;" class="lk-edit lk-close-edit">
						<?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/approve-circular-button.svg"; ?>
						</button>
					</div>
				</div>
				<div class="lk-double lk-field_info">
					<p class="fieald_title">Укажите Ваши размеры и мы будем стараться показывать только подходящие Вам вещи. Вся указанная информация - строго конфиденциальна.</p>
				</div>
				<div class="lk-double">
					<div class="left-side">
						<p class="fieald_title">Размер обуви:</p>
						<input type="text" onkeyup="delchar($(this)[0]); changeuserfield('UF_SIZE_SHOES', 'user_size_shoes')" disabled="disabled" class="gray_field user_size_shoes"  value="<?=$arUser['UF_SIZE_SHOES'];?>" tabindex="16">
					</div>
                    <div class="right-side">
						<p class="fieald_title">Размер одежды:</p>
						<input type="text" onkeyup="delchar($(this)[0]); changeuserfield('UF_SIZE', 'user_size')" disabled="disabled"  value="<?=$arUser['UF_SIZE'];?>" class="gray_field user_size" tabindex="15">
					</div>
				</div>
                <div class="lk-double">
					<div class="left-side">
						<p class="fieald_title">Размер брюк:</p>
						<input type="text" onkeyup="delchar($(this)[0]); changeuserfield('UF_PANTS_SIZE', 'user_pants_size')" disabled="disabled" class="gray_field user_pants_size"  value="<?=$arUser['UF_PANTS_SIZE'];?>" tabindex="16">
					</div>
				</div>
			</div>
			<div class="lk-double passwords">
				<div class="left-side">
					<p class="h3">Смена пароля</p>
					<button class="lk-edit">
						<?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/lk-edit.svg"; ?>
					</button>
					<button style="display:none;" class="lk-edit lk-close-edit">
						<?php include $_SERVER['DOCUMENT_ROOT']."/images/komilfo/approve-circular-button.svg"; ?>
					</button>
					<p class="fieald_title">Введите текущий пароль:</p>
					<input type="text" onkeyup="checkpass()" disabled="disabled" class="gray_field active_password" tabindex="16">
					<p class="fieald_title">Введите новый пароль:</p>
					<input type="text" onkeyup="checknewpass()" disabled="disabled" class="gray_field new_pass1" tabindex="17">
					<p class="fieald_title">Повторите новый пароль:</p>
					<input type="text" onkeyup="checknewpass()" disabled="disabled" class="gray_field new_pass2" tabindex="18">
				</div>
			</div>
		</div>
	</div>
	<?}?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отложенные товары");
?>
<? if (!$USER->IsAuthorized())
    header("Location: /");
?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
  )
);?>
<div class="lk-indent"></div>
<div class="container">
	<?
	if($USER->IsAuthorized()){
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	//echo "<pre>"; print_r($arUser); echo "</pre>";
	?>
	<div class="row">
		<div class="col-md-3">
			<? $APPLICATION->IncludeComponent("komilfo:menu", "left_profile", Array(
                "ROOT_MENU_TYPE" => "left_profile",	// Тип меню для первого уровня
                ),
                false
            );?>
		</div>
		<div class="col-md-9">
            <form class="searchline wishlistsearch" action="">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129" width="512px" height="512px"> <g> <path d="M51.6,96.7c11,0,21-3.9,28.8-10.5l35,35c0.8,0.8,1.8,1.2,2.9,1.2s2.1-0.4,2.9-1.2c1.6-1.6,1.6-4.2,0-5.8l-35-35 c6.5-7.8,10.5-17.9,10.5-28.8c0-24.9-20.2-45.1-45.1-45.1C26.8,6.5,6.5,26.8,6.5,51.6C6.5,76.5,26.8,96.7,51.6,96.7z M51.6,14.7 c20.4,0,36.9,16.6,36.9,36.9C88.5,72,72,88.5,51.6,88.5c-20.4,0-36.9-16.6-36.9-36.9C14.7,31.3,31.3,14.7,51.6,14.7z" fill="#262626"></path> </g></svg>
				<input value="<?=htmlspecialcharsbx($_REQUEST['q']);?>" placeholder='Поиск' type="text" name="q">
			</form>
            <div class="itemsandbutton-wishlist">
                <?
                if($_REQUEST['AJAXS']=="Y")
                    $APPLICATION->RestartBuffer();

                ?>
                <div class="cssload-container">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i>
                    </div>
                </div>
                <div class="container_for_fixing">
                    <div class="tiny-items">
                        <?
                        if($_REQUEST['AJAX']=="Y")
                            $APPLICATION->RestartBuffer();
                            if($_REQUEST['DELETEWISH']=="Y"){
                                $arSelect = Array("ID", "NAME", "CREATED_BY");
                                $arFilter = Array("IBLOCK_ID"=>11,"PROPERTY_GOOD"=>$_REQUEST['ID'], "CREATED_BY"=>$USER->GetID());
                                $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
                                while($ob = $res->GetNextElement())
                                {
                                 $arFields = $ob->GetFields();
                                 CIBlockElement::Delete($arFields['ID']);
                                }
                            }
                            $APPLICATION->IncludeComponent("komilfo:catalog.section.list","categoriesarraysearch",
                                Array(
                                    "VIEW_MODE" => "LIST",
                                    "SHOW_PARENT_NAME" => "Y",
                                    "IBLOCK_TYPE" => "",
                                    "IBLOCK_ID" => CATALOG_IBLOCK_ID_CONST,
                                    "SECTION_ID" => "",
                                    "SECTION_CODE" => "",
                                    "SECTION_URL" => "",
                                    "COUNT_ELEMENTS" => "N",
                                    "TOP_DEPTH" => "3",
                                    "SECTION_FIELDS" => "",
                                    "SECTION_USER_FIELDS" => "",
                                    "ADD_SECTIONS_CHAIN" => "Y"
                                )		
                            );
                            $APPLICATION->IncludeComponent(
                                "komilfo:wish.list",
                                "",
                                array(
                                    "IBLOCK_ID" => 11,
                                    "ON_PAGE"=>20,
                                    "USER_ID" => $USER->GetID(),
                                    "CATALOG_IBLOCK_ID" => CATALOG_IBLOCK_ID_CONST
                                )
                            );
                            ?>
                            <?if($_REQUEST['AJAX']=="Y")
                                die();?>
                    </div>
                </div>
                <div class="morewishlist">
				<? if($GLOBALS['pagescount']>1){?>
				<button pagenum="1" maxpages="2" class="btn btn-fullorange loadmorewishlist">Показать все</button>
				<?}?>
				<div class="load_circle-container">
					<div class="load_circle-anim"></div>
				</div>
			</div>
            <?if($_REQUEST['AJAXS']=="Y")
			die();?>
            </div>
        </div>
	</div>
	<?}?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
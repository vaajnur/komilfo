<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Мои заказы");
$APPLICATION->SetTitle("Мои заказы");
?>
<? if (!$USER->IsAuthorized())
    header("Location: /");
?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
    "AREA_FILE_SUFFIX" => "",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
  )
);?>
<div class="lk-indent"></div>
<div class="container">
	<?
	if($USER->IsAuthorized()){
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	//echo "<pre>"; print_r($arUser); echo "</pre>";
	?>
	<div class="row">
		<div class="col-md-3">
			<? 
			$APPLICATION->IncludeComponent("komilfo:menu", "left_profile", Array(
                "ROOT_MENU_TYPE" => "left_profile",	// Тип меню для первого уровня
                ),
                false
              );?>
		</div>
		<div class="col-md-9">
            <?
            //echo $_REQUEST['PAGEN_1'];
			if (!isset($_REQUEST['PAGEN_1'])) $_REQUEST['PAGEN_1']=1; 
            $APPLICATION->IncludeComponent("komilfo:sale.personal.order.list","komilfo",Array(
					"STATUS_COLOR_N" => "green",
					"STATUS_COLOR_P" => "yellow",
					"STATUS_COLOR_F" => "gray",
					"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
					"PATH_TO_DETAIL" => "order_detail.php?ID=#ID#",
					"PATH_TO_COPY" => "basket.php",
					"PATH_TO_CANCEL" => "order_cancel.php?ID=#ID#",
					"PATH_TO_BASKET" => "basket.php",
					"PATH_TO_PAYMENT" => "payment.php",
					"ORDERS_PER_PAGE" => 10,
					"ID" => $ID,
					"SET_TITLE" => "N",
					"SAVE_IN_SESSION" => "N",
					"NAV_TEMPLATE" => "",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_GROUPS" => "Y",
					'HISTORIC_STATUSES' => array(''),
					"ACTIVE_DATE_FORMAT" => "d.m.Y"
				)
			);?>
		</div>
	</div>
	<?}?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
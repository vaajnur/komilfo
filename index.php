<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "комиссионный магазин, одежда, бутик, брендовая, элитная, бу, санкт петербург, спб, доставка, цена, стоимость, недорого, дешево");
$APPLICATION->SetPageProperty("description", "Комиссионный магазин одежды и аксессуаров в Санкт-Петербурге. Комиссионный магазин элитной одежды в СПб. Комиссионный бутик бренды.");
$APPLICATION->SetPageProperty("title", "Ресейл-бутик Комильфо");
$APPLICATION->SetTitle("Интернет-магазин \"Одежда\"");
?><div class="tiles-container index-banner">
	<div class="teaser-2">
 <a href="https://komilfo-butik.com/catalog/" class="container lightSlider-container" style="background-image: url(/upload/iblock/d14/807.jpg)"> <span class="lightSlider-wrapper"> <span class="lightSlider-title">Скидка 10%</span> <br>
 <span class="lightSlider-text">При покупке 3 вещей на весь чек</span> </span> <span class="btn btn-fullorange lightSlider-link">В каталог</span> </a>
	</div>
</div>
<div class="teaser">
	 <?$APPLICATION->IncludeComponent(
	"komilfo:main_slider",
	".default",
	Array(
		"IBLOCK_ID" => 5,
		"LINK_PROPERTY" => "LINK",
		"PHOTO_PROPERTY" => "IMAGE",
		"TEXT_PROPERTY" => "TEXT"
	)
);?>
</div>
 <?$APPLICATION->IncludeComponent(
	"komilfo:news.list",
	"banners",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"FIELD_CODE" => Array("ID"),
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"NEWS_COUNT" => "8",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => Array("DESCRIPTION","TEXT","IMAGE","SHOW_BUTTON","HEAD","LINK"),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_TITLE" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC"
	)
);?>
<div class="chat">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-7">
				<div class="chat__pic">
 <img src="images/pic-003.png" alt="">
				</div>
			</div>
			<div class="col-12 col-lg-5">
				<h2 class="chat__title">Теперь все новинки наших бутиков и… </h2>
				<p class="chat__text">
					 ничего лишнего, Вы сможете получать себе на What`s App. Заполните заявку и мы Вас пригласим в наш чат, интересный именно Вам.
				</p>
				<div class="chat__form">
					<form class="chat-form">
						<div class="chat-form__row">
 <input type="text" name="name" placeholder="ФИО">
						</div>
						<div class="chat-form__row">
 <input type="text" class="phone-inp" name="phone" placeholder="Телефон" required="">
						</div>
						<div class="chat-form__row">
 <span>Укажите<br>
							 пол:</span>
							<div class="chat-form__controls">
 <label class="chat-form__lable"> <input class="chat-form__radio" type="radio" name="sex" value="male"> <span class="chat-form__radio-icon"></span> <span class="chat-form__name">Муж</span> </label> <label class="chat-form__lable"> <input class="chat-form__radio" type="radio" name="sex" value="female"> <span class="chat-form__radio-icon"></span> <span class="chat-form__name">Жен</span> </label>
							</div>
 <input class="chat-form__input" type="text" name="size" placeholder="Укажите размер">
						</div>
						<div class="chat-form__row">
 <button class="btn btn-fullorange chat-form__submit" type="submit">Отправить</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="swap-products">
				 <? //  <a id="discounts" >АКЦИИ</a> ?> <a id="novinki" >НОВИНКИ</a>
			</div>
		</div>
		<div class="products-container loading">
			 <!-- анимация загрузки ниже, срабатывает если есть класс loading у родителя выше -->
			<div class="load-container">
				<div class="load-circle">
				</div>
				<div class="load-circle">
				</div>
			</div>
			<div class="container">
				<ul class="products products-bottomfix">
				</ul>
 <a class="btn btn-fullorange btn-square btn-fullwidth products-btn" href="/novinki/">Посмотреть все новинки</a>
			</div>
		</div>
		<div class="articles-list">
			<div class="container">
				<div class="h2">
 <a href="/blog/">Свежие статьи</a>
				</div>
				 <?$APPLICATION->IncludeComponent(
	"komilfo:news.list",
	"blog_main",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => Array("ID","DATE_CREATE"),
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"NEWS_COUNT" => "6",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => Array(),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_TITLE" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
			</div>
		</div>
		<div class="sign-up">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="bg-container">
							<p class="h1">
								 Fashion-Рассылка
							</p>
							<p>
								 Самые интересные материалы из нашего блога о новостях мира моды
							</p>
							<div class="input-group">
								 <?$APPLICATION->IncludeComponent(
	"komilfo:subscribe.form",
	"just_form",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CODE_FORM" => "MAINPAGE",
		"PAGE" => "/ajax.content/subscribe.php",
		"RUBRIC_ID" => 2,
		"SHOW_HIDDEN" => "Y",
		"USE_PERSONALIZATION" => "Y"
	)
);?>
							</div>
 <span>Ваши контакты не попадут в руки 3-х лиц: мы бережно их храним<br>
							 в соответствии с 152-ФЗ «О защите персональных данных»<br>
							 и нашей <a href="/privacy_policy/">Политикой конфиденциальности</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
 <br>
 <br>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
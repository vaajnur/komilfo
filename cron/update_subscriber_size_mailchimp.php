<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');

//раз в день выполнять
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");

require_once($_SERVER["DOCUMENT_ROOT"].'/ajax.content/MCAPI.class.php');
$api_key = '7b66957fcf1fb0de20e652cd508ded6a-us6';
$listid = '2dde958d7c';
define('MC_API_KEY', $api_key);
define('MC_LIST_ID', $listid);
$arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"); 
$arFilter = Array("ACTIVE"=>"Y");
$rsRubric = CRubric::GetList($arOrder, $arFilter); 
$arRubrics = array(); 
$rubric_id=0;
$rubrics_shoes=array();
$rubrics_shoes_ids=array();
$rubrics_pants=array();
$rubrics_pants_ids=array();
$rubrics_cloth=array();
$rubrics_cloth_ids=array();
while($arRubric = $rsRubric->GetNext()) 
{
	if(strpos($arRubric['NAME'],"Размер обуви")!==false){
		$rubrics_shoes[]=$arRubric['ID'];
		$rubrics_shoes_ids[]=$arRubric['DESCRIPTION'];
	}
	if(strpos($arRubric['NAME'],"Размер брюк")!==false){
		$rubrics_pants[]=$arRubric['ID'];
		$rubrics_pants_ids[]=$arRubric['DESCRIPTION'];
	}
	if(strpos($arRubric['NAME'],"Размер одежды")!==false){
		$rubrics_cloth[]=$arRubric['ID'];
		$rubrics_cloth_ids[]=$arRubric['DESCRIPTION'];
	}
}
foreach($rubrics_shoes as $key=>$rubric_id){
	$subscr = CSubscription::GetList(
		array("ID"=>"ASC"),
		array("RUBRIC"=>array($rubric_id),  "ACTIVE"=>"Y" )
	);
	while(($subscr_arr = $subscr->Fetch())){
		$api = new MCAPI(MC_API_KEY);
		$listID = MC_LIST_ID;
		$vars=$api->listUpdateMember($listID, $subscr_arr["EMAIL"], array("MMERGE2"=>$rubrics_shoes_ids[$key]));
	}
}
foreach($rubrics_pants as $key=>$rubric_id){
	$subscr = CSubscription::GetList(
		array("ID"=>"ASC"),
		array("RUBRIC"=>array($rubric_id),  "ACTIVE"=>"Y" )
	);
	while(($subscr_arr = $subscr->Fetch())){
		$api = new MCAPI(MC_API_KEY);
		$listID = MC_LIST_ID;
		$vars=$api->listUpdateMember($listID, $subscr_arr["EMAIL"], array("MMERGE4"=>$rubrics_pants_ids[$key]));
	}
}
foreach($rubrics_cloth as $key=>$rubric_id){
	$subscr = CSubscription::GetList(
		array("ID"=>"ASC"),
		array("RUBRIC"=>array($rubric_id),  "ACTIVE"=>"Y" )
	);
	while(($subscr_arr = $subscr->Fetch())){
		$api = new MCAPI(MC_API_KEY);
		$listID = MC_LIST_ID;
		$vars=$api->listUpdateMember($listID, $subscr_arr["EMAIL"], array("MMERGE5"=>$rubrics_cloth_ids[$key]));
	}
}
?>
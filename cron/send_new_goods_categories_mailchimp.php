<?
die();
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');

//раз в неделю выполнять
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
if (!empty($argv[1])) {
	parse_str($argv[1], $_GET);
  }
$count=10;
$count_goods=0;
/* здесь делаем из мейлчимпа массив с емейлами по категориям  $emails[SECTION_ID]=array(email1,email2)*/
// Вывод рубрик можно производить таким способом
$arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"); 
$arFilter = Array("ACTIVE"=>"Y", "LID"=>LANG); 
$rsRubric = CRubric::GetList($arOrder, $arFilter); 
$arRubrics = array(); 
$section_ids=array();
$rubric_ids=array();
while($arRubric = $rsRubric->GetNext()) 
{ 
 	if(intval($arRubric['DESCRIPTION'])>0&&strlen($arRubric['DESCRIPTION'])>0&&$arRubric['ID']!='5'&&$arRubric['ID']!='51'){
		$section_ids[]=$arRubric['DESCRIPTION'];
		$rubric_ids[]=$arRubric['ID'];
	} 
}
$emails=array();
foreach($section_ids as $key=>$section_id){
	$subscr = CSubscription::GetList(
		array("ID"=>"ASC"),
		array("RUBRIC"=>array($rubric_ids[$key]),  "ACTIVE"=>"Y" )
	);
	while(($subscr_arr = $subscr->Fetch())){  
   		$emails[$section_id][]=$subscr_arr["EMAIL"];
	}
}
foreach($section_ids as $section_id){
	$count_goods=0;
	$arSelect = Array("ID", "NAME",'DETAIL_PAGE_URL',"DETAIL_PICTURE","CATALOG_PRICE_2");
	$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST,"ACTIVE"=>"Y","!DETAIL_PICTURE"=>false,">CATALOG_PRICE_2"=>0,"SECTION_ID"=>$section_id,"!DETAIL_PICTURE"=>false,'<DATE_CREATE'  => date('d.m.Y H:i:s'),'>DATE_CREATE'  => date('d.m.Y H:i:s',strtotime("-".$_GET['days']." day")));
	$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
	$goods='<table class="row content" style="border-bottom:1px solid #d4d4d4;border-collapse:collapse;border-spacing:0;display:table;margin-bottom:20px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>';
	$index=0;
	while($ob = $res->GetNextElement())
	{
		if($index%2==0&&$index>0)
			$goods.='</tr>';
		if($index%2==0)
			$goods.='<tr style="padding:0;text-align:left;vertical-align:top">';
		$count_goods++;
		$ar_res = $ob->GetFields();
		$ar_res_props=$ob->GetProperties();
		?>
		<?
		$goods.='<th class="content-item small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:8px;text-align:left;width:274px">
    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><img  style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto" src="https://komilfo-butik.com/'.CFile::GetPath($ar_res["DETAIL_PICTURE"]).'"/>';
		$goods.='<p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:5px;padding:0;text-align:left"><a href="'.$ar_res['DETAIL_PAGE_URL'].'">'.explode(" ",$ar_res['NAME'])[0].' '.$ar_res_props['CML2_MANUFACTURER']['VALUE'].'</a></p>';
		if(strlen($ar_res_props['TSENA_V_OFITS_BUTIKE_TOVAR']["VALUE"])>0){
			$goods.='<p class="oldprice" style="Margin:0;Margin-bottom:10px;color:#c50d0d;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:5px;padding:0;text-align:left;text-decoration:line-through">'.$ar_res_props['TSENA_V_OFITS_BUTIKE_TOVAR']["VALUE"]." р.</p>";
		}
		$goods.='<p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:5px;padding:0;text-align:left">'.number_format($ar_res['CATALOG_PRICE_2'], 0, ',', ' ').' р.</p><a href="'.$ar_res['DETAIL_PAGE_URL'].'" class="purchase" style="Margin:0;background-color:#ffb400;border-bottom:1px solid transparent;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;max-width:200px;padding:10px 5px;text-align:center;text-decoration:none">Купить</a></th></tr></table></th>';
		$goods.=' ';
		$index++;
	}
	$goods.='</tr></table></th></tr>';
	$goods."</tr></tbody></table>";
	if($count_goods>0){
		foreach($emails[$section_id] as $email){
			//отправляем письмо
			$arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID_CONST, 'ID' => $section_id);
			$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter,array(),array("ID","NAME","PICTURE","DESCRIPTION","UF_*",'SECTION_PAGE_URL'));
			if ($arSection = $rsSections->Fetch())
			{
				$section_name=$arSection['NAME'];
			}
			 $arEventFields = array( 
				"EMAIL" => $email,
				"THEME"=> "Новинки из раздела ".$section_name,
				"GOODS" => $goods
			); 
			if (CEvent::Send("SEND_NEW_GOODS", "s1", $arEventFields,"Y",87)): 
			   echo "ok<br>"; 
			endif;
		}
	}
}    

?>

<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

CModule::IncludeModule("subscribe");
$arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"); 
$arFilter = Array("ACTIVE"=>"Y");
$rsRubric = CRubric::GetList($arOrder, $arFilter); 
$arRubrics = array(); 
while($arRubric = $rsRubric->GetNext()) 
{
	
	/* // обновили имена рубрик по навигационной цепочке вместо просто имен
	if(is_numeric($arRubric['DESCRIPTION'])&&strpos($arRubric['NAME'],"Размер")===false){
		$nav = CIBlockSection::GetNavChain(false, $arRubric['DESCRIPTION']);
		$new_name="";
		while ($sectionItem = $nav->Fetch()){
				$new_name.=$sectionItem['NAME']."/";
		}
		if(strlen($new_name)==0&&$arRubric['DESCRIPTION']!="120"){
			$new_name=$arRubric['NAME'];
		}
		if(strlen($new_name)>0){
			$rubric = new CRubric;
			$arFields = Array(
				"NAME" => $new_name,
				"LID" => "s1"
			);
			if($arRubric['ID']>0)
			{
				if(!$rubric->Update($arRubric['ID'], $arFields))
					echo $rubric->LAST_ERROR;
			}
			echo $arRubric['DESCRIPTION']."-".$new_name."<br>"; 
		}
		
	}*/
}
//читаем csv с емейлами
require_once ($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/csv_data.php");
$csvFile = new CCSVData('R', true);
$csvFile->LoadFile($_SERVER['DOCUMENT_ROOT'] . "/cron/clients.csv");
$csvFile->SetDelimiter(";");
while ($arRes = $csvFile->Fetch()) 
{
	print_r($arRes);
	$arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"); 
	$arFilter = Array("ACTIVE"=>"Y");
	$rsRubric = CRubric::GetList($arOrder, $arFilter); 
	$arRubrics = array(); 
	$rubric_id_shoes=0;
	$rubric_id_clothes=0;
	$rubric_id_pants=0;
	while($arRubric = $rsRubric->GetNext()) 
	{
		if($arRubric['NAME']=="Размер обуви - ".$arRes[1])
			$rubric_id_shoes=$arRubric['ID'];
		if($arRubric['NAME']=="Размер одежды - ".$arRes[2])
			$rubric_id_clothes=$arRubric['ID'];
		if($arRubric['NAME']=="Размер брюк - ".$arRes[3])
			$rubric_id_pants=$arRubric['ID'];
	}
	//добавляем рубрику если ее нет
	if($rubric_id_clothes==0){
		$rubric = new CRubric;
		$arFields = Array(
			"ACTIVE" => "Y",
			"NAME" => "Размер одежды - ".$arRes[2],
			"DESCRIPTION" => $arRes[2],
			"LID" => "s1"
		);
		$rubric_id_clothes = $rubric->Add($arFields);
	}
	if($rubric_id_pants==0){
		$rubric = new CRubric;
		$arFields = Array(
			"ACTIVE" => "Y",
			"NAME" => "Размер брюк - ".$arRes[3],
			"DESCRIPTION" => $arRes[3],
			"LID" => "s1"
		);
		$rubric_id_pants = $rubric->Add($arFields);
	}
	if($rubric_id_shoes==0){
		$rubric = new CRubric;
		$arFields = Array(
			"ACTIVE" => "Y",
			"NAME" => "Размер обуви - ".$arRes[1],
			"DESCRIPTION" => $arRes[1],
			"LID" => "s1"
		);
		$rubric_id_shoes = $rubric->Add($arFields);
	}
	$subscription = CSubscription::GetByEmail($arRes[0]);
	//обновить рубрики
	if($arSub = $subscription->Fetch()){
		$aSubscrRub = CSubscription::GetRubricArray($arSub['ID']);
		if(!in_array($rubric_id,$aSubscrRub)){
			array_push($aSubscrRub,$rubric_id_clothes);
			array_push($aSubscrRub,$rubric_id_pants);
			array_push($aSubscrRub,$rubric_id_shoes);
			if($arRes[4]=="Женщина"){
				array_push($aSubscrRub,5);
			}
			else{
				array_push($aSubscrRub,51);
			}
			$arFields_sub = Array(
				"RUB_ID" => $aSubscrRub
			);
			$subscr = new CSubscription;
			if($subscr->Update($arSub['ID'],$arFields_sub,"s1"))
			{ $submited=1; echo " - обновлен"; }
		}
	}
	//добавить подписчика
	else{
		$aSubscrRub=array();
		array_push($aSubscrRub,$rubric_id_clothes);
		array_push($aSubscrRub,$rubric_id_pants);
		array_push($aSubscrRub,$rubric_id_shoes);
		if($arRes[4]=="Женщина"){
			array_push($aSubscrRub,5);
		}
		else{
			array_push($aSubscrRub,51);
		}
		$arFields = Array(
			"USER_ID" => false,
			"FORMAT" => ($FORMAT <> "html"? "text":"html"),
			"EMAIL" => $arRes[0],
			"CONFIRMED" => "Y",
      		"SEND_CONFIRM" => "N",
			"ACTIVE" => "Y",
			"RUB_ID" => $aSubscrRub
		);
		$subscr = new CSubscription;
		$ID = $subscr->Add($arFields);
		if($ID>0)
			CSubscription::Authorize($ID);
		echo " - добавлен";
	}
	echo "<br><br>";
}
?>
<?php
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => 20, "CODE" => "CML2_MANUFACTURER"));
$counter_brands = 0;
while ($enum_fields = $property_enums->GetNext()) {
	//echo $enum_fields["ID"]." - ".$enum_fields["VALUE"]."<br>";
	$el = new CIBlockElement;
	$arParams = array("replace_space" => "-", "replace_other" => "-");
	$trans = Cutil::translit($enum_fields["VALUE"], "en", $arParams);
	$arLoadProductArray = Array(
		'IBLOCK_SECTION_ID' => false, // элемент лежит в корне раздела
		'IBLOCK_ID' => 13,
		'NAME' => $enum_fields["VALUE"],
		'CODE' => $trans,
		'ACTIVE' => "N"
	);
	$i = 0;
	$ID = 0;
	$arSelect = Array("ID", "NAME", "PROPERTY_TSENA_V_OFITS_BUTIKE");
	$arFilter = Array("IBLOCK_ID" => 20, "PROPERTY_CML2_MANUFACTURER_VALUE" => $enum_fields["VALUE"], ">DETAIL_PICTURE" => 0, "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y", ">CATALOG_PRICE_2" => 0);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelect);
	while ($ob = $res->GetNextElement()) {
		$counter_brands++;
		$arLoadProductArray['ACTIVE'] = "Y";
	}
	$arSelect = Array("ID", "NAME");
	$arFilter = Array("IBLOCK_ID" => 13, "NAME" => $enum_fields["VALUE"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFi = $ob->GetFields();
		$ID = $arFi['ID'];
		$i++;
	}
	if ($i == 0) {
		$BRANDID = $el->Add($arLoadProductArray);
	} else {
		$el->Update($ID, $arLoadProductArray);
	}
}
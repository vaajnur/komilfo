
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

$arFields = Array(
  "ACTIVE" => "Y"
  );
$rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST,'ELEMENT_SUBSECTIONS' => 'Y'), true, array());
while ($arSection = $rsSection->GetNext()) {
  $activeElements=$arSection['ELEMENT_CNT'];
	if($activeElements>0){
		$bs = new CIBlockSection;
		$res = $bs->Update($arSection['ID'], $arFields);
		echo $arSection['NAME']."-activated<br>";
	}
}
?>
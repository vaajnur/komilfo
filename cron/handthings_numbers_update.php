<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
$arSelect = Array("ID", "NAME","PROPERTY_SELL","PROPERTY_ACCEPT","PROPERTY_PAY");
$arFilter = Array("IBLOCK_ID"=>21,"ID"=>174308);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);

while($ob = $res->GetNextElement())
{
	$arFi=$ob->GetFields();
    $ID=$arFi['ID'];
	$val1=intval($arFi['PROPERTY_PAY_VALUE'])+44077;
	$val2=intval($arFi['PROPERTY_SELL_VALUE'])+12;
	$val3=intval($arFi['PROPERTY_ACCEPT_VALUE'])+17;
	CIBlockElement::SetPropertyValuesEx($ID, 21, array("PAY" => $val1));
	CIBlockElement::SetPropertyValuesEx($ID, 21, array("SELL" => $val2));
	CIBlockElement::SetPropertyValuesEx($ID, 21, array("ACCEPT" => $val3));
}

?>
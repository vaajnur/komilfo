
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

CModule::IncludeModule("subscribe");

//читаем xml

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');
function scandir_by_mtime($folder) {
	$dircontent = scandir($folder);
	$arr = array();
	foreach($dircontent as $filename) {
	  if ($filename != '.' && $filename != '..') {
		if (filemtime($folder.$filename) === false) return false;
		$dat = filemtime($folder.$filename);
		$arr[$dat] = $filename;
	  }
	}
	if (!krsort($arr)) return false;
	return $arr;
  }

  // ПРИХОДЫ

  $arr=scandir_by_mtime($_SERVER['DOCUMENT_ROOT'] ."/upload/docs/");
  print_r($arr);
  $counter=0;
  foreach($arr as $name){
	if($counter==0){
		$filename=$name;
	}
	$counter++;
  }
  echo $filename;
$xml_string=file_get_contents($_SERVER['DOCUMENT_ROOT']."/upload/docs/".$filename);
$xml = new CDataXML();
$xml->LoadString($xml_string);
$arData = $xml->GetArray();
$docs=array();
$index=0;
foreach($arData['Документы']["#"]['ДокументПоступления'] as $doc){
	$index++;
	$docs[$index]['number']=$doc['@']['Номер'];
	$docs[$index]['date']=$doc['@']['Дата'];
	$docs[$index]['sum']=$doc['@']['СуммаДокумента'];	
	$docs[$index]['client']=$doc['#']['Контрагент'][0]["@"]['Наименование'];	
	$docs[$index]['phone']=$doc['#']['Контрагент'][0]["#"]['КонтактнаяИнформация'][0]["#"]['Контакт'][0]['@']['Представление'];	
	$index_g=0;
	foreach($doc['#']['Товары'][0]['#']['Товар'] as $good){
		$docs[$index]['goods'][$index_g]['measurement']=$good['@']['ЕдиницаИзмерения'];
		$docs[$index]['goods'][$index_g]['price']=$good['@']['Цена'];
		$docs[$index]['goods'][$index_g]['cost']=$good['@']['Сумма'];
		$docs[$index]['goods'][$index_g]['amount']=$good['@']['Количество'];
		$docs[$index]['goods'][$index_g]['amount_back']=$good['@']['КоличествоВозврата'];
		$docs[$index]['goods'][$index_g]['name']=$good['@']['Наименование'];
		$docs[$index]['goods'][$index_g]['code']=$good['@']['Код'];
		$index_g++;
	}
	echo "<pre>";
	//print_r($doc);
	echo "</pre>";
}
foreach ($docs as $doc){
	$bs = new CIBlockSection;
	$arFields = Array(
	  "ACTIVE" => "Y",
	  "IBLOCK_ID" => 22,
	  "NAME" => $doc['number'],
	  "SORT" => $SORT,
	  "UF_NAME_CLIENT" => $doc['client'],
	  "UF_PHONE_CLIENT" => $doc['phone'],
	  "UF_COST_OF_GOODS" => $doc['sum']
	);
	$SECTION_ID=0;
	$arFilter = array('IBLOCK_ID' => 22,"NAME"=>$doc['number']);
	$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
	while ($arSection = $rsSections->Fetch())
	{
		$SECTION_ID=$arSection['ID'];
	}
	if($SECTION_ID > 0)
	{
	  $res = $bs->Update($SECTION_ID, $arFields);
	}
	else
	{
	  $SECTION_ID = $bs->Add($arFields);
	}
	echo "SECTION ".$SECTION_ID.":<br>";
	foreach($doc['goods'] as $good){
		$el = new CIBlockElement;
		$PROP = array();
		$PROP[242] = $good['amount'];
		$PROP[251] = $good['amount_back'];
		$PROP[243] = $good['measurement'];
		$PROP[244] = $good['price'];
		$PROP[245] = $good['cost'];
		$arLoadProductArray = Array(  
		'MODIFIED_BY' => $GLOBALS['USER']->GetID(), // элемент изменен текущим пользователем  
		'IBLOCK_SECTION_ID' => $SECTION_ID, // элемент лежит в корне раздела  
		'IBLOCK_ID' => 22, 
		'NAME' => $good['name'],  
		'ACTIVE' => 'Y'
		);
		$i = 0;
		$IDPHOTO=0;
		echo $good['name'];
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_ID"=>22, "NAME"=>$good['name'],"IBLOCK_SECTION_ID"=>$SECTION_ID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			echo "- found";
			$IDPHOTO=$arFields['ID'];  $i++;
		}
		//echo $i;
		if($i==0){
			echo "- add<br>";
			$PRODUCT_ID = $el->Add($arLoadProductArray);
			$arSelect = Array("ID", "NAME");
			$arFilter = Array("IBLOCK_ID"=>20, "%NAME"=>$good['name']);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				CIBlockElement::SetPropertyValues($PRODUCT_ID, 22,$arFields['ID'], "good");
				echo "товар найден".$arFields['ID']; 
			}
			CIBlockElement::SetPropertyValues($PRODUCT_ID, 22,$good['amount'], "amount");
			CIBlockElement::SetPropertyValues($PRODUCT_ID, 22,$good['amount_back'], "amount_back");
            CIBlockElement::SetPropertyValues($PRODUCT_ID, 22,$good['measurement'], "measurement");
            CIBlockElement::SetPropertyValues($PRODUCT_ID, 22,$good['price'], "price");
			CIBlockElement::SetPropertyValues($PRODUCT_ID, 22,$good['cost'], "cost");
			CIBlockElement::SetPropertyValues($PRODUCT_ID, 22,$good['code'], "code_1c");
		}
		else{
			echo "- update<br>";
			$arSelect = Array("ID", "NAME");
			$arFilter = Array("IBLOCK_ID"=>20, "%NAME"=>$good['name']);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				CIBlockElement::SetPropertyValues($IDPHOTO, 22,$arFields['ID'], "good");
				echo "товар найден - ".$arFields['ID'];
			} 
			$el->Update($IDPHOTO, $arLoadProductArray);
			CIBlockElement::SetPropertyValues($IDPHOTO, 22,$good['amount'], "amount");
			CIBlockElement::SetPropertyValues($IDPHOTO, 22,$good['amount_back'], "amount_back");
            CIBlockElement::SetPropertyValues($IDPHOTO, 22,$good['measurement'], "measurement");
            CIBlockElement::SetPropertyValues($IDPHOTO, 22,$good['price'], "price");
			CIBlockElement::SetPropertyValues($IDPHOTO, 22,$good['cost'], "cost");
			CIBlockElement::SetPropertyValues($IDPHOTO, 22,$good['code'], "code_1c");
		}
	}
}

//ВОЗВРАТЫ
$arr=scandir_by_mtime($_SERVER['DOCUMENT_ROOT'] ."/upload/docs_return/");
print_r($arr);
$counter=0;
foreach($arr as $name){
  if($counter==0){
	  $filename=$name;
  }
  $counter++;
}
echo $filename;
$xml_string=file_get_contents($_SERVER['DOCUMENT_ROOT']."/upload/docs_return/".$filename);
$xml = new CDataXML();
$xml->LoadString($xml_string);
$arData = $xml->GetArray();
$docs=array();
$index=0;
foreach($arData['Документы']["#"]['ДокументВозврата'] as $doc){
  $index++;
  $docs[$index]['number']=$doc['@']['Номер'];
  $docs[$index]['date']=$doc['@']['Дата'];
  $docs[$index]['sum']=$doc['@']['СуммаДокумента'];	
  $docs[$index]['client']=$doc['#']['Контрагент'][0]["@"]['Наименование'];	
  $docs[$index]['phone']=$doc['#']['Контрагент'][0]["#"]['КонтактнаяИнформация'][0]["#"]['Контакт'][0]['@']['Представление'];	
  $index_g=0;
  foreach($doc['#']['Товары'][0]['#']['Товар'] as $good){
	  $docs[$index]['goods'][$index_g]['measurement']=$good['@']['ЕдиницаИзмерения'];
	  $docs[$index]['goods'][$index_g]['price']=$good['@']['Цена'];
	  $docs[$index]['goods'][$index_g]['cost']=$good['@']['Сумма'];
	  $docs[$index]['goods'][$index_g]['amount']=$good['@']['Количество'];
	  $docs[$index]['goods'][$index_g]['amount_back']=$good['@']['КоличествоВозврата'];
	  $docs[$index]['goods'][$index_g]['name']=$good['@']['Наименование'];
	  $docs[$index]['goods'][$index_g]['code']=$good['@']['Код'];
	  $index_g++;
  }
  echo "<pre>";
  //print_r($doc);
  echo "</pre>";
}
foreach ($docs as $doc){
  $bs = new CIBlockSection;
  $arFields = Array(
	"ACTIVE" => "Y",
	"IBLOCK_ID" => 22,
	"NAME" => $doc['number'],
	"SORT" => $SORT,
	"UF_NAME_CLIENT" => $doc['client'],
	"UF_PHONE_CLIENT" => $doc['phone'],
	"UF_COST_OF_GOODS" => $doc['sum']
  );
  foreach($doc['goods'] as $good){
	  $i = 0;
	  $IDPHOTO=0;
	  echo $good['name'];
	  $arSelect = Array("ID", "NAME");
	  $arFilter = Array("IBLOCK_ID"=>22, "NAME"=>$good['name'],"PROPERTY_code_1c"=>$good['code']);
	  $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
	  while($ob = $res->GetNextElement())
	  {
		  $arFields = $ob->GetFields();
		  echo "- found";
		  $IDPHOTO=$arFields['ID'];  $i++;
	  }
	  //echo $i;
	  if($i==0){
	  }
	  else{
		  echo "- update<br>";
		  CIBlockElement::SetPropertyValues($IDPHOTO, 22,$good['amount_back'], "amount_back");
	  }
  }
}
?>

<?
/*date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');*/
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
$arSelect = Array("ID", "NAME","PROPERTY_TSENA_V_OFITS_BUTIKE_TOVAR");
//в фильтре указываем что ищем товары которые были добавлены не более часа назад
$arFilter = Array("IBLOCK_ID"=>4);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>100000), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arFields['ID'], 'CATALOG_GROUP_ID' => 2)); 
	if ($arPrice = $rsPrices->Fetch()) $price = floor($arPrice["PRICE"]);
    $arFields["PROPERTY_TSENA_V_OFITS_BUTIKE_VALUE"]=str_replace(" ","",$arFields["PROPERTY_TSENA_V_OFITS_BUTIKE_VALUE"]);
    if(strlen($arFields["PROPERTY_TSENA_V_OFITS_BUTIKE_VALUE"])==0||$price==0){
        $razn=0;
        continue;
    }
    else{
        if($arFields["PROPERTY_TSENA_V_OFITS_BUTIKE_VALUE"]>$price){
            $razn=$arFields["PROPERTY_TSENA_V_OFITS_BUTIKE_VALUE"]-$price;
            CIBlockElement::SetPropertyValuesEx($arFields['ID'], 4, array("DISCOUNT_VALUE" => "".$razn)); 
        }
    }    
}


?>
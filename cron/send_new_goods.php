<?
die();
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');

//раз в неделю выполнять
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
//активные и подтвержденные адреса, подписанные на рубрики
$subscr = CSubscription::GetList(
    array("ID"=>"ASC"),
    array("RUBRIC"=>array(2),  "ACTIVE"=>"Y" )
);
$count=6;
$arSelect = Array("ID", "NAME",'DETAIL_PAGE_URL',"DETAIL_PICTURE","CATALOG_PRICE_2");
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID_CONST,"ACTIVE"=>"Y","!DETAIL_PICTURE"=>false,">CATALOG_PRICE_2"=>0);
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
$goods='<table class="row content" style="border-bottom:1px solid #d4d4d4;border-collapse:collapse;border-spacing:0;display:table;margin-bottom:20px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>';
	$index=0;
	while($ob = $res->GetNextElement())
	{
		if($index%2==0&&$index>0)
			$goods.='</tr>';
		if($index%2==0)
			$goods.='<tr style="padding:0;text-align:left;vertical-align:top">';
		$count_goods++;
		$ar_res = $ob->GetFields();
		$ar_res_props=$ob->GetProperties();
		?>
		<?
		$goods.='<th class="content-item small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:8px;text-align:left;width:274px">
    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><img  style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto" src="https://komilfo-butik.com/'.CFile::GetPath($ar_res["DETAIL_PICTURE"]).'"/>';
		$goods.='<p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:5px;padding:0;text-align:left"><a href="'.$ar_res['DETAIL_PAGE_URL'].'">'.explode(" ",$ar_res['NAME'])[0].' '.$ar_res_props['CML2_MANUFACTURER']['VALUE'].'</a></p>';
		if(strlen($ar_res_props['TSENA_V_OFITS_BUTIKE_TOVAR']["VALUE"])>0){
			$goods.='<p class="oldprice" style="Margin:0;Margin-bottom:10px;color:#c50d0d;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:5px;padding:0;text-align:left;text-decoration:line-through">'.$ar_res_props['TSENA_V_OFITS_BUTIKE_TOVAR']["VALUE"]." р.</p>";
		}
		$goods.='<p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:5px;padding:0;text-align:left">'.number_format($ar_res['CATALOG_PRICE_2'], 0, ',', ' ').' р.</p><a href="'.$ar_res['DETAIL_PAGE_URL'].'" class="purchase" style="Margin:0;background-color:#ffb400;border-bottom:1px solid transparent;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;max-width:200px;padding:10px 5px;text-align:center;text-decoration:none">Купить</a></th></tr></table></th>';
		$goods.=' ';
		$index++;
	}
	$goods.='</tr></table></th></tr>';
	$goods."</tr></tbody></table>";
while(($subscr_arr = $subscr->Fetch())){  
   echo $subscr_arr["EMAIL"]."-";
    //отправляем письмо
     $arEventFields = array( 
		"THEME" => "Новинки в Комильфо",
        "EMAIL" => $subscr_arr["EMAIL"], 
        "GOODS" => $goods
    ); 
    if (CEvent::Send("SEND_NEW_GOODS", "s1", $arEventFields,"Y",87)): 
       echo "ok<br>"; 
    endif; 
}
    

?>
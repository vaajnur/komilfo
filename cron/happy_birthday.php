<?
/*date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');*/
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
$resUsers=CUser::GetList($by="id",$order="asc",array("ACTIVE"=>"Y","PERSONAL_BIRTHDAY_DATE"=>date("m-d")));	
while($arUsers=$resUsers->Fetch())
{
	$arEvFields=array(
		"EMAIL" => $arUsers["EMAIL"],
		"NAME" => $arUsers["NAME"],
		"LAST_NAME" => $arUsers["LAST_NAME"]
	);
	$ev = new CEvent;
	$ev->Send("SEND_BDAY_COUPON","s1",$arEvFields);
}
?>
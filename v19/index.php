<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Огромнейший ассортимент брендовой одежды. Низкие цены, высокое качество, приветливый, грамотный персонал, эксклюзивные вещи.");
$APPLICATION->SetPageProperty("title", "Комиссионный магазин брендовой одежды - купить брендовую одежду в сток аутлете по низким ценам");
$APPLICATION->SetTitle("(Копия шаблона) Интернет-магазин \"Одежда\"");
?><div class="teaser">
	<div class="container">
		<?$APPLICATION->IncludeComponent(
			"komilfo:main_slider",
			".default",
			Array(
				"IBLOCK_ID" => 5,
				"LINK_PROPERTY" => "LINK",
				"PHOTO_PROPERTY" => "IMAGE",
				"TEXT_PROPERTY" => "TEXT"
			)
		);?>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"komilfo:news.list",
	"banners",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"FIELD_CODE" => Array("ID"),
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"NEWS_COUNT" => "8",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => Array("DESCRIPTION","TEXT","IMAGE","SHOW_BUTTON","HEAD","LINK"),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_TITLE" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC"
	)
);?> <a class="open-quickview-popup" href="#">Быстрый просмотр</a>
<script>
</script>
<div class="container">
	<div class="swap-products">
		 <? //  <a id="discounts" >АКЦИИ</a> ?> <a id="novinki" >НОВИНКИ</a>
	</div>
</div>
<div class="products-container loading">
	 <!-- анимация загрузки ниже, срабатывает если есть класс loading у родителя выше -->
	<div class="load-container">
		<div class="load-circle">
		</div>
		<div class="load-circle">
		</div>
	</div>
	<div class="container">
		<ul class="products products-bottomfix">
		</ul>
 <a class="btn btn-fullorange btn-square btn-fullwidth products-btn" href="/novinki/">Посмотреть все новинки</a>
	</div>
</div>
<div class="articles-list">
	<div class="container">
		<div class="h2">
 <a href="/blog/">Свежие статьи</a>
		</div>
		 <?$APPLICATION->IncludeComponent(
	"komilfo:news.list",
	"blog_main",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => Array("ID","DATE_CREATE"),
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"NEWS_COUNT" => "6",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => Array(),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_TITLE" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
	</div>
</div>
<div class="sign-up">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="bg-container">
					<p class="h1">
						 Fashion-Рассылка
					</p>
					<p>
						 Самые интересные материалы из нашего блога о новостях мира моды
					</p>
					<div class="input-group">
						 <?$APPLICATION->IncludeComponent(
	"komilfo:subscribe.form",
	"just_form",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CODE_FORM" => "MAINPAGE",
		"PAGE" => "/ajax.content/subscribe.php",
		"RUBRIC_ID" => 2,
		"SHOW_HIDDEN" => "Y",
		"USE_PERSONALIZATION" => "Y"
	)
);?>
					</div>
 <span>Ваши контакты не попадут в руки 3-х лиц: мы бережно их храним<br>
					 в соответствии с 152-ФЗ «О защите персональных данных»<br>
					 и нашей <a href="/privacy_policy/">Политикой конфиденциальности</a></span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="container seo-text">
	<p class="h3">
		 Комиссионный бутик
	</p>
	 Одежда класса люкс - очень привлекательна. Во-первых, это высочайшее качество. Во-вторых - это шикарные дизайны именитых кутюрье. В-третьих - это показатель высокого статуса, индикатор престижа. <br>
 <br>
	 Традиционно, люди, которые могут позволить себе купить брендовые вещи в первую очередь идут в бутик брендовой одежды. В бутиках представлены последние коллекции, высокий уровень обслуживания, все можно примерить, потрогать, влюбиться. <br>
 <br>
	 Но… цены очень кусаются, правда? <br>
 <br>
	 Многие люди, листая очередной Vogue или Numero мечтают о том, чтобы обладать каким-нибудь из брендов, представленных там. Но смотря на ценники в интернет-бутиках грустно вздыхают и оставляют эту идею. <br>
 <br>
	 Удивительно, но многие даже не задумываются, где купить брендовые вещи в 3, в 4 и даже в 5 раз дешевле, чем в монобрендовых бутиках. Не ожидая сезонных скидок. <br>
 <br>
	 Формат стоков и аутлетов уже стал привычным, но и там сейчас не найти по-настоящему низких цен - хороших аутлетов в России можно перечесть по пальцам одной руки. <br>
 <br>
	 И тут приходят на помощь брендовые комиссионные магазины. Стереотип о том, что люксовая одежда обязательно должна быть из бутика - уже давно разрушен на Западе, который страдает от перепроизводства вещей. Бесконтрольный шопоголизм и показная роскошь стали моветоном. И потихоньку концепция вторичного использования проникает в России. Мы уже давно активно продаем и покупаем вещи на Авито, меняемся с подругами, участвуем в гаражных распродажах и своп-вечеринках. <br>
 <br>
	 Комильфо - сеть комиссионных бутиков брендовой одежды класса люкс, расположенных в центре города, в пяти минутах ходьбы от ст. м. Гостиный Двор и в пяти минутах от ст. м. Горьковская. У нас представленный широкий ассортимент товара: более 5000 наименований мужской и женской одежды и аксессуаров. Которые можно купить в два, в три, а то и в пять раз дешевле, чем в обычном бутике брендовой одежды. <br>
 <br>
	 Комиссионный магазин одежды - это формат далеко не для всех. У нас любят одеваться стилисты, которые находят всегда что-то особенное для себя. Любят одеваться шопоголики, которые уже знают наизусть весь сезонный ассортимент бутиков и приезжают к нам за чем-то новым, ведь новинки появляются каждый день. У нас любят одеваться люди, демонстрирующие определенный статус, при этом не считают нужным тратить огромные деньги на подходящую одежду, считая, что сэкономленные деньги лучше инвестировать дальше. <br>
 <br>
	 И мы рады, что в наших бутиках уникальные вещи находят по-настоящему интересных хозяев. Приходите, будем Вам рады!
</div> -->
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сотрудничество");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container static-page">
	<div class="row">
		<div class="col-md-6 disable-pag">
			<ul id="autoWidth" class="cs-hidden">
				<li><img src="/images/komilfo/colabo/1.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/2.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/3.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/4.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/5.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/6.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/7.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/8.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/9.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/10.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/11.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/12.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/13.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/14.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/15.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/16.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/17.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/18.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/19.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/20.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/21.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/22.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/23.jpg" alt=""></li>
				<li><img src="/images/komilfo/colabo/24.jpg" alt=""></li>
			</ul>
		</div>
		<div class="col-md-6">
			<p><b>Мы открыты к сотрудничеству!</b></p>
			<p>Уже 5 лет плодотворно работаем с фотографами, моделями, стилистами и журналами. Наша одежда участвует в fashion-съемках и модных показах.</p>
			<p>По вопросам творческого сотрудничества обращайтесь на почту: <a class="inverted-link" href="mailto:info@komilfo-butik.com">info@komilfo-butik.com</a></p>
		</div>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
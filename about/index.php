<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О магазине");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<div class="container static-page">
	<div class="row">
		<div class="col-xs-12">
 <br>
			 Комильфо-бутик - сеть комиссионных магазинов оригинальной брендовой одежды в центре Санкт-Петербурга. У нас представлено 1053 бренда класса люкс. Вещи привозят наши клиенты со всего мира - со стоков, аутлетов, дисконтов, миланских распродаж. Благодаря этому мы делаем скидки до 90% первоначальной стоимости.&nbsp;
		</div>
		<div class="col-xs-12">
 <br>
			 Мы работаем ежедневно, с 11:00 до 21:00. А прием вещей осуществляем со вторника по пятницу, с 14:00 до 20:00 в бутике,&nbsp;либо <a href="/handthings/"><u>онлайн</u></a>.&nbsp;<br>
 <img width="630" alt="Комиссионный бутик Комильфо" src="/upload/medialibrary/d98/d98219f6ca8fa4936de6058d6b5d2d8d.jpg" height="419" title="Комиссионный бутик Комильфо" align="middle"><br>
			 80% вещей - новые, и еще&nbsp;20% в отличном состоянии. Одежду отбирают и проверяют профессионалы - опыт работы с брендами 17 лет. Мы продаем только оригинальные вещи. 117 брендов эксклюзивные - в России купить можно только у нас. <br>
 <br>
			 Живете в Санкт-Петербурге? <br>
 <br>
			 К нам удобно приезжать мерить вещи - они все есть в магазине. Бутик на углу Садовой и Ломоносова, сразу за Гостиным Двором - Садовая, 28. Это в 3 минутах ходьбы от ст. м. Гостиный Двор. Бутик на углу Каменноостровского и Малой Посадской, в 4 минутах ходьбы от ст. м. Горьковская. Опытные консультанты и стилисты с удовольствием помогут Вам подобрать идеальное обновление гардероба. <br>
 <br>
			 Основа бизнеса - первоклассный сервис. Мы любим наши вещи и наших клиентов, и к каждому найдем подход, который оставит Вас на 100% довольными. Разделите нашу любовь к хорошим вещам и умении грамотно управлять финансами - заказывайте на сайте и приезжайте к нам в магазин за одеждой, со скидками до 90%.<br>
 <br>
			 Остались вопросы? С удовольствием пообщаюсь, <span class="comagic_phone3">8 (800) 333-67-94</span>. <br>
			 Федотовская Александра, владелица Комильфо-бутик.
			<div class="row">
				<div class="col-sm-6">
					<div class="yandex-map">
						 <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa24701967aecb748f0527994efbe4e0a8a7bf454ce46af9f554cd742607179a6&amp;width=100%&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="yandex-map">
						 <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A172fd8a20544ec84db4dbf51e08d7d400e5042255b691416a755803269bfd86f&amp;width=100%25&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
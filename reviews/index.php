<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/breadcrumbs.php"
	)
);?>
<!-- vk -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?142"></script>
<script type="text/javascript">
  VK.init({apiId: 5903440, onlyWidgets: true});
</script>
<!-- fb -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.8&appId=1739549109688772";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="container reviews-c">
   <p>Мы за то, чтобы отзывы были настоящими и живыми!<br>Нам будет приятно услышать Ваше мнение о нас:</p>
    <div class="row">
        <div class="col-md-6">
            <div id="vk_comments"></div>
            <script type="text/javascript">
                VK.Widgets.Comments("vk_comments", {autoPublish :1,limit: 10, attach: "*"});
            </script>
        </div>
        <div class="col-md-6">
            <div class="fb-comments" data-href="http://new.komilfo-butik.com/reviews/" data-numposts="5"></div>
        </div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
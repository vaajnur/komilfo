<?php
if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/dbconn.php")) {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/dbconn.php");
	$db_host = $DBHost;
	$db_user = $DBLogin;
	$db_pass = $DBPassword;
    $db_name = $DBName;
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
        $protocol = 'https://';
    } else {
        $protocol = 'http://';
    }
    $html=file_get_contents($protocol.$_SERVER['HTTP_HOST']);
    $errors_arr=array("query error","try later","Fatal error");
    foreach($errors_arr as $error){
        $pos = strpos($html,$error);
        if ($pos !== false) {
            echo "error";
        }
    }
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/wp-config.php")) {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/wp-config.php");
	$db_host = DB_HOST;
	$db_user = DB_USER;
	$db_pass = DB_PASSWORD;
	$db_name = DB_NAME;
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/config.php") && file_exists($_SERVER['DOCUMENT_ROOT'] . "/admin/config.php")) {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");
	$db_host = DB_HOSTNAME;
	$db_user = DB_USERNAME;
	$db_pass = DB_PASSWORD;
	$db_name = DB_DATABASE;
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/config.php")) {
	require_once($_SERVER['DOCUMENT_ROOT'] . "/config.php");
	$db_host = DB_HOST;
	$db_user = DB_USER;
	$db_pass = DB_PASS;
	$db_name = DB_TABLE;
}
$link = mysqli_connect($db_host, $db_user, $db_pass, $db_name) or die();
echo "1";